using UnityEngine;
using UnityEngine.Tilemaps;

namespace Game {

    public class GridTileSet {
        public Tile tile = Tile.CreateInstance<Tile>();
        public Tile leftEdge = Tile.CreateInstance<Tile>();
        public Tile rightEdge = Tile.CreateInstance<Tile>();
        public Tile topEdge = Tile.CreateInstance<Tile>();
        public Tile bottomEdge = Tile.CreateInstance<Tile>();

        public Tile corner00 = Tile.CreateInstance<Tile>();
        public Tile corner01 = Tile.CreateInstance<Tile>();
        public Tile corner10 = Tile.CreateInstance<Tile>();
        public Tile corner11 = Tile.CreateInstance<Tile>();
    }
}