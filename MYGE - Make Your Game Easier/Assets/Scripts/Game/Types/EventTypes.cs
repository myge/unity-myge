using Config;
using Colyseus.Schema;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game {

    namespace Events {

        public class FilesDroppedEvent : UnityEvent<List<string>, Vector3> {}
        public class TokenSelectionEvent : UnityEvent<string, string> {}
        public class TokenIDEvent : UnityEvent<string> {}
    }
}