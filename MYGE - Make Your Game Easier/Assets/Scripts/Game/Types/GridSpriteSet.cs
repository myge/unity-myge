﻿using UnityEngine;

namespace Game {

    public class GridSpriteSet {
        public Sprite tile;
        public Sprite leftEdge;
        public Sprite rightEdge;
        public Sprite topEdge;
        public Sprite bottomEdge;

        public Sprite corner00;
        public Sprite corner01;
        public Sprite corner10;
        public Sprite corner11;
    }
}