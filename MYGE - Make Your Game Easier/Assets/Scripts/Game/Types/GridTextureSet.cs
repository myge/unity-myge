﻿using UnityEngine;

namespace Game {

    public class GridTextureSet {
        public Texture2D tile;

        public Texture2D leftEdge;
        public Texture2D rightEdge;
        public Texture2D topEdge;
        public Texture2D bottomEdge;

        public Texture2D corner00;
        public Texture2D corner01;
        public Texture2D corner10;
        public Texture2D corner11;
    }
}