﻿using Network;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.Tilemaps;
using UI;

namespace Game {

    public class FogManager : EnhancedMonoBehaviour {
        private Tile fogTile;
        private Tile fogTileTrans;
        private Tile previewTile;

    #region Unity Editor Fields
        public ColyseusClientController colyseusClientController;
        public AreaSelectionPanel areaSelectionPanel;
        public TokenManager tokenManager;
        public Tilemap previewTilemap;
        public int previewTextureResolution = 200;
        public Color previewWallColor;
        public Tilemap fogTilemap;
        public Color fogTileColor;
        public Color fogTileTransparentColor;
        public float defaultSightRange = 10f;
    #endregion

    #region Public Functions
        public void SetPreviewActive(bool _active) {
            // We cannot just disable the entire game object - it needs to stay active,
            //  just invisible, for the purposes of raycasting and wall collision.
            float _alpha = _active ? 1 : 0;
            previewTilemap.color = new Color(1, 1, 1, _alpha);
        }

        public async void SetWallTile(string _areaId, int _x, int _y, bool _wall) {
            await colyseusClientController.CallService("setWallTile", new {
                areaId = _areaId,
                wall = _wall,
                position = new {
                    x = _x,
                    y = _y
                }
            });
        }
        public async void SetWallTile(int _x, int _y, bool _wall) {
            await colyseusClientController.CallService("setWallTile", new {
                areaId = areaSelectionPanel.currentAreaId,
                wall = _wall,
                position = new {
                    x = _x,
                    y = _y
                }
            });
        }

        public void UpdateForAreaView(string _areaId) {
            if (_areaId == "") {
                RenderPreview(null);
                return;
            }

            AreaState _area = colyseusClientController.GetLocalState().areas[_areaId];
            if (_area == null) {
                LogWarning("No area of ID " + _areaId);
                return;
            }

            RenderPreview(_area);
            RenderFog(_area);
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!colyseusClientController) LogWarning("No colyseusClientController attached");
            if (!areaSelectionPanel) LogWarning("No areaSelectionPanel attached");
            if (!tokenManager) LogWarning("No tokenManager attached");
            if (!previewTilemap) LogWarning("No previewTilemap attached");
            if (!fogTilemap) LogWarning("No fogTilemap attached");
        }

        private void HandleGMFogUpdate(string _areaId, string _tokenId) {
            if (_areaId == areaSelectionPanel.currentAreaId && colyseusClientController.role == Role.GM) {
                RenderFog(colyseusClientController.GetLocalState().areas[_areaId]);
            }
        }

        private void HandleFogUpdate(string _areaId, string _tokenId) {
            CampaignState _state = colyseusClientController.GetLocalState();
            if (_areaId == areaSelectionPanel.currentAreaId) {
                if (colyseusClientController.role == Role.GM) {
                    // Token selection only matters if GM is local
                    if (!tokenManager.IsTokenSelected(_tokenId)) return;
                } else {
                    // Ownership only matters if a player is local
                    if (!tokenManager.IsTokenControllableByLocal(_areaId, _tokenId)) return;
                }

                RenderFog(_state.areas[_areaId]);
            }
        }

        private void RenderFog(AreaState _area) {
            if (_area == null) {
                fogTilemap.ClearAllTiles();
                return;
            }

            fogTilemap.ClearAllTiles();
            fogTilemap.size = new Vector3Int(_area.areaSettings.gridWidth, _area.areaSettings.gridHeight, 1);

            Tile _fogTile = fogTile;

            IEnumerable<TokenState> _sightSourceTokens = null;
            if (colyseusClientController.role == Role.GM) {
                IEnumerable<TokenState> _gmSelectedTokenIDs = tokenManager.GetSelectedTokenIDs().Select(_id => _area.tokens[_id]);

                if (_gmSelectedTokenIDs.Count<TokenState>() < 1) {
                    // If nothing is selected, the GM just sees everything, so there is nothing left to do
                    return;
                } else {
                    // If something is selected, show the GM the perspective of those tokens
                    _sightSourceTokens = _gmSelectedTokenIDs;

                    // The GM still sees through fog - it's semi-transparent
                    _fogTile = fogTileTrans;
                }
            } else {
                // A player always sees all tiles that *any* of their controlled tokens can see
                string _localSessionId = colyseusClientController.GetLocalSessionId();
                _sightSourceTokens = colyseusClientController.GetLocalState().GetTokensOwnedByPlayer(_area.id, _localSessionId);
            }

            var _seenTiles = new HashSet<Vector2Int>();

            foreach (TokenState _token in _sightSourceTokens) {
                // Hidden tokens never contribute to vision
                if (_token.visibleToGmOnly && colyseusClientController.role != Role.GM) continue;

                // Cast rays from the token's position toward all target tiles
                Vector2 _origin = _token.pos.ToVector();
                float _range = defaultSightRange; // TODO: Customizable sight range?

                for (int x = 0; x < _area.areaSettings.gridWidth; x++) {
                    for (int y = 0; y < _area.areaSettings.gridHeight; y++) {
                        Vector2Int _target = new Vector2Int(x, y);

                        if (_seenTiles.Contains(_target)) continue;

                        Vector2 _difference = (_target - _origin);
                        float _distance = _difference.magnitude;

                        if (_distance > defaultSightRange) {
                            // The tile was out of sight range, don't even consider it. This makes
                            //  large maps bearable in terms of computation time.
                            fogTilemap.SetTile((Vector3Int)_target, _fogTile);
                            continue;
                        }

                        Vector2 _direction = _difference.normalized;
                        RaycastHit2D _hit = Physics2D.Raycast(_origin, _direction, _distance);

                        if (_hit.collider != null) {
                            // The ray has hit something, thus the *target tile* is obscured.
                            // We don't really care where the *wall* exactly is, just that a wall
                            //  has been hit.
                            fogTilemap.SetTile((Vector3Int)_target, _fogTile);
                        } else {
                            // The ray has not hit anything, thus the tile is visible. Mark
                            //  it as visible and don't test for it any further.
                            fogTilemap.SetTile((Vector3Int)_target, null);
                            _seenTiles.Add(_target);
                        }
                    }
                }
            }

            fogTilemap.RefreshAllTiles();
        }

        private void RenderPreview(AreaState _area) {
            if (_area == null) {
                previewTilemap.ClearAllTiles();
                return;
            }

            previewTilemap.ClearAllTiles();
            previewTilemap.size = new Vector3Int(_area.areaSettings.gridWidth, _area.areaSettings.gridHeight, 1);

            _area.wallTiles.ForEach((_posStringInt, _value) => {
                Vector2Int _pos = SchemaExtensions.VectorFromStringInt(_posStringInt);
                previewTilemap.SetTile((Vector3Int)_pos, previewTile);
            });

            previewTilemap.RefreshAllTiles();
        }

        private void RegisterOutsideListeners() {
            colyseusClientController.emitter.onAddWallTile.AddListener(delegate (string _areaId, int _x, int _y) {
                if (_areaId == areaSelectionPanel.currentAreaId) {
                    SetPreviewTile(new Vector2Int(_x, _y), true);
                    RenderFog(colyseusClientController.GetLocalState().areas[_areaId]);
                }
            });
            colyseusClientController.emitter.onRemoveWallTile.AddListener(delegate (string _areaId, int _x, int _y) {
                if (_areaId == areaSelectionPanel.currentAreaId) {
                    SetPreviewTile(new Vector2Int(_x, _y), false);
                    RenderFog(colyseusClientController.GetLocalState().areas[_areaId]);
                }
            });

            // If the set of either GM-selected or player-owned tokens changes, recalculate the fog
            colyseusClientController.emitter.onAddToken.AddListener(delegate (TokenState _token, string _areaId, string _tokenId) {
                HandleFogUpdate(_areaId, _tokenId);
            });
            colyseusClientController.emitter.onTokenPositionChanged.AddListener(delegate (string _areaId, string _tokenId, List<Colyseus.Schema.DataChange> _changes) {
                HandleFogUpdate(_areaId, _tokenId);
            });
            colyseusClientController.emitter.onRemoveToken.AddListener(delegate (TokenState _token, string _areaId, string _tokenId) {
                HandleFogUpdate(_areaId, _tokenId);
            });

            // If the token selection changes, recalculate the fog but only for GM
            tokenManager.onTokenSelected.AddListener(delegate (string _areaId, string _tokenId) {
                HandleGMFogUpdate(_areaId, _tokenId);
            });
            tokenManager.onTokenDeselected.AddListener(delegate (string _areaId, string _tokenId) {
                HandleGMFogUpdate(_areaId, _tokenId);
            });
        }

        private void SetFogTile(Vector2Int _pos, bool _fog) {
            _pos += new Vector2Int(1, 1);
            fogTilemap.SetTile((Vector3Int)_pos, _fog ? fogTile : null);
            fogTilemap.RefreshTile((Vector3Int)_pos);
        }

        private void SetPreviewTile(Vector2Int _pos, bool _wall) {
            previewTilemap.SetTile((Vector3Int)_pos, _wall ? previewTile : null);
            previewTilemap.RefreshTile((Vector3Int)_pos);
        }

        private void UpdateTileSprites() {
            fogTile = Tile.CreateInstance<Tile>();
            fogTile.sprite = FogSpriteGenerator.GetWallPreviewSprite();
            fogTile.flags = TileFlags.None;
            fogTile.color = fogTileColor;

            fogTileTrans = Tile.CreateInstance<Tile>();
            fogTileTrans.sprite = FogSpriteGenerator.GetWallPreviewSprite();
            fogTileTrans.flags = TileFlags.None;
            fogTileTrans.color = fogTileTransparentColor;

            previewTile = Tile.CreateInstance<Tile>();
            previewTile.sprite = FogSpriteGenerator.GetWallPreviewSprite();
            previewTile.flags = TileFlags.None;
            previewTile.color = previewWallColor;
        }
    #endregion
    
    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
            FogSpriteGenerator.previewTextureResolution = previewTextureResolution;
            UpdateTileSprites();
            RegisterOutsideListeners();

            // Start with the preview disabled, wait for the tool to be selected
            //  so that it's enabled once again.
            SetPreviewActive(false);
        }
    #endregion
    }
}