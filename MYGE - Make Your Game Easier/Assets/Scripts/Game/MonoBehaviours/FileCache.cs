﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.Networking;

namespace Network {

    public class FileCache : EnhancedMonoBehaviour {
        Dictionary<string, Texture2D> cachedTextures = new Dictionary<string, Texture2D>();

        [Serializable]
        private class ImageURIResult {
            public string imageURI = "";
        }

        [Serializable]
        private class ImageUploadResult {
            [Serializable]
            public class FileDetail {
                public string name = "";
                public string mimetype = "";
                public int size = 0;
                public string href = "";
                public string uri = "";
            }
            public bool status = false;
            public string message = "";
            public FileDetail[] files = new FileDetail[] {};
        }
        
    #region Unity Editor Fields
        public ColyseusClientController colyseusClientController;
        public Texture2D placeholderTexture;
        public int webTextureDownloadRetryInterval = 2000;
        public int webTextureDownloadRetryLimit = 3;
    #endregion
    
    #region Public Functions
        public async Task<string[]> GetAllImageURIs() {
            string _listJSON = await colyseusClientController.CallService("getImagesFileList");
            string[] _list = JsonConvert.DeserializeObject<string[]>(_listJSON);
            return _list;
        }

        public void GetTexture(string _imageURI, Action<Texture2D> _onSuccess = null, Action<string> _onError = null) {
            if (cachedTextures.ContainsKey(_imageURI)) {
                if (_onSuccess != null) _onSuccess(cachedTextures[_imageURI]);
            } else {
                DownloadTexture(_imageURI, _onSuccess, _onError);
            }
        }

        public async void GetTextureFromWeb(string _externalURL, Action<Texture2D, string> _onSuccess = null, Action<string> _onError = null) {
            string _resultJSON = await colyseusClientController.CallService("downloadImage", new {
                externalImageURL = _externalURL
            });
            var _result = JsonConvert.DeserializeObject<ImageURIResult>(_resultJSON);
            string _imageURI = _result.imageURI;

            for (int i = webTextureDownloadRetryLimit; i >= 0; i--) {
                try {
                    Texture2D _texture = await GetTextureAsync(_imageURI);
                    // Download successful
                    if (_onSuccess != null) _onSuccess(_texture, _imageURI);
                    break;
                } catch (Exception _ex) {
                    // Download failed
                    if (i == 0) {
                        // This was the last retry
                        if (_onError != null) _onError(_ex.Message);
                    } else {
                        // There are stil retries left
                        await Task.Delay(webTextureDownloadRetryInterval);
                    }
                }
            }
        }

        public void UploadTexture(string _filepath, Action<string> _onSuccess = null, Action<string> _onError = null) {
            StartCoroutine(UploadTextureCoroutine(_filepath, _onSuccess, _onError));
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!colyseusClientController) LogWarning("No colyseusClientController attached");
        }
        
        private void DownloadTexture(string _imageURI, Action<Texture2D> _onSuccess = null, Action<string> _onError = null) {
            StartCoroutine(DownloadTextureCoroutine(_imageURI, _onSuccess, _onError));
        }

        private IEnumerator DownloadTextureCoroutine(string _imageURI, Action<Texture2D> _onSuccess = null, Action<string> _onError = null) {
            string _url = colyseusClientController.GetHostHTTPAddress() + "/" + _imageURI;

            using (UnityWebRequest _req = UnityWebRequestTexture.GetTexture(_url)) {
                // Request and wait
                yield return _req.SendWebRequest();

                if (_req.isNetworkError || _req.isHttpError) {
                    if (_onError != null) {
                        _onError(_req.error);
                    } else {
                        LogWarning("Error downloading " + _imageURI + ": " + _req.error);
                    }
                } else {
                    // The asset is ready
                    Texture2D _texture = DownloadHandlerTexture.GetContent(_req);
                    if (cachedTextures.ContainsKey(_imageURI)) {
                        cachedTextures[_imageURI] = _texture;
                    } else {
                        cachedTextures.Add(_imageURI, _texture);
                    }

                    if (_onSuccess != null) _onSuccess(_texture);
                }
            }
        }

        private string GetMimeType(string _path) {
            string _extension = Path.GetExtension(_path);
            switch (_extension) {
                case ".gif": return "image/gif";
                case ".jpeg": return "image/jpeg";
                case ".jpg": return "image/jpeg";
                case ".png": return "image/png";
                case ".webp": return "image/webp";
                default:
                    throw new Exception("No mimetype supported for extension " + _extension);
            }
        }

        private IEnumerator UploadTextureCoroutine(string _path, Action<string> _onSuccess = null, Action<string> _onError = null) {
            string _url = colyseusClientController.GetHostHTTPAddress() + "/upload-images";
            string _filename = Path.GetFileName(_path);
            string _mimeType = GetMimeType(_path);
            
            using (UnityWebRequest _file = UnityWebRequestTexture.GetTexture(_path)) {
                // Request and wait
                yield return _file.SendWebRequest();

                if (_file.isNetworkError) {
                    if (_onError != null) {
                        _onError(_file.error);
                    } else {
                        LogWarning("Error loading " + _path + ": " + _file.error);
                    }
                    yield break;
                }

                // We have the texture at this point, let's remember it
                Texture2D _texture = DownloadHandlerTexture.GetContent(_file);

                List<IMultipartFormSection> _form = new List<IMultipartFormSection>();
                _form.Add(new MultipartFormFileSection("images", _file.downloadHandler.data, _filename, _mimeType));

                using (UnityWebRequest _req = UnityWebRequest.Post(_url, _form)) {
                    yield return _req.SendWebRequest();

                    if (_req.isNetworkError || _req.isHttpError) {
                        if (_onError != null) {
                            _onError(_req.error);
                        } else {
                            LogWarning("Error uploading " + _path + ": " + _req.error);
                        }
                    } else {
                        // We should have a response
                        string _responseJSON = _req.downloadHandler.text;
                        var _response = JsonConvert.DeserializeObject<ImageUploadResult>(_responseJSON);
                        var _fileData = _response.files[0];

                        string _imageURI = _fileData.uri;

                        // Now, knowing the uri, we can cache the texture that is already in the memory,
                        //  so that we don't re-download it needlessly.
                        if (cachedTextures.ContainsKey(_imageURI)) {
                            cachedTextures[_imageURI] = _texture;
                        } else {
                            cachedTextures.Add(_imageURI, _texture);
                        }

                        if (_onSuccess != null) _onSuccess(_imageURI);
                    }
                }
            }
        }

        private async Task<Texture2D> GetTextureAsync(string _imageURI) {
            var _tcs = new TaskCompletionSource<Texture2D>();
            GetTexture(_imageURI, delegate (Texture2D _tex) {
                _tcs.SetResult(_tex);
            }, delegate (string _error) {
                _tcs.SetException(new Exception(_error));
            });

            Texture2D _texture = await _tcs.Task;
            return _texture;
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
        }
    #endregion
    }
}