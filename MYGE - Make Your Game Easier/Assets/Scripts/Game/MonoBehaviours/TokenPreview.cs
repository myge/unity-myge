﻿using Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCore.Utils;

namespace Game {

    public class TokenPreview : EnhancedMonoBehaviour {
        private string loadedPortraitImageURI = "";

    #region Unity Editor Fields
        public Token token;
        public Camera tokenPreviewCamera;
        public RenderTextureDragAndDrop tokenPortraitDragAndDrop;
        public float scaleSensitivity = 0.1f;
    #endregion

    #region Public Functions
        public void ChangeScale(float _deltaPercentage, float _scaleMin = 0.05f, float _scaleMax = 2.0f) {
            float _oldScale = GetScale();
            // Similar to zoom - for large scale values, it becomes frustratingly slow to
            //  scroll. The solution in this case is to make change ratio proportional to
            //  the scale at any given point in time.
            float _deltaScale = _deltaPercentage * _oldScale;
            float _newScale = Mathf.Clamp(_oldScale + _deltaScale * scaleSensitivity, _scaleMin, _scaleMax);
            SetScale(_newScale);

            // It's difficult to do a drag-and-drop with respect to the sprite pivot point,
            //  it's done with the transform local position instead. The pivot point instead
            //  always remains at (0.5, 0.5) normalized.
            // This means that scaling the image, if the image is not centered, effectively
            //  changes the focal point of the preview, as the previous focal point has
            //  moved by some vector.
            // Given how local position is already measured from the preview's (parent's)
            //  center, it's enough for it to remain proportional to the scale. By default,
            //  of course, it does not. Thus this is done here.
            float _scaleRatio = _newScale / _oldScale;
            token.portraitRenderer.transform.localPosition *= _scaleRatio;
        }

        public PortraitState CollectPortrait() {
            PortraitState _portrait = new PortraitState();
            _portrait.imageURI = loadedPortraitImageURI;
            _portrait.tokenOffset.x = token.portraitRenderer.transform.localPosition.x;
            _portrait.tokenOffset.y = token.portraitRenderer.transform.localPosition.y;
            _portrait.tokenScale = GetScale();
            return _portrait;
        }

        public void DragPortrait() {
            tokenPortraitDragAndDrop.Drag();
        }

        public void DropPortrait() {
            tokenPortraitDragAndDrop.Drop();
        }

        public float GetScale() {
            return token.portraitRenderer.transform.localScale.x;
        }

        public bool IsMouseOnPreview() {
            Vector2 _localMousePos = tokenPortraitDragAndDrop.textureTransform.InverseTransformPoint(Input.mousePosition);
            return tokenPortraitDragAndDrop.textureTransform.rect.Contains(_localMousePos);
        }

        public void LoadPortrait(string _imageURI, Texture2D _texture, Vector3 _offset, float _scale) {
            loadedPortraitImageURI = _imageURI;
            token.SetPortrait(_texture, _offset, _scale);
        }

        public void Reset() {
            token.portraitRenderer.transform.localPosition = new Vector3();
            SetScale(1f);
        }

        public void SetScale(float _scale) {
            token.portraitRenderer.transform.localScale = new Vector3(_scale, _scale, 1);
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!token) LogWarning("No token attached");
            if (!tokenPreviewCamera) LogWarning("No tokenPreviewCamera attached");
            if (!tokenPortraitDragAndDrop) LogWarning("No tokenPortraitDragAndDrop attached");
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
        }
    #endregion

    }
}