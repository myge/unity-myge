﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Game {

    public class GridManager : EnhancedMonoBehaviour {
        private GridTileSet tiles;
        
    #region Unity Fields
        public Tilemap tilemap;

        public int tileTextureResolution = 200;
        public int edgeThickness = 6;
    #endregion

    #region Public Functions
        public void Clear() {
            tilemap.ClearAllTiles();
            tilemap.RefreshAllTiles();
        }

        // Gets the center points of the bottom-left and top-right corner tiles.
        public Tuple<Vector3, Vector3> GetGridCornerCentersWorld() {
            Vector3 _bottomLeftCorner = tilemap.CellToWorld(new Vector3Int(1, 1, 0));
            Vector3 _topRightCorner = tilemap.CellToWorld(new Vector3Int(tilemap.size.x - 2, tilemap.size.y - 2, 0));

            return new Tuple<Vector3, Vector3>(_bottomLeftCorner, _topRightCorner);
        }

        // Gets the corner points of the whole grid.
        public Tuple<Vector3, Vector3> GetGridCornersWorld() {
            Vector3 _bottomLeftCorner = tilemap.CellToWorld(new Vector3Int(1, 1, 0)) + tilemap.tileAnchor;
            Vector3 _topRightCorner = tilemap.CellToWorld(new Vector3Int(tilemap.size.x - 1, tilemap.size.y - 1, 0)) + tilemap.tileAnchor;

            return new Tuple<Vector3, Vector3>(_bottomLeftCorner, _topRightCorner);
        }

        public Vector3Int GetPosUnderMouse() {
            Vector3 _mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3Int _snappedMousePos = Vector3Int.RoundToInt(_mouseWorldPos);

            return _snappedMousePos;
        }

        public bool IsMouseOverGrid() {
            Vector3 _mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            return IsPosOnGrid(_mouseWorldPos);
        }

        public bool IsPosOnGrid(Vector3 _pos) {
            var _corners = GetGridCornersWorld();
            Vector3 _bottomLeftCorner = _corners.Item1;
            Vector3 _topRightCorner = _corners.Item2;
            
            return (
                _bottomLeftCorner.x <= _pos.x && _pos.x <= _topRightCorner.x &&
                _bottomLeftCorner.y <= _pos.y && _pos.y <= _topRightCorner.y
            );
        }

        public void RenderGrid(int _width, int _height) {
            SetSize(_width, _height);
        }

        public void SetEdgeThickness(int _edgeThickness) {
            edgeThickness = _edgeThickness;
            UpdateTileSet();
            tilemap.RefreshAllTiles();
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!tilemap) LogWarning("No tilemap attached");
        }

        private void PopulateTiles(int _width, int _height) {
            // Center tiles
            for (int x = 1; x <= _width; x++) {
                for (int y = 1; y <= _height; y++) {
                    tilemap.SetTile(new Vector3Int(x, y, 0), tiles.tile);
                }
            }

            /* Yes, the sprites are reversed. This is due to how, for example, 
                the left edge (of the whole grid) must use "right edge" (of an
                individual tile) sprites, so that they match the other tiles. */
            
            // Vertical edges
            for (int y = 1; y <= _height; y++) {
                // Left edge
                tilemap.SetTile(new Vector3Int(0, y, 0), tiles.rightEdge);

                // Right edge
                tilemap.SetTile(new Vector3Int(_width + 1, y, 0), tiles.leftEdge);
            }

            // Horizontal edges
            for (int x = 1; x <= _width; x++) {
                // Bottom edge
                tilemap.SetTile(new Vector3Int(x, 0, 0), tiles.topEdge);

                // Top edge
                tilemap.SetTile(new Vector3Int(x, _height + 1, 0), tiles.bottomEdge);
            }

            // Corners
            tilemap.SetTile(new Vector3Int(0, 0, 0), tiles.corner11);
            tilemap.SetTile(new Vector3Int(0, _height + 1, 0), tiles.corner10);
            tilemap.SetTile(new Vector3Int(_width + 1, 0, 0), tiles.corner01);
            tilemap.SetTile(new Vector3Int(_width + 1, _height + 1, 0), tiles.corner00);

            // Remove leftover tiles
            for (int x = 0; x < tilemap.size.x; x++) {
                if (x <= _width + 1) {
                    for (int y = _height + 2; y < tilemap.size.y; y++) {
                        tilemap.SetTile(new Vector3Int(x, y, 0), null);
                    }
                } else {
                    for (int y = 0; y < tilemap.size.y; y++) {
                        tilemap.SetTile(new Vector3Int(x, y, 0), null);
                    }
                }
            }
        }

        private void SetSize(int _width, int _height) {
            PopulateTiles(_width, _height);
            // There must be extra space for edge tiles on all 4 sides
            tilemap.size = new Vector3Int(_width + 2, _height + 2, 1);
            tilemap.RefreshAllTiles();
        }

        private void UpdateTileSet() {
            tiles = new GridTileSet();

            GridSpriteSet _sprites = GridSpriteGenerator.GetGridSpriteSet(edgeThickness);
            tiles.tile.sprite = _sprites.tile;

            tiles.leftEdge.sprite = _sprites.leftEdge;
            tiles.rightEdge.sprite = _sprites.rightEdge;
            tiles.topEdge.sprite = _sprites.topEdge;
            tiles.bottomEdge.sprite = _sprites.bottomEdge;

            tiles.corner00.sprite = _sprites.corner00;
            tiles.corner01.sprite = _sprites.corner01;
            tiles.corner10.sprite = _sprites.corner10;
            tiles.corner11.sprite = _sprites.corner11;
        }
    #endregion

        private void Awake() {
            CheckIntegrity();
            GridSpriteGenerator.tileTextureResolution = tileTextureResolution;
            UpdateTileSet();
        }
    }
}