﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCore.Utils;

namespace Game {

    public class Token : EnhancedMonoBehaviour {
        private Vector3 defaultPortraitLocalPos;
        private Vector3 defaultPortraitLocalScale;
        private Sprite defaultPortraitSprite;

    #region Unity Editor Fields
        public SpriteRenderer portraitRenderer;
        public float portraitPixelsPerUnit = 200f;
    #endregion

    #region Public Functions
        public void ResetPortraitToDefault() {
            portraitRenderer.sprite = defaultPortraitSprite;
            portraitRenderer.transform.localPosition = defaultPortraitLocalPos;
            portraitRenderer.transform.localScale = defaultPortraitLocalScale;
        }

        public void SetPortrait(Texture2D _texture, Vector3 _offset = default(Vector3), float _scale = 1f) {
            Sprite _sprite = CreatePortraitSprite(_texture);
            portraitRenderer.sprite = _sprite;
            portraitRenderer.transform.localPosition = _offset;
            portraitRenderer.transform.localScale = new Vector3(
                x: _scale,
                y: _scale,
                z: 1f
            );
        }

        public void SetOpacity(float _opacity = 1f) {
            SpriteRenderer _bgRenderer = gameObject.GetComponent<SpriteRenderer>();
            Color _newBgColor = new Color(
                r: _bgRenderer.color.r,
                g: _bgRenderer.color.g,
                b: _bgRenderer.color.b,
                a: _opacity
            );
            _bgRenderer.color = _newBgColor;

            Color _newPortraitColor = new Color(
                r: portraitRenderer.color.r,
                g: portraitRenderer.color.g,
                b: portraitRenderer.color.b,
                a: _opacity
            );
            portraitRenderer.color = _newPortraitColor;
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!portraitRenderer) LogWarning("No portraitRenderer attached");
        }

        private Sprite CreatePortraitSprite(Texture2D _texture) {
            return Sprite.Create(
                texture: _texture,
                rect: new Rect(0, 0, _texture.width, _texture.height),
                pivot: new Vector2(0.5f, 0.5f),
                pixelsPerUnit: portraitPixelsPerUnit,
                extrude: 0,
                meshType: SpriteMeshType.Tight
            );
        }

        private void SavePortraitDefaults() {
            defaultPortraitLocalPos = portraitRenderer.transform.localPosition;
            defaultPortraitLocalScale = portraitRenderer.transform.localScale;
            defaultPortraitSprite = portraitRenderer.sprite;
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
            SavePortraitDefaults();
        }
    #endregion

    }
}