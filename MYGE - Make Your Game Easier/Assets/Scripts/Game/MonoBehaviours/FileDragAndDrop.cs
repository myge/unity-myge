using Game.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCore.Utils;
using B83.Win32;

namespace Game {

    public class FileDragAndDrop : EnhancedMonoBehaviour {
        public FilesDroppedEvent onFilesDropped = new FilesDroppedEvent();
    #region Unity Editor Fields
        
    #endregion

    #region Public Functions
        
    #endregion

    #region Private Functions
        private void CheckIntegrity() {}

        private void HandleDroppedFiles(List<string> _filePaths, POINT _mousePos) {
            Vector3 _screenMousePos = new Vector3(_mousePos.x, _mousePos.y, 0);
            onFilesDropped.Invoke(_filePaths, _screenMousePos);
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            UnityDragAndDropHook.InstallHook();
            UnityDragAndDropHook.OnDroppedFiles += HandleDroppedFiles;
        }

        private void OnDisable() {
            UnityDragAndDropHook.UninstallHook();
        }
    #endregion

    }
}