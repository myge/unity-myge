﻿using Game.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Game {

    public class ToolManager : EnhancedMonoBehaviour {
    
    #region Unity Editor Fields
        public Tool defaultTool;
        public Tool[] registeredTools;
        public float normalAlpha = 0.4f;
        public float selectedAlpha = 1.0f;
    #endregion

    #region Public Properties
        public Tool currentTool { get; private set; }
    #endregion

    #region Public Functions
        public void SelectTool(Tool _tool) {
            if (currentTool) {
                SetToolHighlight(currentTool, false);
                currentTool.ToolDeselect();
            }
            currentTool = _tool;
            SetToolHighlight(_tool, true);
            _tool.ToolSelect();
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!defaultTool) LogWarning("No defaultTool attached");
        }

        private Color GetColorWithAlpha(Color _color, float _alpha) {
            return new Color(
                _color.r,
                _color.g,
                _color.b,
                _alpha
            );
        }

        private void RegisterTools() {
            foreach (Tool _tool in registeredTools) {
                Button _btn = _tool.GetComponent<Button>();
                if (!_btn) {
                    LogWarning("Tool '" + _tool.gameObject.name + "' has no Button component");
                    return;
                }
                _btn.onClick.AddListener(delegate {
                    SelectTool(_tool);
                });

                SetToolHighlight(_tool, false);
            }
        }

        private void SetToolHighlight(Tool _tool, bool _highlighted) {
            Image _img = _tool.GetComponent<Image>();
            if (!_img) {
                LogWarning("Tool '" + _tool.gameObject.name + "' has no Image component");
                return;
            }
            float _alpha = _highlighted ? selectedAlpha : normalAlpha;
            Color _newColor = GetColorWithAlpha(_img.color, _alpha);
            _img.color = _newColor;
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            CheckIntegrity();
            RegisterTools();
            SelectTool(defaultTool);
        }
    #endregion
    }
}