﻿using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;

namespace Game {

    namespace Tools {

        public abstract class Tool : EnhancedMonoBehaviour {
        #region Unity Editor Fields
            public GridManager grid;
        #endregion

        #region Public Functions
            public abstract void LeftMousePress();
            public abstract void LeftMouseHold();
            public abstract void LeftMouseClickRelease();
            public abstract void LeftMouseDragRelease();
            public abstract void RightMousePress();
            public abstract void RightMouseHold();
            public abstract void RightMouseClickRelease();
            public abstract void RightMouseDragRelease();
            public abstract bool BlockMouseCameraMovement();

            public virtual void ToolSelect() {}
            public virtual void ToolDeselect() {}
        #endregion

        #region Private Functions
            protected virtual void CheckIntegrity() {
                if (!grid) LogWarning("No grid attached");
            }
        #endregion
        
        #region Unity Functions
            protected void OnEnable() {
                CheckIntegrity();
            }
        #endregion
        }
    }
}