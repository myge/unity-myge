﻿using System.Collections;
using System.Collections.Generic;
using UnityCore.Menu;
using UnityEngine;
using UI;

namespace Game {

    namespace Tools {

        public class DynamicLightingTool : Tool {
            private Vector3Int lastPos = new Vector3Int(-1, -1, -1);

        #region Unity Editor Fields
            public FogManager fog;
        #endregion

        #region Public Functions
            public override void LeftMousePress() {

            }

            public override void LeftMouseHold() {
                if (!grid.IsMouseOverGrid()) return;

                Vector3Int _pos = grid.GetPosUnderMouse();

                // Compare to last targeted position to prevent frantic sending of
                //  service calls that don't change anything
                if (_pos == lastPos) return;
                lastPos = _pos;

                // Left Shift + Left Click removes walls instead
                bool _wall = !Input.GetKey(KeyCode.LeftShift);
                fog.SetWallTile(_pos.x, _pos.y, _wall);
            }

            public override void LeftMouseClickRelease() {

            }

            public override void LeftMouseDragRelease() {

            }

            public override void RightMousePress() {

            }

            public override void RightMouseHold() {

            }

            public override void RightMouseClickRelease() {

            }

            public override void RightMouseDragRelease() {

            }

            public override bool BlockMouseCameraMovement() {
                // Left click = painting walls. Disallow simultaneous camera movement
                return Input.GetMouseButton(0);
            }

            public override void ToolSelect() {
                fog.SetPreviewActive(true);
            }
            
            public override void ToolDeselect() {
                fog.SetPreviewActive(false);
            }
        #endregion

        #region Private Functions
            protected override void CheckIntegrity() {
                base.CheckIntegrity();

                if (!fog) LogWarning("No fogManager attached");
            }
        #endregion
        
        #region Unity Functions

        #endregion
        }
    }
}