﻿using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Menu;
using UnityEngine;

namespace Game {

    namespace Tools {

        public class TokenSelectionTool : Tool {
            // Keep track of the individual token for drag and drop.
            private string _draggedTokenId = "";

        #region Unity Editor Fields
            public ColyseusClientController colyseusClientController;
            public TokenManager tokenManager;
            public ContextObject gridTileContextMenu;
            public ContextObject tokenContextMenu;
        #endregion

        #region Public Functions
            public override void LeftMousePress() {
                // Left mouse button on a *token* will later select the token
                string _targetTokenId = tokenManager.GetIdOfTokenUnderMouse();
                if (_targetTokenId == "") return;

                // If the client doesn't have permissions to manipulate tokens,
                //  don't even hint that it's possible.
                if (!tokenManager.IsTokenControllableByLocal(_targetTokenId)) return;

                // Start dragging the token, just in case this is what
                //  the user wants.
                tokenManager.DragToken(_targetTokenId);
                _draggedTokenId = _targetTokenId;
            }

            public override void LeftMouseHold() {

            }

            public override void LeftMouseClickRelease() {
                // Left mouse button *release* on a token selects the token
                string _targetTokenId = tokenManager.GetIdOfTokenUnderMouse();
                if (_targetTokenId == "") return;

                // Allow non-owners to *select* a token, but nothing more.
                tokenManager.SelectToken(_targetTokenId);
                if (!tokenManager.IsTokenControllableByLocal(_targetTokenId)) return;

                // This is just a selection, so bail out of the whole drag and
                //  drop scenario.
                tokenManager.DropToken(_targetTokenId, _move: false);
                _draggedTokenId = "";
            }

            public override void LeftMouseDragRelease() {
                // The mouse moved far, so this is the user dragging the token
                //  with the intent to move it.
                if (_draggedTokenId == "") return;

                tokenManager.DropToken(_draggedTokenId, _move: true);
                _draggedTokenId = "";
            }

            public override void RightMousePress() {
                // Right mouse button on a grid tile will later open a context menu
            }

            public override void RightMouseHold() {

            }

            public override void RightMouseClickRelease() {
                // Right mouse button *release* on a grid tile opens a context menu

                // Don't open context menus if the user is dragging a token
                if (_draggedTokenId != "") return;

                // Don't open the context menu if the pointer is not over the grid
                if (!grid.IsMouseOverGrid()) return;
                
                string _targetTokenId = tokenManager.GetIdOfTokenUnderMouse();
                tokenManager.SelectToken(_targetTokenId);
                // Pointer over token should spawn a different menu compared to an empty tile
                if (_targetTokenId != "") {
                    tokenContextMenu.OpenAtScreenPosition(Input.mousePosition);
                } else {
                    // Only the GM can use grid context menu (at least for now, this might
                    //  change if there are more options added to it)
                    if (colyseusClientController.role == Role.GM) {
                        gridTileContextMenu.OpenAtScreenPosition(Input.mousePosition);
                    }
                }
            }

            public override void RightMouseDragRelease() {
                // Nothing
            }

            public override bool BlockMouseCameraMovement() {
                // Don't move the camera if a token is being dragged
                return _draggedTokenId != "";
            }
        #endregion

        #region Private Functions
            protected override void CheckIntegrity() {
                base.CheckIntegrity();
                if (!colyseusClientController) LogWarning("No colyseusClientController attached");
                if (!tokenManager) LogWarning("No tokenManager attached");
                if (!gridTileContextMenu) LogWarning("No gridTileContextMenu attached");
                if (!tokenContextMenu) LogWarning("No tokenContextMenu attached");
            }
        #endregion
        
        #region Unity Functions

        #endregion
        }
    }
}