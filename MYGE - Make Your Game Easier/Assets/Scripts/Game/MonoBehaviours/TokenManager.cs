﻿using Game.Events;
using Network;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.Events;
using UI;

namespace Game {

    public class TokenManager : EnhancedMonoBehaviour {
        private Dictionary<string, Token> tokenObjectsById = new Dictionary<string, Token>();
        private List<string> selectedTokenIDs = new List<string>();

        private TokenIDEvent onTokenObjectCreated = new TokenIDEvent();

        [Serializable]
        private class AddNewTokenResult {
            public TokenState token = null;
            public string tokenId = "";
        }

    #region Unity Fields
        public ColyseusClientController colyseusClientController;
        public GridManager grid;
        public FileCache fileCache;
        public FileDragAndDrop fileDragAndDrop;
        public AreaSelectionPanel areaSelectionPanel;
        public CharacterStatePanel characterStatePanel;
        public CharacterOwnerPanel characterOwnerPanel;
        public TokenPortraitEditorPanel tokenPortraitEditor;
        public ContextObject gridTileContextMenu;
        public GameObject tokenTemplate;
        public Material defaultTokenMaterial;
        public Material selectedTokenMaterial;
        public float hiddenTokensAlpha = 0.5f;
    #endregion

        public readonly TokenSelectionEvent onTokenDeselected = new TokenSelectionEvent();
        public readonly TokenSelectionEvent onTokenSelected = new TokenSelectionEvent();

    #region Public Functions
        public async Task<string> AddNewToken(string _areaId, Vector3 _position) {
            string _resultJSON = await colyseusClientController.CallService("addNewToken", new {
                areaId = _areaId,
                position = new {
                    x = _position.x,
                    y = _position.y
                }
            });
            var _result = JsonConvert.DeserializeObject<AddNewTokenResult>(_resultJSON);
            return _result.tokenId;
        }

        public async void AddNewTokenAtContextMenu() {
            string _areaId = areaSelectionPanel.currentAreaId;

            Vector3 _contextMenuPos = Camera.main.ScreenToWorldPoint(gridTileContextMenu.transform.position);
            Vector3Int _snappedMousePos = Vector3Int.RoundToInt(_contextMenuPos);

            await AddNewToken(_areaId, _snappedMousePos);
        }

        public void AddTokenFromClipboard() {
            string _clipboardContent = GUIUtility.systemCopyBuffer;

            Uri _uri;
            bool _isURL = Uri.TryCreate(_clipboardContent, UriKind.Absolute, out _uri);

            if (_isURL) {
                string _currentAreaId = areaSelectionPanel.currentAreaId;
                Vector3Int _pasteGridPos = Vector3Int.RoundToInt(Camera.main.ScreenToWorldPoint(Input.mousePosition));

                fileCache.GetTextureFromWeb(_clipboardContent, async delegate (Texture2D _texture, string _imageURI) {
                    string _tokenId = await AddNewToken(_currentAreaId, _pasteGridPos);
                    await WaitForTokenObject(_tokenId);
                    string _characterId = GetTokenCharacterID(_currentAreaId, _tokenId);
                    tokenPortraitEditor.EditCharacterPortrait(_characterId, _imageURI);
                });
            }
        }

        public void AddTokenFromDragAndDrop(string _filepath, Vector3Int _dropPos) {
            string _currentAreaId = areaSelectionPanel.currentAreaId;

            fileCache.UploadTexture(_filepath, async delegate (string _imageURI) {
                string _tokenId = await AddNewToken(_currentAreaId, _dropPos);
                await WaitForTokenObject(_tokenId);
                string _characterId = GetTokenCharacterID(_currentAreaId, _tokenId);
                tokenPortraitEditor.EditCharacterPortrait(_characterId, _imageURI);
            });
        }

        public async void DeleteToken(string _areaId, string _tokenId) {
            if (colyseusClientController.role != Role.GM) return;

            DeselectToken(_tokenId);
            await colyseusClientController.CallService("deleteToken", new {
                areaId = _areaId,
                tokenId = _tokenId
            });
        }

        public async void DeleteSelectedTokens() {
            if (colyseusClientController.role != Role.GM) return;

            string _areaId = areaSelectionPanel.currentAreaId;

            var _deletionTasks = new List<Task<string>>();
            foreach (string _tokenId in selectedTokenIDs) {
                _deletionTasks.Add(colyseusClientController.CallService("deleteToken", new {
                    areaId = _areaId,
                    tokenId = _tokenId
                }));
            }

            DeselectAllTokens();
            await Task.WhenAll(_deletionTasks);
        }

        public void DeselectAllTokens() {
            string _areaId = areaSelectionPanel.currentAreaId;

            var _oldSelectedTokenIDs = selectedTokenIDs;
            selectedTokenIDs = new List<string>();

            foreach (string _tokenId in _oldSelectedTokenIDs) {
                DeHighlightToken(_tokenId);
                onTokenDeselected.Invoke(_areaId, _tokenId);
            }

            _oldSelectedTokenIDs.Clear();
        }

        public void DeselectToken(string _tokenId) {
            string _areaId = areaSelectionPanel.currentAreaId;

            DeHighlightToken(_tokenId);
            selectedTokenIDs.Remove(_tokenId);

            onTokenDeselected.Invoke(_areaId, _tokenId);
        }

        public void DragSelectedTokens() {
            foreach (string _tokenId in selectedTokenIDs) {
                DragToken(_tokenId);
            }
        }

        public async void DropSelectedTokens(bool _return = true) {
            var _dropTasks = new List<Task>();
            foreach (string _tokenId in selectedTokenIDs) {
                _dropTasks.Add(DropTokenAsync(_tokenId));
            }
            // TODO: Improve error handling - roll all moves back if something goes wrong
            await Task.WhenAll(_dropTasks);
        }

        public void DragToken(string _tokenId) {
            Token _token = tokenObjectsById[_tokenId];
            DragAndDrop _dd = _token.GetComponent<DragAndDrop>();
            if (!_dd) LogWarning("No DragAndDrop component attached to the token");

            _dd.Drag();
        }

        public async Task<Vector3> DropTokenAsync(string _tokenId, bool _move = false) {
            Token _token = tokenObjectsById[_tokenId];
            DragAndDrop _dd = _token.GetComponent<DragAndDrop>();
            if (!_dd) LogWarning("No DragAndDrop component attached to the token");

            Vector3 _dropPosition = _dd.Drop(!_move);
            if (_move) {
                try {
                    bool _moved = await MoveToken(areaSelectionPanel.currentAreaId, _tokenId, _dropPosition);
                    // If no move was made, snap the token back to its previous position.
                    if (!_moved) _dd.Return();
                } catch {
                    // Something went wrong, for example the move was illegal. Snap the token
                    //  back to its previous position.
                    _dd.Return();
                }
            }
            return _dropPosition;
        }

        // Sometimes we want to just run the task and forget about it.
        public async void DropToken(string _tokenId, bool _move = false) {
            await DropTokenAsync(_tokenId, _move);
        }

        public void EditSelectedTokenCharacter() {
            if (selectedTokenIDs.Count == 0) LogWarning("No tokens are selected to edit");
            if (selectedTokenIDs.Count > 1) LogWarning("Multiple tokens selected, only one can be edited at a time");

            string _tokenId = selectedTokenIDs[0];
            string _characterId = GetTokenCharacterID(areaSelectionPanel.currentAreaId, _tokenId);
            characterStatePanel.EditCharacter(_characterId);
        }

        public void EditSelectedTokenCharacterOwner() {
            if (selectedTokenIDs.Count == 0) LogWarning("No tokens are selected to edit");
            if (selectedTokenIDs.Count > 1) LogWarning("Multiple tokens selected, only one can be edited at a time");

            string _tokenId = selectedTokenIDs[0];
            string _characterId = GetTokenCharacterID(areaSelectionPanel.currentAreaId, _tokenId);
            characterOwnerPanel.EditCharacterOwner(_characterId);
        }

        public void EditSelectedTokenCharacterPortrait() {
            if (selectedTokenIDs.Count == 0) LogWarning("No tokens are selected to edit");
            if (selectedTokenIDs.Count > 1) LogWarning("Multiple tokens selected, only one can be edited at a time");

            string _tokenId = selectedTokenIDs[0];
            string _characterId = GetTokenCharacterID(areaSelectionPanel.currentAreaId, _tokenId);
            tokenPortraitEditor.EditCharacterPortrait(_characterId);
        }

        public string GetIdOfTokenUnderMouse() {
            foreach (var _pair in tokenObjectsById) {
                string _tokenId = _pair.Key;
                Token _token = _pair.Value;
                if (IsMouseOverToken(_token)) return _tokenId;
            }
            return "";
        }

        public ReadOnlyCollection<string> GetSelectedTokenIDs() {
            return selectedTokenIDs.AsReadOnly();
        }

        public void HideSelectedTokens() {
            SetSelectedTokensVisibility(false);
        }

        public bool IsMouseOverAnyToken() {
            return GetIdOfTokenUnderMouse() == "";
        }

        public bool IsMouseOverToken(Token _token) {
            // Mouse can never be over a disabled object
            if (!_token.gameObject.activeSelf) return false;

            Vector3 _mousePosWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            float _lowX = _token.transform.position.x - 0.5f;
            float _highX = _token.transform.position.x + 0.5f;
            float _lowY = _token.transform.position.y - 0.5f;
            float _highY = _token.transform.position.y + 0.5f;
            return (
                _lowX <= _mousePosWorld.x && _mousePosWorld.x <= _highX &&
                _lowY <= _mousePosWorld.y && _mousePosWorld.y <= _highY
            );
        }

        public async Task<bool> MoveToken(string _areaId, string _tokenId, Vector3 _position) {
            Vector3Int _snappedPos = Vector3Int.RoundToInt(_position);

            // Perhaps there is no move to be made
            Vector3 _tokenPos = GetTokenCurrentPosition(_areaId, _tokenId);
            Vector3Int _snappedTokenPos = Vector3Int.RoundToInt(_tokenPos);
            if (_snappedTokenPos.x == _snappedPos.x && _snappedTokenPos.y == _snappedPos.y) {
                return false;
            }

            await colyseusClientController.CallService("moveToken", new {
                areaId = _areaId,
                tokenId = _tokenId,
                position = new {
                    x = _snappedPos.x,
                    y = _snappedPos.y
                }
            });
            return true;
        }

        public bool IsTokenControllableByLocal(string _areaId, string _tokenId) {
            // Tokens that don't exist cannot ever be controlled by anybody
            if (_areaId == "" || _tokenId == "") return false;

            // The GM can always attempt to manipulate everything
            if (colyseusClientController.role == Role.GM) return true;

            // The players can only control tokens representing characters that they own
            if (colyseusClientController.role == Role.Player) {
                CampaignState _state = colyseusClientController.GetLocalState();
                string _sessionId = colyseusClientController.GetLocalSessionId();
                return _state.IsPlayerTokenOwner(_areaId, _tokenId, _sessionId);
            }

            return false;
        }

        public bool IsTokenControllableByLocal(string _tokenId) {
            return IsTokenControllableByLocal(areaSelectionPanel.currentAreaId, _tokenId);
        }

        public bool IsTokenSelected(string _tokenId) {
            return selectedTokenIDs.Contains(_tokenId);
        }

        public void SelectTokens(List<string> _tokenIDs, bool _add = false) {
            if (!_add) {
                DeselectAllTokens();
            }
            foreach (string _tokenId in _tokenIDs) {
                SelectToken(_tokenId, _add: true);
            }
        }

        public void SelectToken(string _tokenId, bool _add = false) {
            string _areaId = areaSelectionPanel.currentAreaId;

            if (!_add) {
                DeselectAllTokens();
            }
            if (_tokenId != "") {
                selectedTokenIDs.Add(_tokenId);
                HighlightToken(_tokenId);
                onTokenSelected.Invoke(_areaId, _tokenId);
            }
        }

        public async void SetSelectedTokensVisibility(bool _visible) {
            string _areaId = areaSelectionPanel.currentAreaId;

            var _visibilityTasks = new List<Task>();
            foreach (string _tokenId in selectedTokenIDs) {
                _visibilityTasks.Add(SetTokenVisibility(_areaId, _tokenId, _visible));
            }
            await Task.WhenAll(_visibilityTasks);
        }

        public async Task SetTokenVisibility(string _areaId, string _tokenId, bool _visible) {
            await colyseusClientController.CallService("setTokenVisibility", new {
                areaId = _areaId,
                tokenId = _tokenId,
                visible = _visible
            });
        }

        public void UnhideSelectedTokens() {
            SetSelectedTokensVisibility(true);
        }

        public void UpdateForAreaView(string _viewedAreaId, bool _clearCurrentArea=false) {
            DeselectAllTokens();

            if (_clearCurrentArea) ClearCurrentAreaTokens();

            var _areas = colyseusClientController.GetLocalState().areas;
            _areas.ForEach(delegate (string _tokenAreaId, AreaState _area) {
                _area.tokens.ForEach(async delegate (string _tokenId, TokenState _token) {
                    await WaitForTokenObject(_token.id);
                    UpdateTokenDisplay(_token, _viewedAreaId, _tokenAreaId);
                });
            });
        }
    #endregion
 
    #region Private Functions
        private void CheckIntegrity() {
            if (!colyseusClientController) LogWarning("No colyseusClientController attached");
            if (!grid) LogWarning("No gridManager attached");
            if (!fileCache) LogWarning("No fileCache attached");
            if (!fileDragAndDrop) LogWarning("No fileDragAndDrop attached");
            if (!areaSelectionPanel) LogWarning("No areaSelectionPanel attached");
            if (!characterStatePanel) LogWarning("No characterStatePanel attached");
            if (!characterOwnerPanel) LogWarning("No characterOwnerPanel attached");
            if (!tokenPortraitEditor) LogWarning("No tokenPortraitEditorPanel attached");
            if (!gridTileContextMenu) LogWarning("No gridTileContextMenu attached");
            if (!tokenTemplate) LogWarning("No tokenTemplate attached");
        }

        private void ClearCurrentAreaTokens() {
            HashSet<string> _idsToRemove = new HashSet<string>();
            HashSet<GameObject> _objsToDestroy = new HashSet<GameObject>();

            foreach (KeyValuePair<string, Token> _kvp in tokenObjectsById) {
                string _tokenId = _kvp.Key;
                Token _token = _kvp.Value;

                if (_token.gameObject.activeSelf) {
                    _idsToRemove.Add(_tokenId);
                    _objsToDestroy.Add(_token.gameObject);
                }
            }

            foreach (string _tokenId in _idsToRemove) tokenObjectsById.Remove(_tokenId);
            foreach (GameObject _obj in _objsToDestroy) Destroy(_obj);
        }

        private GameObject CreateTokenObject(TokenState _token, string _areaId) {
            Vector3 _position = new Vector3(
                x: _token.pos.x,
                y: _token.pos.y
            );
            GameObject _obj = UnityEngine.Object.Instantiate(tokenTemplate, gameObject.transform);
            _obj.transform.position = _position;

            Token _tokenComponent = _obj.GetComponent<Token>();
            if (!_tokenComponent) LogWarning("There is no Token component in the Token parent object");

            tokenObjectsById.Add(_token.id, _tokenComponent);

            string _characterId = GetTokenCharacterID(_areaId, _token.id);
            CharacterState _character = colyseusClientController.GetLocalState().characters[_characterId];
            UpdateTokenPortrait(_token.id, _character.portrait);

            UpdateTokenDisplay(_token, _areaId);

            _obj.name = "Token " + _token.id;
            onTokenObjectCreated.Invoke(_token.id);
            
            return _obj;
        }

        private void DeHighlightToken(string _tokenId) {
            Token _token = tokenObjectsById[_tokenId];
            SpriteRenderer _renderer = _token.GetComponent<SpriteRenderer>();
            _renderer.material = defaultTokenMaterial;
            _token.portraitRenderer.material = defaultTokenMaterial;
        }

        private bool GetShouldTokenBeDisplayed(TokenState _token, string _currentAreaId, string _tokenAreaId) {
            bool _display = _tokenAreaId == _currentAreaId && (colyseusClientController.role == Role.GM || !_token.visibleToGmOnly);
            return _display;
        }

        private string GetTokenCharacterID(string _areaId, string _tokenId) {
            AreaState _area = colyseusClientController.GetLocalState().areas[_areaId];
            TokenState _token = _area.tokens[_tokenId];
            return _token.characterId;
        }

        private Vector3 GetTokenCurrentPosition(string _areaId, string _tokenId) {
            AreaState _area = colyseusClientController.GetLocalState().areas[_areaId];
            TokenState _token = _area.tokens[_tokenId];
            Token _tokenObj = tokenObjectsById[_tokenId];
            
            return new Vector3(
                x: _token.pos.x,
                y: _token.pos.y,
                z: _tokenObj.transform.position.z
            );
        }

        private void HandleServerAddToken(TokenState _token, string _tokenId, string _areaId) {
            CreateTokenObject(_token, _areaId);
        }

        private void HandleServerFirstState(CampaignState _state) {
            _state.areas.ForEach(delegate (string _areaId, AreaState _area) {
                _area.tokens.ForEach(delegate (string _tokenId, TokenState _token) {
                    HandleServerAddToken(_token, _tokenId, _areaId);
                });
            });
        }

        private void HandleServerRemoveToken(TokenState _token, string _tokenId, string _areaId) {
            Token _tokenObj = tokenObjectsById[_tokenId];
            tokenObjectsById.Remove(_tokenId);
            Destroy(_tokenObj.gameObject);
        }

        private void HandleServerPortraitChanges(string _characterId) {
            CampaignState _state = colyseusClientController.GetLocalState();
            CharacterState _character = _state.characters[_characterId];
            PortraitState _portrait = _character.portrait;

            _character.representedByTokens.ForEach(delegate (string _tokenId, string _areaId) {
                // Update all existing token objects. We don't care about non-existing ones,
                //  these should see this stuff handled on creation.
                if (tokenObjectsById.ContainsKey(_tokenId)) {
                    UpdateTokenPortrait(_tokenId, _portrait);
                }
            });
        }

        private void HandleServerTokenChange(string _areaId, string _tokenId, Colyseus.Schema.DataChange _change) {
            AreaState _area = colyseusClientController.GetLocalState().areas[_areaId];
            TokenState _token = _area.tokens[_tokenId];
            //GameObject _tokenObj = tokenObjectsById[_tokenId];

            switch(_change.Field) {
                case "visibleToGmOnly":
                    UpdateTokenDisplay(_token, _areaId);
                    break;
                default:
                    break;
            }
        }

        private void HandleServerTokenPositionChange(string _areaId, string _tokenId, Colyseus.Schema.DataChange _change) {
            AreaState _area = colyseusClientController.GetLocalState().areas[_areaId];
            TokenState _token = _area.tokens[_tokenId];
            Token _tokenObj = tokenObjectsById[_tokenId];

            // We don't care what the exact changes are, just update the whole position
            _tokenObj.transform.position = new Vector3(
                x: _token.pos.x,
                y: _token.pos.y,
                z: _tokenObj.transform.position.z
            );
        }

        private void HighlightToken(string _tokenId) {
            Token _token = tokenObjectsById[_tokenId];
            SpriteRenderer _renderer = _token.GetComponent<SpriteRenderer>();
            _renderer.material = selectedTokenMaterial;
            _token.portraitRenderer.material = selectedTokenMaterial;
        }

        private void RegisterFileDragAndDropListener() {
            fileDragAndDrop.onFilesDropped.AddListener(delegate (List<string> _filePaths, Vector3 _mousePos) {
                // It's only allowed to drop *exactly one* file
                if (_filePaths.Count != 1) return;

                Vector3 _mouseWorldPos = Camera.main.ScreenToWorldPoint(_mousePos);

                // Only allow dropping on grid tiles
                if (!grid.IsPosOnGrid(_mouseWorldPos)) return;

                string _filepath = _filePaths[0];
                Vector3Int _snappedMouseWorldPos = Vector3Int.RoundToInt(_mouseWorldPos);

                AddTokenFromDragAndDrop(_filepath, _snappedMouseWorldPos);
            });
        }

        private void RegisterTokenCollectionStateListeners() {
            colyseusClientController.emitter.onAddToken.AddListener(delegate (TokenState _token, string _tokenId, string _areaId) {
                HandleServerAddToken(_token, _tokenId, _areaId);
            });
            colyseusClientController.emitter.onRemoveToken.AddListener(delegate (TokenState _token, string _tokenId, string _areaId) {
                HandleServerRemoveToken(_token, _tokenId, _areaId);
            });
            colyseusClientController.emitter.onTokenChanged.AddListener(delegate (string _areaId, string _tokenId, List<Colyseus.Schema.DataChange> _changes) {
                foreach (var _change in _changes) HandleServerTokenChange(_areaId, _tokenId, _change);
            });
            colyseusClientController.emitter.onTokenPositionChanged.AddListener(delegate (string _areaId, string _tokenId, List<Colyseus.Schema.DataChange> _changes) {
                foreach (var _change in _changes) HandleServerTokenPositionChange(_areaId, _tokenId, _change);
            });
            colyseusClientController.emitter.onCharacterPortraitChanged.AddListener(delegate (string _characterId, List<Colyseus.Schema.DataChange> _changes) {
                HandleServerPortraitChanges(_characterId);
            });
        }

        private void UpdateTokenDisplay(TokenState _token, string _currentAreaId, string _tokenAreaId) {
            Token _tokenObj = tokenObjectsById[_token.id];

            UpdateTokenOpactity(_token);
            bool _display = GetShouldTokenBeDisplayed(_token, _currentAreaId, _tokenAreaId);
            _tokenObj.gameObject.SetActive(_display);
        }
        private void UpdateTokenDisplay(TokenState _token, string _tokenAreaId) {
            UpdateTokenDisplay(_token, areaSelectionPanel.currentAreaId, _tokenAreaId);
        }

        private void UpdateTokenOpactity(TokenState _token) {
            Token _tokenObj = tokenObjectsById[_token.id];
            SpriteRenderer _renderer = _tokenObj.GetComponent<SpriteRenderer>();

            float _alpha = _token.visibleToGmOnly ? hiddenTokensAlpha : 1.0f;
            _tokenObj.SetOpacity(_alpha);
        }

        private void UpdateTokenPortrait(string _tokenId, PortraitState _portrait) {
            Token _tokenObj = tokenObjectsById[_tokenId];

            // If the "portrait" is actually empty, revert to the default generic character image
            if (_portrait.imageURI == null || _portrait.imageURI == "") {
                _tokenObj.ResetPortraitToDefault();
            } else {
                Vector3 _tokenOffset = new Vector3(_portrait.tokenOffset.x, _portrait.tokenOffset.y, 0);
                fileCache.GetTexture(_portrait.imageURI, delegate (Texture2D _texture) {
                    _tokenObj.SetPortrait(_texture, _tokenOffset, _portrait.tokenScale);
                });
            }
        }

        private async Task WaitForTokenObject(string _tokenId) {
            var _tcs = new TaskCompletionSource<bool>();

            if (tokenObjectsById.ContainsKey(_tokenId)) {
                _tcs.SetResult(true);
                await _tcs.Task;
            } else {
                UnityAction<string> _listener = delegate (string _id) {
                    if (_id == _tokenId) {
                        _tcs.SetResult(true);
                    }
                };
                onTokenObjectCreated.AddListener(_listener);

                await _tcs.Task;
                onTokenObjectCreated.RemoveListener(_listener);
            }
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
            RegisterTokenCollectionStateListeners();
            RegisterFileDragAndDropListener();
        }

        private void Start() {
            HandleServerFirstState(colyseusClientController.GetLocalState());
        }
    #endregion
    }
}