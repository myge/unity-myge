﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {

    public static class FogSpriteGenerator {
        public static int previewTextureResolution = 200;

    #region Public Functions
        public static Sprite GetWallPreviewSprite() {
            int _res = previewTextureResolution;
            Texture2D _wallTex = GetWallPreviewTexture();

            Sprite _wall = Sprite.Create(
                texture: _wallTex,
                rect: new Rect(0, 0, _res, _res),
                pivot: new Vector2(0, 0),
                pixelsPerUnit: _res,
                extrude: 0,
                meshType: SpriteMeshType.FullRect
            );

            return _wall;
        }

        public static Texture2D GetWallPreviewTexture() {
            int _res = previewTextureResolution;
            Texture2D _wallTex = new Texture2D(_res, _res);

            Color _white = new Color(1, 1, 1, 1);

            for (int x = 0; x < _res; x++) {
                for (int y = 0; y < _res; y++) {
                    _wallTex.SetPixel(x, y, _white);
                }
            }

            _wallTex.Apply();
            return _wallTex;
        }
    #endregion

    #region Private Functions
        
    #endregion
    }
}