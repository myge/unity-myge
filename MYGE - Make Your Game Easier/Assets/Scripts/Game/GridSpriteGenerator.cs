﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {

    public static class GridSpriteGenerator {
        // Thickness in this case is the thickness *on the texture*,
        //  so actual thickness divided by 2.
        private static Dictionary<int, GridSpriteSet> spritesByThickness = new Dictionary<int, GridSpriteSet>();
        private static Dictionary<int, GridTextureSet> texturesByThickness = new Dictionary<int, GridTextureSet>();

        public static int tileTextureResolution = 200;

    #region Public Functions
        public static GridSpriteSet GetGridSpriteSet(int _edgeThickness) {
            // Tiles will be touching each other, so the effective thickness would be doubled,
            //  hence the division by 2.
            int _thickness = Mathf.RoundToInt(_edgeThickness / 2);
            if (_thickness < 1) _thickness = 1;

            return _GetGridSpriteSet(_thickness);
        }

        public static GridTextureSet GetGridTextureSet(int _edgeThickness) {
            int _thickness = Mathf.RoundToInt(_edgeThickness / 2);
            if (_thickness < 1) _thickness = 1;

            return _GetGridTextureSet(_thickness);
        }
    #endregion

    #region Private Functions
        private static GridSpriteSet _GetGridSpriteSet(int _thickness) {
            // Generate sprite sets as required
            if (!spritesByThickness.ContainsKey(_thickness)) {
                GridSpriteSet _sprites = GenerateGridSprites(_thickness);
                spritesByThickness.Add(_thickness, _sprites);
                return _sprites;
            } else {
                GridSpriteSet _sprites = spritesByThickness[_thickness];
                return _sprites;
            }
        }

        private static GridTextureSet _GetGridTextureSet(int _thickness) {
            // Generate texture sets as required
            if (!texturesByThickness.ContainsKey(_thickness)) {
                GridTextureSet _textures = GenerateGridTextures(_thickness);
                texturesByThickness.Add(_thickness, _textures);
                return _textures;
            } else {
                GridTextureSet _textures = texturesByThickness[_thickness];
                return _textures;
            }
        }

        private static GridTextureSet GenerateGridTextures(int _thickness) {
            GridTextureSet _textures = new GridTextureSet();
            _textures.tile = new Texture2D(tileTextureResolution, tileTextureResolution);
            _textures.leftEdge = new Texture2D(tileTextureResolution, tileTextureResolution);
            _textures.rightEdge = new Texture2D(tileTextureResolution, tileTextureResolution);
            _textures.topEdge = new Texture2D(tileTextureResolution, tileTextureResolution);
            _textures.bottomEdge = new Texture2D(tileTextureResolution, tileTextureResolution);
            _textures.corner00 = new Texture2D(tileTextureResolution, tileTextureResolution);
            _textures.corner01 = new Texture2D(tileTextureResolution, tileTextureResolution);
            _textures.corner10 = new Texture2D(tileTextureResolution, tileTextureResolution);
            _textures.corner11 = new Texture2D(tileTextureResolution, tileTextureResolution);

            Color _white = new Color(1, 1, 1, 1);
            Color _trans = new Color(1, 1, 1, 0);
            
            for (int x = 0; x < tileTextureResolution; x++) {
                for (int y = 0; y < tileTextureResolution; y++) {
                    // Left edge
                    if (x < _thickness) {
                        _textures.tile.SetPixel(x, y, _white);
                        _textures.leftEdge.SetPixel(x, y, _white);

                        // Top-left corner
                        if (y >= tileTextureResolution - _thickness) {
                            _textures.corner00.SetPixel(x, y, _trans);
                            _textures.corner01.SetPixel(x, y, _white);
                            _textures.corner10.SetPixel(x, y, _trans);
                            _textures.corner11.SetPixel(x, y, _trans);

                            _textures.topEdge.SetPixel(x, y, _white);
                            _textures.rightEdge.SetPixel(x, y, _trans);
                            _textures.bottomEdge.SetPixel(x, y, _trans);
                        }
                        // Bottom-left corner
                        else if (y < _thickness) {
                            _textures.corner00.SetPixel(x, y, _white);
                            _textures.corner01.SetPixel(x, y, _trans);
                            _textures.corner10.SetPixel(x, y, _trans);
                            _textures.corner11.SetPixel(x, y, _trans);

                            _textures.bottomEdge.SetPixel(x, y, _white);
                            _textures.rightEdge.SetPixel(x, y, _trans);
                            _textures.topEdge.SetPixel(x, y, _trans);
                        }
                        // Left edge proper
                        else {
                            _textures.corner00.SetPixel(x, y, _trans);
                            _textures.corner01.SetPixel(x, y, _trans);
                            _textures.corner10.SetPixel(x, y, _trans);
                            _textures.corner11.SetPixel(x, y, _trans);

                            _textures.rightEdge.SetPixel(x, y, _trans);
                            _textures.bottomEdge.SetPixel(x, y, _trans);
                            _textures.topEdge.SetPixel(x, y, _trans);
                        }
                    }

                    // Right edge
                    else if (x >= tileTextureResolution - _thickness) {
                        _textures.tile.SetPixel(x, y, _white);
                        _textures.rightEdge.SetPixel(x, y, _white);

                        // Top-right corner
                        if (y >= tileTextureResolution - _thickness) {
                            _textures.corner00.SetPixel(x, y, _trans);
                            _textures.corner01.SetPixel(x, y, _trans);
                            _textures.corner10.SetPixel(x, y, _trans);
                            _textures.corner11.SetPixel(x, y, _white);

                            _textures.topEdge.SetPixel(x, y, _white);
                            _textures.leftEdge.SetPixel(x, y, _trans);
                            _textures.bottomEdge.SetPixel(x, y, _trans);
                        }
                        // Bottom-right corner
                        else if (y < _thickness) {
                            _textures.corner00.SetPixel(x, y, _trans);
                            _textures.corner01.SetPixel(x, y, _trans);
                            _textures.corner10.SetPixel(x, y, _white);
                            _textures.corner11.SetPixel(x, y, _trans);

                            _textures.bottomEdge.SetPixel(x, y, _white);
                            _textures.leftEdge.SetPixel(x, y, _trans);
                            _textures.topEdge.SetPixel(x, y, _trans);
                        }
                        // Right edge proper
                        else {
                            _textures.corner00.SetPixel(x, y, _trans);
                            _textures.corner01.SetPixel(x, y, _trans);
                            _textures.corner10.SetPixel(x, y, _trans);
                            _textures.corner11.SetPixel(x, y, _trans);

                            _textures.leftEdge.SetPixel(x, y, _trans);
                            _textures.bottomEdge.SetPixel(x, y, _trans);
                            _textures.topEdge.SetPixel(x, y, _trans);
                        }
                    }

                    // Top edge
                    else if (y >= tileTextureResolution - _thickness) {
                        _textures.tile.SetPixel(x, y, _white);
                        _textures.leftEdge.SetPixel(x, y, _trans);
                        _textures.rightEdge.SetPixel(x, y, _trans);
                        _textures.topEdge.SetPixel(x, y, _white);
                        _textures.bottomEdge.SetPixel(x, y, _trans);

                        _textures.corner00.SetPixel(x, y, _trans);
                        _textures.corner01.SetPixel(x, y, _trans);
                        _textures.corner10.SetPixel(x, y, _trans);
                        _textures.corner11.SetPixel(x, y, _trans);
                    }

                    // Bottom edge
                    else if (y < _thickness) {
                        _textures.tile.SetPixel(x, y, _white);
                        _textures.leftEdge.SetPixel(x, y, _trans);
                        _textures.rightEdge.SetPixel(x, y, _trans);
                        _textures.topEdge.SetPixel(x, y, _trans);
                        _textures.bottomEdge.SetPixel(x, y, _white);

                        _textures.corner00.SetPixel(x, y, _trans);
                        _textures.corner01.SetPixel(x, y, _trans);
                        _textures.corner10.SetPixel(x, y, _trans);
                        _textures.corner11.SetPixel(x, y, _trans);
                    }

                    // Center
                    else {
                        _textures.tile.SetPixel(x, y, _trans);
                        _textures.leftEdge.SetPixel(x, y, _trans);
                        _textures.rightEdge.SetPixel(x, y, _trans);
                        _textures.topEdge.SetPixel(x, y, _trans);
                        _textures.bottomEdge.SetPixel(x, y, _trans);

                        _textures.corner00.SetPixel(x, y, _trans);
                        _textures.corner01.SetPixel(x, y, _trans);
                        _textures.corner10.SetPixel(x, y, _trans);
                        _textures.corner11.SetPixel(x, y, _trans);
                    }
                }
            }

            _textures.tile.Apply();
            _textures.leftEdge.Apply();
            _textures.rightEdge.Apply();
            _textures.topEdge.Apply();
            _textures.bottomEdge.Apply();

            _textures.corner00.Apply();
            _textures.corner01.Apply();
            _textures.corner10.Apply();
            _textures.corner11.Apply();

            return _textures;
        }
    
        private static GridSpriteSet GenerateGridSprites(int _thickness) {
            GridSpriteSet _sprites = new GridSpriteSet();

            int _res = tileTextureResolution;
            GridTextureSet _textures = _GetGridTextureSet(_thickness);
            
            _sprites.tile = Sprite.Create(
                texture: _textures.tile,
                rect: new Rect(0, 0, _res, _res),
                pivot: new Vector2(0, 0),
                pixelsPerUnit: _res,
                extrude: 0,
                meshType: SpriteMeshType.FullRect
            );

            _sprites.leftEdge = Sprite.Create(
                texture: _textures.leftEdge,
                rect: new Rect(0, 0, _res / 2, _res),
                pivot: new Vector2(0, 0),
                pixelsPerUnit: _res,
                extrude: 0,
                meshType: SpriteMeshType.FullRect
            );

            _sprites.rightEdge = Sprite.Create(
                texture: _textures.rightEdge,
                rect: new Rect(_res / 2, 0, _res / 2, _res),
                pivot: new Vector2(-1, 0),
                pixelsPerUnit: _res,
                extrude: 0,
                meshType: SpriteMeshType.FullRect
            );

            _sprites.topEdge = Sprite.Create(
                texture: _textures.topEdge,
                rect: new Rect(0, _res / 2, _res, _res / 2),
                pivot: new Vector2(0, -1),
                pixelsPerUnit: _res,
                extrude: 0,
                meshType: SpriteMeshType.FullRect
            );

            _sprites.bottomEdge = Sprite.Create(
                texture: _textures.bottomEdge,
                rect: new Rect(0, 0, _res, _res / 2),
                pivot: new Vector2(0, 0),
                pixelsPerUnit: _res,
                extrude: 0,
                meshType: SpriteMeshType.FullRect
            );

            _sprites.corner00 = Sprite.Create(
                texture: _textures.corner00,
                rect: new Rect(0, 0, _res / 2, _res / 2),
                pivot: new Vector2(0, 0),
                pixelsPerUnit: _res,
                extrude: 0,
                meshType: SpriteMeshType.FullRect
            );
            
            _sprites.corner01 = Sprite.Create(
                texture: _textures.corner01,
                rect: new Rect(0, _res / 2, _res / 2, _res / 2),
                pivot: new Vector2(0, -1),
                pixelsPerUnit: _res,
                extrude: 0,
                meshType: SpriteMeshType.FullRect
            );

            _sprites.corner10 = Sprite.Create(
                texture: _textures.corner10,
                rect: new Rect(_res / 2, 0, _res / 2, _res / 2),
                pivot: new Vector2(-1, 0),
                pixelsPerUnit: _res,
                extrude: 0,
                meshType: SpriteMeshType.FullRect
            );

            _sprites.corner11 = Sprite.Create(
                texture: _textures.corner11,
                rect: new Rect(_res / 2, _res / 2, _res / 2, _res / 2),
                pivot: new Vector2(-1, -1),
                pixelsPerUnit: _res,
                extrude: 0,
                meshType: SpriteMeshType.FullRect
            );
            
            return _sprites;
        }
    #endregion
    }
}