﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils {

    public static class CameraExtensions {
        public static float zoomSensitivity = 2.0f;
        
        public static void Zoom(this Camera _cam, float _deltaZoomPercentage, float _minZoomPercentage = 0.2f, float _maxZoomPercentage = 2.0f) {
            if (_cam.orthographic) {
                // The camera size is inversely proportional to the amount of zoom (in percents). Linearly changing the
                //  size with respect to, for example, a constant scrolling rate, results in an annoyingly slow velocity
                //  the farther out we zoom. The equation below makes it, so that we always zoom in/out a constant amount
                //  of percentage points.
                // This tends to +Infinity or -Infinity for _cam.orthographicSize == 50. The infinity is not a problem,
                //  but there are negative numbers very close to each other. The result could randomly come out negative
                //  and later get clamped to the minimum instead of the maximum. The absolute value solves that (the camera
                //  size should never be negative anyway).
                float _targetSize = Mathf.Abs((5 * _cam.orthographicSize) / (5 + _deltaZoomPercentage * _cam.orthographicSize));
                float _closestZoom = 5 / _maxZoomPercentage;
                float _farthestZoom = 5 / _minZoomPercentage;
                _cam.orthographicSize = Mathf.Clamp(_targetSize, _closestZoom, _farthestZoom);
            } else {
                throw new System.NotImplementedException("Zoom() camera extension method only supports ortographic cameras");
            }
        }

        public static void MoveClampedXY(this Camera _cam, Vector3 _deltaPosition, Vector3 _bottomLeftCorner, Vector3 _topRightCorner) {
            Vector3 _oldPos = _cam.transform.position;
            Vector3 _newPos = new Vector3(
                x: Mathf.Clamp(_oldPos.x + _deltaPosition.x, _bottomLeftCorner.x, _topRightCorner.x),
                y: Mathf.Clamp(_oldPos.y + _deltaPosition.y, _bottomLeftCorner.y, _topRightCorner.y),
                z: _oldPos.z
            );
            _cam.transform.position = _newPos;
        }
    }
}