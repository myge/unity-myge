﻿using Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils {

    public class ParsedRollGroup {
        public List<DiceRollResult.IndividualRoll> rolls = new List<DiceRollResult.IndividualRoll>();
        public int modifier;

    #region Public Methods
        public ParsedRollGroup(string _rollStr) {
            // Sometimes people may write "d20 + 3", or even make typos
            //  like "d20+ 3". Dropping every single spaxce should fix that.
            _rollStr = _rollStr.Replace(" ", "");

            // Given how we are about to split with respect to "+", replace
            //  all subtraction operators with addition of a negative number.
            // When the "+" is dropped as a split delimiter, positive numbers
            //  will be preceded by nothing (which is good), and negative
            //  numbers will still each be preceded by a "-".
            _rollStr = _rollStr.Replace("-", "+-");

            // Split with respect to "+"
            string[] _terms = _rollStr.Split(new string[] {"+"}, System.StringSplitOptions.RemoveEmptyEntries);
            
            // All the constant terms will be later summed as one modifier.
            modifier = 0;

            foreach (string _term in _terms) {
                // Split each term with respect to the "d" (dice) operator.
                // This makes it easy to detect what is on the left side of "d",
                //  and what is on the right side of "d".
                // The lack of empty entries removal lets one check if either
                //  side of "d" is empty.
                string[] _dargs = _term.Split(new string[] {"d"}, 2, System.StringSplitOptions.None);
                
                if (_dargs.Length == 1) {
                    // Only one result indicates that it's a constant term
                    int _constant = 0;
                    int.TryParse(_dargs[0].Trim(), out _constant);
                    modifier += _constant;
                } else if (_dargs.Length == 2) {
                    // Two results hints that there is a proper "d" notation,
                    //  thus this term is a die roll.
                    string _timesStr = _dargs[0];
                    string _sidesStr = _dargs[1];

                    // I apologize, but I couldn't help myself and had to make
                    //  this joke.
                    if (_sidesStr == "x") {
                        throw new System.ArgumentException("This software does not support differential calculus");
                    }

                    // A left side written as "-" (as in "-d20") should be
                    //  understood as "-1". All other negative values will
                    //  be caught by int.TryParse().
                    if (_timesStr == "-") _timesStr = "-1";

                    // If the TryParse succeeds, use the parsed int, otherwise
                    //  default to 1.
                    int _times = int.TryParse(_timesStr, out _times) ? _times : 1;

                    // If the TryParse on sides fails, consider it an unknown
                    //  die type.
                    int _sides;
                    if (!int.TryParse(_sidesStr, out _sides)) {
                        throw new System.ArgumentException($"Unrecognized die \"d{_sidesStr}\"");
                    }

                    if (_sides < 1) {
                        // Dice with non-positive numbers of sides are illegal
                        throw new System.ArgumentException("There is no such thing as dice with zero or fewer sides");
                    }

                    DiceRollResult.IndividualRoll _dieRoll = new DiceRollResult.IndividualRoll(_times, _sides);
                    rolls.Add(_dieRoll);
                }
            }
        }

        public override string ToString() {
            List<string> _terms = new List<string>();
            foreach (DiceRollResult.IndividualRoll _die in rolls) {
                _terms.Add($"{_die.times}d{_die.sides}");
            }
            _terms.Add(modifier.ToString());
            
            string _rollStr = string.Join("+", _terms);

            // A negative number, for example -3, converts to a string of
            //  "-3" (obviously). This means that the above join operation
            //  with "+" as delimiter will create a notation of, for example,
            //  1d20+-3, hence all "+-" must be replaced with "-".
            _rollStr = _rollStr.Replace("+-", "-");

            return _rollStr;
        }
    #endregion
    }
}