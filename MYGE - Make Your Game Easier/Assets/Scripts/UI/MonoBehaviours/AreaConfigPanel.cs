﻿using Network;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    
    public class AreaConfigPanel : EnhancedMonoBehaviour {
        private AreaState areaBeingEdited = null;

    #region Unity Editor Fields
        public AreaSettingsPanel areaSettings;
        public InputField nameInput;
    #endregion

    #region Public Functions
        public AreaState CollectAreaConfig() {
            if (areaBeingEdited == null) {
                LogWarning("No area is currently being edited, cannot collect config");
                return null;
            }

            AreaState _area = new AreaState();

            _area.id = areaBeingEdited.id;
            _area.name = nameInput.text;
            _area.areaSettings = areaSettings.CollectAreaSettings();

            return _area;
        }

        public void EditArea(AreaState _area) {
            areaBeingEdited = _area;
            LoadAreaConfig(_area);
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!areaSettings) LogWarning("No areaSettings attached");
            if (!nameInput) LogWarning("No nameInput attached");
        }

        private void LoadAreaConfig(AreaState _area) {
            nameInput.text = _area.name;
            areaSettings.LoadAreaSettings(_area.areaSettings);
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            CheckIntegrity();
        }

        private void OnDisable() {
            areaBeingEdited = null;
        }
    #endregion
    }
}