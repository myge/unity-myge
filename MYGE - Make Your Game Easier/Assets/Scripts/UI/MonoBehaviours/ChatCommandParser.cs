﻿using Network;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI {

    public class ChatCommandParser : EnhancedMonoBehaviour {
        private Dictionary<string, Command> commands = new Dictionary<string, Command>();

    #region Unity Fields
        public ColyseusClientController colyseusClientController;
        public string commandOperator = "/";
        public string commandArgsSeparator = ",";
    #endregion

    #region Public Functions
        public bool IsMessageCommand(string _message) {
            // A message is a command when it starts with the command operator,
            //  *and* there is text past the operator.
            return _message.StartsWith(commandOperator) && _message.Length > commandOperator.Length;
        }

        // Bear in mind this may throw quite a lot of things (Command::Execute() also throws).
        public async Task ParseAndRunCommand(string _message) {
            // Don't accept non-command messages
            if (!IsMessageCommand(_message)) {
                throw new System.ArgumentException("\"" + _message + "\" is not a command.");
            }

            // Get rid of the command operator at the start
            _message = _message.Substring(commandOperator.Length);
            
            // Separate the command itself from its arguments
            string[] _sections = _message.Split(new char[] {' '}, 2);
            string _commandString = _sections[0].Trim();
            string _argsString = _sections.Length > 1 ? _sections[1] : "";

            // Try to find a matching command first before doing anything else
            Command _command = FindMatchingCommand(_commandString);
            if (_command == null) {
                throw new System.ArgumentException("No matching command found for \"" + commandOperator + _commandString + "\"");
            }

            // Split arg string into args and trim them all individually afterwards
            string[] _args = _argsString.Split(new string[] {commandArgsSeparator}, System.StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < _args.Length; i++) {
                _args[i] = _args[i].Trim();
            }

            // Finally, run the command with the given set of args
            Log("Executing command " + commandOperator + _commandString + " with args: " + _argsString);
            await _command.Execute(_args);
        }
    #endregion

    #region Private Functions
        private void InitializeCommands() {
            // All command definitions go here

            AddCommand(new Command("roll", new Command.CommandAction(async delegate (string[] _args) {
                string _rollStr = string.Join("", _args);

                // Parse the joined string
                ParsedRollGroup _roll = new ParsedRollGroup(_rollStr);

                // Request a roll on the server
                await colyseusClientController.CallService("rollDice", new {
                    broadcast = "true", // The results shall be sent to every client
                    rolls = _roll.rolls.ToArray(),
                    modifier = _roll.modifier
                });
            }), _requiredArgs: 1));
        }

        private void AddCommand(Command _command) {
            if (commands.ContainsKey(_command.id)) {
                LogWarning("Cannot add new command " + commandOperator + _command.id + " - it already exists");
                return;
            }

            commands.Add(_command.id, _command);
        }

        private void CheckIntegrity() {
            if (!colyseusClientController) LogWarning("No ColyseusClientController attached");
        }

        private Command FindMatchingCommand(string _commandId) {
            Command _lastMatch = null;
            foreach (Command _command in commands.Values) {
                if (_command.id.StartsWith(_commandId)) {
                    // TODO: Potentially improve, show possible suggestions to the user
                    if (_lastMatch == null) {
                        _lastMatch = _command;
                    } else {
                        // The moment there is more than one match, the given commandId
                        //  should be considered as too ambiguous and rejected.
                        return null;
                    }
                }
            }
            return _lastMatch;
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
            InitializeCommands();
        }
    #endregion
    }
}