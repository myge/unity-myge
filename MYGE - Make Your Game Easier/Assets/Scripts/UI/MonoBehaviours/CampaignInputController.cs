﻿using Game;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityCore.Input;
using UnityCore.Menu;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Utils;

namespace UI {

    public class CampaignInputController : InputController {
        private bool initialized;
        private Vector3 previousMousePosition;
        private Vector3 mouseDelta;
        private Vector3 mouseDeltaWorld;

    #region Unity Fields
        public ChatPanel chatPanel;
        public EventSystem eventSystem;
        public GridManager grid;
        public float keyPressZoomAmount = 0.1f;
        public float mouseClickErrorMargin = 20.0f;
        public ContextObject gridTileContextMenu;
        public ContextObject tokenContextMenu;
        public EscMenuPanel escMenu;
        public TokenManager tokenManager;
        public ToolManager toolManager;
        public GameObject[] eventConsumingOptionPanels;
    #endregion

    #region Private Functions
        private void CloseAllContextMenus() {
            gridTileContextMenu.Close();
            tokenContextMenu.Close();
        }
    
        private void DeFocusChat() {
            GameObject _chatInputObj = chatPanel.chatInput.gameObject;
            _chatInputObj.SetActive(false);
            eventSystem.SetSelectedGameObject(null);
            chatPanel.SetScrollbarVisibility(false);
        }

        private void FocusChat() {
            GameObject _chatInputObj = chatPanel.chatInput.gameObject;
            _chatInputObj.SetActive(true);
            eventSystem.SetSelectedGameObject(_chatInputObj);
            chatPanel.SetScrollbarVisibility(true);
        }

        private void Initialize() {
            if (initialized) return;

            GameObject _chatInputObj = chatPanel.chatInput.gameObject;

            // Telling a click and a drag apart is a bit tricky.
            // The condition for the click is "there was no mouse movement between
            //  pressing and releasing the mouse button". Thus, we need to keep
            //  track of the position at the time of the last press.
            Vector3 _lastLeftMousePressPosition = new Vector3();
            Vector3 _lastRightMousePressPosition = new Vector3();

            AddInputLayer(new InputLayer("mouseScrollOverUI",
                _axisHandlers: new Dictionary<string, AxisHandler> {
                    // Stuff such as scrolling the chat with the pointer over it is already a feature of
                    //  ScrollView. What's left to do is to simply devour all scrollwheel events whenever
                    //  the cursor is over any UI element.
                    ["Mouse ScrollWheel"] = new AxisHandler(delegate {
                        // Eat the event
                        return false;
                    })
                }, _alwaysConsumes: false, _predicate: new Predicate(delegate { return eventSystem.IsPointerOverGameObject(); })
            ));

            // Clicking away from a context menu should close it.
            AddInputLayer(new InputLayer("contextMenus",
                _keyPressHandlers: new Dictionary<KeyCode, InputHandler> {
                    // Eat enter so that chat doesn't get the focus randomly
                    [KeyCode.Return] = new InputHandler(delegate {
                        return false;
                    }),
                    // Eat left click for no token drag-dropping
                    [KeyCode.Mouse0] = new InputHandler(delegate {
                        return false;
                    }),
                    // Right click closes the context menus (and right release
                    //  can later pop them back on from the main layer)
                    [KeyCode.Mouse1] = new InputHandler(delegate {
                        CloseAllContextMenus();
                        return true;
                    })
                },
                _keyReleaseHandlers: new Dictionary<KeyCode, InputHandler> {
                    // Left click release closes context menus
                    [KeyCode.Mouse0] = new InputHandler(delegate {
                        CloseAllContextMenus();
                        return false;
                    })
                }, _alwaysConsumes: false, _predicate: new Predicate(delegate {
                    if (gridTileContextMenu.gameObject.activeSelf) return true;
                    if (tokenContextMenu.gameObject.activeSelf) return true;
                    return false;
                })));

            // Any options window being open should block everything else.
            AddInputLayer(new InputLayer("options",
                _predicate: new Predicate(delegate { return eventConsumingOptionPanels.Any(_obj => _obj.activeInHierarchy); })
            ));

            // The escape menu, aside from consuming everything else, is supposed to close on an Escape key
            AddInputLayer(new InputLayer("escapeMenu",
                _keyPressHandlers: new Dictionary<KeyCode, InputHandler> {
                    [KeyCode.Escape] = new InputHandler(delegate {
                        escMenu.Close();
                        return false;
                    })
                }, _predicate: new Predicate(delegate { return escMenu.gameObject.activeInHierarchy; })
            ));

            // Whenever the user is typing in chat. This is an always consuming layer - nothing should
            //  go off when someone is typing.
            AddInputLayer(new InputLayer("chat",
                _keyPressHandlers: new Dictionary<KeyCode, InputHandler> {
                    // Enter submits the chat message
                    [KeyCode.Return] = new InputHandler(delegate {
                        // Send chat message
                        chatPanel.SendMessage();

                        // De-focus chat
                        DeFocusChat();

                        return false;
                    }),
                    // Escape de-focuses the chat, "exiting" it
                    [KeyCode.Escape] = new InputHandler(delegate {
                        DeFocusChat();
                        return false;
                    })
                }, _predicate: new Predicate(delegate { return _chatInputObj.activeInHierarchy; })
            ));

            // Nothing extraordinary is selected or open - the user "is" in the main view of the game.
            AddInputLayer(new InputLayer("main", 
                _keyPressHandlers: new Dictionary<KeyCode, InputHandler> {
                    // Enter focuses the chat
                    [KeyCode.Return] = new InputHandler(delegate {
                        FocusChat();
                        return false;
                    }),

                    // Classic escape menu
                    [KeyCode.Escape] = new InputHandler(delegate {
                        escMenu.Show();
                        return false;
                    }),

                    // Delete removes selected tokens
                    [KeyCode.Delete] = new InputHandler(delegate {
                        tokenManager.DeleteSelectedTokens();
                        // Close any context menus, just in case one is related to
                        //  the token being deleted.
                        CloseAllContextMenus();
                        return false;
                    }),

                    // Pasting an image link onto the grid (Ctrl-V)
                    // TODO: Use a proper key combination system once there is one. For now this is
                    //  the only case anyway.
                    [KeyCode.V] = new InputHandler(delegate {
                        if (UnityEngine.Input.GetKey(KeyCode.LeftControl) && grid.IsMouseOverGrid()) {
                            tokenManager.AddTokenFromClipboard();
                        }
                        return false;
                    }),

                    // Plus key zooms in
                    [KeyCode.KeypadPlus] = new InputHandler(delegate {
                        Camera.main.Zoom(keyPressZoomAmount);
                        return false;
                    }),
                    // KeyCode.Equals is the "= +" key
                    [KeyCode.Equals] = new InputHandler(delegate {
                        Camera.main.Zoom(keyPressZoomAmount);
                        return false;
                    }),

                    // Minus zoomes out
                    [KeyCode.KeypadMinus] = new InputHandler(delegate {
                        Camera.main.Zoom(-keyPressZoomAmount);
                        return false;
                    }),
                    [KeyCode.Minus] = new InputHandler(delegate {
                        Camera.main.Zoom(-keyPressZoomAmount);
                        return false;
                    }),
                    
                    // Mouse buttons fire the current tool's behavior
                    [KeyCode.Mouse0] = new InputHandler(delegate {
                        _lastLeftMousePressPosition = Input.mousePosition;
                        toolManager.currentTool.LeftMousePress();
                        return false;
                    }),
                    [KeyCode.Mouse1] = new InputHandler(delegate {
                        _lastRightMousePressPosition = Input.mousePosition;
                        toolManager.currentTool.RightMousePress();
                        return false;
                    })
                },
                _keyReleaseHandlers: new Dictionary<KeyCode, InputHandler>() {
                    [KeyCode.Mouse0] = new InputHandler(delegate {
                        // See margin of error explaination for the Mouse1 handler below
                        if (Vector3.SqrMagnitude(Input.mousePosition - _lastLeftMousePressPosition) < mouseClickErrorMargin) {
                            toolManager.currentTool.LeftMouseClickRelease();
                        } else {
                            // The mouse moved far, so this is a drag
                            toolManager.currentTool.LeftMouseDragRelease();
                        }
                        return false;
                    }),
                    [KeyCode.Mouse1] = new InputHandler(delegate {
                        // Give some margin of error. Unity does that by default for vectors, but the default
                        //  precision is way too strict for this case.
                        if (Vector3.SqrMagnitude(Input.mousePosition - _lastRightMousePressPosition) < mouseClickErrorMargin) {
                            toolManager.currentTool.RightMouseClickRelease();
                        } else {
                            // The mouse moved far, so this is a drag
                            toolManager.currentTool.RightMouseDragRelease();
                        }
                        return false;  
                    })
                },
                _axisHandlers: new Dictionary<string, AxisHandler> {
                    // Scrollwheel for zooming
                    ["Mouse ScrollWheel"] = new AxisHandler(delegate (float _axis, float _axisRaw) {
                        Camera.main.Zoom(_axis);
                        return false;
                    }),

                    // Moving the camera around with right mouse button + drag
                    ["Mouse X"] = new AxisHandler(delegate (float _axis, float _axisRaw) {
                        // Don't move the camera if the current tool overrides that
                        if (toolManager.currentTool.BlockMouseCameraMovement()) return false;
                        if (Input.GetMouseButton(1)) {
                            // 1. We want the feeling of "dragging" the world around, hence the minus sign
                            // 2. We specifically calculate the mouse delta within world coordinates, so
                            //  that the world *exactly* "follows" the mouse cursor.
                            var _camBoundsCorners = grid.GetGridCornerCentersWorld();
                            Camera.main.MoveClampedXY(new Vector3(-mouseDeltaWorld.x, 0), _camBoundsCorners.Item1, _camBoundsCorners.Item2);
                        }
                        return false;
                    }),
                    ["Mouse Y"] = new AxisHandler(delegate (float _axis, float _axisRaw) {
                        // Don't move the camera if the current tool overrides that
                        if (toolManager.currentTool.BlockMouseCameraMovement()) return false;
                        if (Input.GetMouseButton(1)) {
                            var _camBoundsCorners = grid.GetGridCornerCentersWorld();
                            Camera.main.MoveClampedXY(new Vector3(0, -mouseDeltaWorld.y), _camBoundsCorners.Item1, _camBoundsCorners.Item2);
                        }
                        return false;
                    }),
                }
            ));

            initialized = true;
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            Initialize();
            previousMousePosition = Input.mousePosition;
        }

        private void LateUpdate() {
            if (Input.GetMouseButton(0)) toolManager.currentTool.LeftMouseHold();
            if (Input.GetMouseButton(1)) toolManager.currentTool.RightMouseHold();

            mouseDelta = Input.mousePosition - previousMousePosition;
            mouseDeltaWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition) - Camera.main.ScreenToWorldPoint(previousMousePosition);
            previousMousePosition = Input.mousePosition;
        }
    #endregion
    }
}
