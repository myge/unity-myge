﻿using Config;
using Network;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class CharacterStatePanel : EnhancedMonoBehaviour {
        private string currentCharacterId;
        private Dictionary<string, RulesetFieldPanel> panelsByFieldName = new Dictionary<string, RulesetFieldPanel>();
        private Dictionary<string, RulesetField> fieldsByName = new Dictionary<string, RulesetField>();

    #region Unity Editor Fields
        public ColyseusClientController colyseusClientController;
        public RectTransform[] defaultCategoryColumns;
        public RectTransform[] specificCategoryColumns;
        public GameObject categoryPrefab;
        public GameObject attributeFieldPrefab;
        public GameObject boolFlagFieldPrefab;
        public GameObject boolTrackFieldPrefab;
        public GameObject resourceFieldPrefab;
        public GameObject textEnumFieldPrefab;
        public GameObject textFieldPrefab;
    #endregion

    #region Public Functions
        public void Close() {
            currentCharacterId = "";
            LoadCurrentCharacterValues();
            UpdateFieldPanelsEditability();

            gameObject.SetActive(false);
        }

        public void EditCharacter(string _characterId) {
            currentCharacterId = _characterId;
            LoadCurrentCharacterValues();
            UpdateFieldPanelsEditability();

            gameObject.SetActive(true);
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!colyseusClientController) LogWarning("No colyseusClientController attached");
            if (defaultCategoryColumns.Length < 1) LogWarning("No default category columns attached - there must be at least one");
            if (specificCategoryColumns.Length < 1) LogWarning("No specific category columns attached - there must be at least one");
            if (!categoryPrefab) LogWarning("No categoryPrefab attached");
            if (!attributeFieldPrefab) LogWarning("No attributeFieldPrefab attached");
            if (!boolFlagFieldPrefab) LogWarning("No boolFlagFieldPrefab attached");
            if (!boolTrackFieldPrefab) LogWarning("No boolTrackFieldPrefab attached");
            if (!resourceFieldPrefab) LogWarning("No resourceFieldPrefab attached");
            if (!textEnumFieldPrefab) LogWarning("No textEnumFieldPrefab attached");
            if (!textFieldPrefab) LogWarning("No textFieldPrefab attached");
        }

        private void AddFieldToDefaultCategory(RulesetField _field) {
            RectTransform _column = GetBestDefaultCategoryColumn();
            GameObject _fieldObj = InstantiateFieldPanel(_field, _column);
        }

        private void AddSpecificCategory(List<RulesetField> _fields, string _category) {
            RectTransform _column = GetBestSpecificCategoryColumn();

            CharacterFieldCategoryPanel _categoryPanel = InstantiateCategoryPanel(_fields, _category);
            GameObject _categoryObj = _categoryPanel.gameObject;

            _categoryObj.transform.SetParent(_column);
            // Apparently the scale gets messed up for whatever reason when instatiating
            //  without a parent.
            _categoryObj.transform.localScale = new Vector3(1, 1, 1);
        }

        private async void CallModifyCharacterService(object[] _fieldChanges) {
            // If there is no character being edited, ignore all UI modifications
            if (currentCharacterId == "") return;

            await colyseusClientController.CallService("modifyCharacter", new {
                characterId = currentCharacterId,
                fieldChanges = _fieldChanges
            });
        }

        private bool CanLocalEditCharacter(string _characterId) {
            // No-one can ever edit anything when no character is currently being edited
            if (currentCharacterId == "") return false;

            // The GM can always edit every character
            if (colyseusClientController.role == Role.GM) return true;
            
            // Otherwise only the character's owner can edit the character
            CampaignState _state = colyseusClientController.GetLocalState();
            CharacterState _character;
            if (!_state.characters.TryGetValue(_characterId, out _character)) {
                LogWarning("Cannot check local permissions for editing character of ID '" + _characterId + "' - no such character exists");
                return false;
            }
            return _character.ownerSessionId == colyseusClientController.GetLocalSessionId();
        }

        private void ClearRuleset() {
            panelsByFieldName.Clear();
            fieldsByName.Clear();
            foreach (RectTransform _column in defaultCategoryColumns) {
                foreach (RectTransform _child in _column) {
                    GameObject.Destroy(_child.gameObject);
                }
            }
            foreach (RectTransform _column in specificCategoryColumns) {
                foreach (RectTransform _child in _column) {
                    GameObject.Destroy(_child.gameObject);
                }
            }
        }

        private void ClearValues() {
            foreach (RulesetFieldPanel _panel in panelsByFieldName.Values) {
                _panel.ClearValues();
            }
        }

        // Returns the column in the default category that has the most free space.
        private RectTransform GetBestDefaultCategoryColumn() {
            int _minHeightIdx = 0; // Always default to the first column if no other answer is found
            float _minHeight = float.PositiveInfinity;
            
            for (int i = 0; i < defaultCategoryColumns.Length; i++) {
                RectTransform _column = defaultCategoryColumns[i];
                float _height = GetColumnHeight(_column);
                // Add +1 to make sure that it's *actually* smaller, not floating point artifacts
                if (_height + 1 < _minHeight) {
                    _minHeight = _height;
                    _minHeightIdx = i;
                }
            }

            return defaultCategoryColumns[_minHeightIdx];
        }

        // Returns the column in the specific category section, that has the most free space.
        private RectTransform GetBestSpecificCategoryColumn() {
            int _minHeightIdx = 0; // Always default to the first column if no other answer is found
            float _minHeight = float.PositiveInfinity;
            
            for (int i = 0; i < specificCategoryColumns.Length; i++) {
                RectTransform _column = specificCategoryColumns[i];
                float _height = GetColumnHeight(_column);
                // Add +1 to make sure that it's *actually* smaller, not floating point artifacts
                if (_height + 1 < _minHeight) {
                    _minHeight = _height;
                    _minHeightIdx = i;
                }
            }

            return specificCategoryColumns[_minHeightIdx];
        }

        // The column height is computed manually, because updating the UI takes a frame, which
        //  would *massively* slow down the execution when loading a new ruleset. Prefab heights
        //  are actually known in advance, as are the vertical layout group settings.
        private float GetColumnHeight(RectTransform _column) {
            var _vert = _column.GetComponent<VerticalLayoutGroup>();
            float _height = _vert.padding.top + (_column.childCount - 1) * _vert.spacing + _vert.padding.bottom;
            foreach (RectTransform _child in _column) {
                var _categoryPanel = _child.GetComponent<CharacterFieldCategoryPanel>();
                float _childHeight = _categoryPanel != null ? _categoryPanel.GetHeight() : _child.rect.height;
                _height += _childHeight;
            }
            return _height;
        }

        private void Initialize() {
            CampaignState _state = colyseusClientController.GetLocalState();
            LoadRuleset(_state.rpgSettings.ruleset);
        }

        private CharacterFieldCategoryPanel InstantiateCategoryPanel(List<RulesetField> _fields, string _category) {
            GameObject _categoryObj = Instantiate(categoryPrefab);
            // Apparently the scale gets messed up for whatever reason when instatiating
            //  without a parent.
            _categoryObj.transform.localScale = new Vector3(1, 1, 1);
            var _categoryPanel = _categoryObj.GetComponent<CharacterFieldCategoryPanel>();

            _categoryPanel.SetCategoryHeader(_category);

            foreach (RulesetField _field in _fields) {
                InstantiateFieldPanel(_field, _categoryObj.transform);
            }

            return _categoryPanel;
        }

        private GameObject InstantiateFieldPanel(RulesetField _field, Transform _parent) {
            // Only render GM-only fields if the GM is local
            if (_field.gmOnly && colyseusClientController.role != Role.GM) return null;

            RulesetFieldType _type = _field.GetFieldType();

            fieldsByName.Add(_field.name, _field);

            if (_type == RulesetFieldType.Attribute) {
                GameObject _fieldObj = Instantiate(attributeFieldPrefab, _parent);
                var _fieldPanel = _fieldObj.GetComponent<AttributeFieldPanel>();
                _fieldPanel.LoadFieldSpecification((AttributeField)_field);
                panelsByFieldName.Add(_field.name, _fieldPanel);
                RegisterAttributeFieldPanelUIListeners(_field.name, _fieldPanel);
                return _fieldObj;
            } else if (_type == RulesetFieldType.Bool) {
                GameObject _fieldObj = Instantiate(boolFlagFieldPrefab, _parent);
                var _fieldPanel = _fieldObj.GetComponent<BoolFlagFieldPanel>();
                _fieldPanel.LoadFieldSpecification((BoolFlagField)_field);
                panelsByFieldName.Add(_field.name, _fieldPanel);
                RegisterBoolFlagFieldPanelUIListeners(_field.name, _fieldPanel);
                return _fieldObj;
            } else if (_type == RulesetFieldType.BoolTrack) {
                GameObject _fieldObj = Instantiate(boolTrackFieldPrefab, _parent);
                var _fieldPanel = _fieldObj.GetComponent<BoolTrackFieldPanel>();
                _fieldPanel.LoadFieldSpecification((BoolTrackField)_field);
                panelsByFieldName.Add(_field.name, _fieldPanel);
                RegisterBoolTrackFieldPanelUIListeners(_field.name, _fieldPanel);
                return _fieldObj;
            } else if (_type == RulesetFieldType.Resource) {
                GameObject _fieldObj = Instantiate(resourceFieldPrefab, _parent);
                var _fieldPanel = _fieldObj.GetComponent<ResourceFieldPanel>();
                _fieldPanel.LoadFieldSpecification((ResourceField)_field);
                panelsByFieldName.Add(_field.name, _fieldPanel);
                RegisterResourceFieldPanelUIListeners(_field.name, _fieldPanel);
                return _fieldObj;
            } else if (_type == RulesetFieldType.Text) {
                GameObject _fieldObj = Instantiate(textFieldPrefab, _parent);
                var _fieldPanel = _fieldObj.GetComponent<TextFieldPanel>();
                _fieldPanel.LoadFieldSpecification((TextField)_field);
                panelsByFieldName.Add(_field.name, _fieldPanel);
                RegisterTextFieldPanelUIListeners(_field.name, _fieldPanel);
                return _fieldObj;
            } else if (_type == RulesetFieldType.TextEnum) {
                GameObject _fieldObj = Instantiate(textEnumFieldPrefab, _parent);
                var _fieldPanel = _fieldObj.GetComponent<TextEnumFieldPanel>();
                _fieldPanel.LoadFieldSpecification((TextEnumField)_field);
                panelsByFieldName.Add(_field.name, _fieldPanel);
                RegisterTextEnumFieldPanelUIListeners(_field.name, _fieldPanel);
                return _fieldObj;
            }

            return null;
        }

        private void HandleServerAddRulesetFieldValue(RulesetFieldValue _value, string _fieldName, string _characterId, RulesetFieldType _type) {
            // Only deal with events regarding the character being currently edited
            if (_characterId != currentCharacterId) return;

            LoadFieldValue(_value, _fieldName, _type);
        }

        private void HandleServerRulesetFieldValueChanged(string _fieldName, string _characterId, RulesetFieldType _type, List<Colyseus.Schema.DataChange> _changes) {
            // Only deal with events regarding the character being currently edited
            if (_characterId != currentCharacterId) return;

            // It's easier to reload the entire field value object every time,
            //  especially with how certain field panels do recalculation in
            //  their LoadFieldValue method.
            CharacterState _character = colyseusClientController.GetLocalState().characters[_characterId];
            RulesetFieldValue _value = _character.GetFieldValue(_fieldName, _type);
            LoadFieldValue(_value, _fieldName, _type);
        }

        private void LoadCurrentCharacterValues() {
            ClearValues();

            if (currentCharacterId == "") return;
            CharacterState _character = colyseusClientController.GetLocalState().characters[currentCharacterId];

            foreach (RulesetFieldValue _value in _character.attributeValues.Values) {
                LoadFieldValue(_value, _value.fieldName, RulesetFieldType.Attribute);
            }
            foreach (RulesetFieldValue _value in _character.boolFlagValues.Values) {
                LoadFieldValue(_value, _value.fieldName, RulesetFieldType.Bool);
            }
            foreach (RulesetFieldValue _value in _character.boolTrackValues.Values) {
                LoadFieldValue(_value, _value.fieldName, RulesetFieldType.BoolTrack);
            }
            foreach (RulesetFieldValue _value in _character.resourceValues.Values) {
                LoadFieldValue(_value, _value.fieldName, RulesetFieldType.Resource);
            }
            foreach (RulesetFieldValue _value in _character.textEnumValues.Values) {
                LoadFieldValue(_value, _value.fieldName, RulesetFieldType.TextEnum);
            }
            foreach (RulesetFieldValue _value in _character.textValues.Values) {
                LoadFieldValue(_value, _value.fieldName, RulesetFieldType.Text);
            }
        }

        private void LoadFieldValue(RulesetFieldValue _value, string _fieldName, RulesetFieldType _type) {
            if (_type == RulesetFieldType.Attribute) {
                var _panel = (AttributeFieldPanel)panelsByFieldName[_fieldName];
                _panel.LoadFieldValue((AttributeValue)_value);
            } else if (_type == RulesetFieldType.Bool) {
                var _panel = (BoolFlagFieldPanel)panelsByFieldName[_fieldName];
                _panel.LoadFieldValue((BoolFlagValue)_value);
            } else if (_type == RulesetFieldType.BoolTrack) {
                var _panel = (BoolTrackFieldPanel)panelsByFieldName[_fieldName];
                _panel.LoadFieldValue((BoolTrackValue)_value);
            } else if (_type == RulesetFieldType.Resource) {
                var _panel = (ResourceFieldPanel)panelsByFieldName[_fieldName];
                _panel.LoadFieldValue((ResourceValue)_value);
            } else if (_type == RulesetFieldType.TextEnum) {
                var _panel = (TextEnumFieldPanel)panelsByFieldName[_fieldName];
                _panel.LoadFieldValue((TextEnumValue)_value);
            } else if (_type == RulesetFieldType.Text) {
                var _panel = (TextFieldPanel)panelsByFieldName[_fieldName];
                _panel.LoadFieldValue((TextValue)_value);
            }
        }

        // Use every time the ruleset is changed in the game settings
        private void LoadRuleset(Ruleset _ruleset) {
            ClearRuleset();

            var _fieldsByCategory = _ruleset.GetFieldsInOrderByCategory();
            var _categories = _fieldsByCategory.Keys;

            foreach (var _kvp in _fieldsByCategory) {
                string _category = _kvp.Key;
                List<RulesetField> _fields = _kvp.Value;

                // The "" (null) string stands for the "default" category at the top
                if (_category == "") {
                    foreach (RulesetField _field in _fields) {
                        AddFieldToDefaultCategory(_field);
                    }
                } else {
                    AddSpecificCategory(_fields, _category);
                }
            }

            UpdateColumnVisibility();
        }

        private void RegisterAttributeFieldPanelUIListeners(string _fieldName, AttributeFieldPanel _panel) {
            _panel.genericToggle.onValueChanged.AddListener(delegate {
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Attribute.ToString(),
                    genericToggle = _panel.genericToggle.isOn
                } });
            });
            _panel.baseValueInput.onValueChanged.AddListener(delegate {
                int _value = int.TryParse(_panel.baseValueInput.text, out _value) ? _value : 0;
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Attribute.ToString(),
                    value = _value
                } });
            });
            _panel.tempModInput.onValueChanged.AddListener(delegate {
                int _value = int.TryParse(_panel.tempModInput.text, out _value) ? _value : 0;
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Attribute.ToString(),
                    tempModifier = _value
                } });
            });
            _panel.tempOverrideInput.onValueChanged.AddListener(delegate {
                int _value = int.TryParse(_panel.tempOverrideInput.text, out _value) ? _value : 0;
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Attribute.ToString(),
                    tempValueOverride = _value
                } });
            });
            // TODO: Add clickable roll listener
        }

        private void RegisterBoolFlagFieldPanelUIListeners(string _fieldName, BoolFlagFieldPanel _panel) {
            _panel.flagToggle.onValueChanged.AddListener(delegate {
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Bool.ToString(),
                    value = _panel.flagToggle.isOn
                } });
            });
        }

        private void RegisterBoolTrackFieldPanelUIListeners(string _fieldName, BoolTrackFieldPanel _panel) {
            _panel.genericToggle.onValueChanged.AddListener(delegate {
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.BoolTrack.ToString(),
                    genericToggle = _panel.genericToggle.isOn
                } });
            });
            _panel.onTrackValueChanged.AddListener(delegate {
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.BoolTrack.ToString(),
                    toggles = _panel.GetTogglesValues()
                } });
            });
        }

        private void RegisterResourceFieldPanelUIListeners(string _fieldName, ResourceFieldPanel _panel) {
            _panel.genericToggle.onValueChanged.AddListener(delegate {
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Resource.ToString(),
                    genericToggle = _panel.genericToggle.isOn
                } });
            });
            _panel.currentValueInput.onValueChanged.AddListener(delegate {
                int _value = int.TryParse(_panel.currentValueInput.text, out _value) ? _value : 0;
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Resource.ToString(),
                    current = _value
                } });
            });
            _panel.currentTempModInput.onValueChanged.AddListener(delegate {
                int _value = int.TryParse(_panel.currentTempModInput.text, out _value) ? _value : 0;
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Resource.ToString(),
                    tempCurrentModifier = _value
                } });
            });
            _panel.maxValueInput.onValueChanged.AddListener(delegate {
                int _value = int.TryParse(_panel.maxValueInput.text, out _value) ? _value : 0;
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Resource.ToString(),
                    max = _value
                } });
            });
            _panel.maxTempModInput.onValueChanged.AddListener(delegate {
                int _value = int.TryParse(_panel.maxTempModInput.text, out _value) ? _value : 0;
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Resource.ToString(),
                    tempMaxModifier = _value
                } });
            });
            _panel.tempBufferInput.onValueChanged.AddListener(delegate {
                int _value = int.TryParse(_panel.tempBufferInput.text, out _value) ? _value : 0;
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Resource.ToString(),
                    tempBuffer = _value
                } });
            });
        }

        private void RegisterTextEnumFieldPanelUIListeners(string _fieldName, TextEnumFieldPanel _panel) {
            _panel.genericToggle.onValueChanged.AddListener(delegate {
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.TextEnum.ToString(),
                    genericToggle = _panel.genericToggle.isOn
                } });
            });
            _panel.dropdown.onValueChanged.AddListener(delegate {
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.TextEnum.ToString(),
                    optionIdx = _panel.dropdown.value
                } });
            });
        }

        private void RegisterTextFieldPanelUIListeners(string _fieldName, TextFieldPanel _panel) {
            _panel.genericToggle.onValueChanged.AddListener(delegate {
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Text.ToString(),
                    genericToggle = _panel.genericToggle.isOn
                } });
            });
            _panel.textInput.onValueChanged.AddListener(delegate {
                CallModifyCharacterService(new object[] { new {
                    fieldName = _fieldName,
                    type = RulesetFieldType.Text.ToString(),
                    text = _panel.textInput.text
                } });
            });
        }

        private void RegisterRulesetFieldValueListeners() {
            colyseusClientController.emitter.onAddRulesetFieldValue.AddListener(delegate (RulesetFieldValue _value, string _fieldName, string _characterId, RulesetFieldType _type) {
                HandleServerAddRulesetFieldValue(_value, _fieldName, _characterId, _type);
            });
            colyseusClientController.emitter.onRulesetFieldValueChanged.AddListener(delegate (string _fieldName, string _characterId, RulesetFieldType _type, List<Colyseus.Schema.DataChange> _changes) {
                HandleServerRulesetFieldValueChanged(_fieldName, _characterId, _type, _changes);
            });
        }

        private void UpdateColumnVisibility() {
            foreach (RectTransform _column in defaultCategoryColumns) {
                if (_column.childCount < 1) _column.gameObject.SetActive(false);
            }
            foreach (RectTransform _column in specificCategoryColumns) {
                if (_column.childCount < 1) _column.gameObject.SetActive(false);
            }
        }

        private void UpdateFieldPanelsEditability() {
            bool _canEdit = CanLocalEditCharacter(currentCharacterId);

            foreach (RulesetFieldPanel _panel in panelsByFieldName.Values) {
                _panel.SetEditable(_canEdit);
            }
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
            Initialize();
            RegisterRulesetFieldValueListeners();
        }
    #endregion
    }
}