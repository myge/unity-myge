﻿using Game;
using Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCore.Utils;

namespace UI {

    public class TokenPortraitEditorPanel : EnhancedMonoBehaviour {
        private string characterId = "";

    #region Unity Editor Fields
        public ColyseusClientController colyseusClientController;
        public FileCache fileCache;
        public TokenPreview tokenPreview;
        public ImageSelectionPanel imageSelectionPanel;
    #endregion

    #region Public Functions
        public void Close() {
            characterId = "";
            tokenPreview.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }

        public async void EditCharacterPortrait(string _characterId, string _imageURI = null) {
            characterId = _characterId;
            CampaignState _state = colyseusClientController.GetLocalState();
            CharacterState _character = _state.characters[characterId];
            PortraitState _portrait = _character.portrait;

            Vector3 _tokenOffset = new Vector3(_portrait.tokenOffset.x, _portrait.tokenOffset.y, 0);
            float _tokenScale = _portrait.tokenScale;

            // If there is no image given, use the current portrait
            if (_imageURI == null || _imageURI == "") {
                _imageURI = _portrait.imageURI;
            } else {
                // If there is a given image, use a scale that makes sense
                _tokenScale = 1f;
            }

            // If there is still no image assigned, make the user select one first
            if (_imageURI == null || _imageURI == "") {
                string _selectedImageURI = await imageSelectionPanel.GetImageSelection();
                if (_selectedImageURI == "") {
                    // The user backed out, end the whole thing
                    Close();
                    return;
                }

                // The user selected an image, let's use that going forward
                _imageURI = _selectedImageURI;

                // Also, assign a default scale that makes sense (it's initialized to 0)
                _tokenScale = 1f;
            }

            // Acquire the texture and, once done, load it into the preview
            fileCache.GetTexture(_imageURI, delegate (Texture2D _texture) {
                tokenPreview.LoadPortrait(
                    _imageURI,
                    _texture,
                    _tokenOffset,
                    _tokenScale
                );
                tokenPreview.gameObject.SetActive(true);
            });

            gameObject.SetActive(true);
        }

        public void Reset() {
            tokenPreview.Reset();
        }

        public async void Submit() {
            PortraitState _portrait = tokenPreview.CollectPortrait();
            await colyseusClientController.CallService("setCharacterPortrait", new {
                characterId = characterId,
                imageURI = _portrait.imageURI,
                tokenOffset = new {
                    x = _portrait.tokenOffset.x,
                    y = _portrait.tokenOffset.y
                },
                tokenScale = _portrait.tokenScale
            });
            Close();
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!colyseusClientController) LogWarning("No colyseusClientController attached");
            if (!fileCache) LogWarning("No fileCache attached");
            if (!tokenPreview) LogWarning("No tokenPreview attached");
            if (!imageSelectionPanel) LogWarning("No imageSelectionPanel attached");
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
        }

        private void Update() {
            // Interacting with this panel is independent of any tool,
            //  thus the input can just be handled here, it's trivial this way.
            if (characterId != "") {
                if (tokenPreview.IsMouseOnPreview()) {
                    // Allow to zoom and to *start* dragging only if mouse is over
                    //  the token preview zone.
                    if (Input.GetMouseButtonDown(0)) tokenPreview.DragPortrait();
                    if (Input.GetAxisRaw("Mouse ScrollWheel") != 0) {
                        float _axis = Input.GetAxis("Mouse ScrollWheel");
                        tokenPreview.ChangeScale(_axis);
                    };
                }

                // *Drop*, on the other hand, is always allowed. It would be 
                //  extremely annoying if the object being dragged stuck to the
                //  cursor because it was not dropped in the correct area.
                if (Input.GetMouseButtonUp(0)) tokenPreview.DropPortrait();
            }
        }
    #endregion

    }
}