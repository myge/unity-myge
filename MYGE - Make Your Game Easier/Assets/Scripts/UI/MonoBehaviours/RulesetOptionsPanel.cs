﻿using Config;
using Colyseus.Schema;
using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class RulesetOptionsPanel : EnhancedMonoBehaviour {
        private List<RulesetField> rulesetFields = new List<RulesetField>();

    #region Unity Fields
        public RulesetFieldType newFieldDefaultType;
        public ObjectList fieldsObjectList;
    #endregion

    #region Public Functions
        public void AddField() {
            AddFieldOfType(newFieldDefaultType);
        }

        public void Clear() {
            rulesetFields.Clear();
            fieldsObjectList.Clear();
        }

        public Ruleset CollectRuleset() {
            Ruleset _ruleset = new Ruleset();

            // TODO: Ruleset name
            foreach (RulesetField _field in rulesetFields) {
                if (_field != null) {
                    _ruleset.AddField(_field);
                }
            }

            return _ruleset;
        }

        public void LoadRuleset(Ruleset _ruleset) {
            Clear();

            // TODO: Ruleset name

            // First populate the list of fields
            rulesetFields = _ruleset.GetFieldsInOrder();

            // Then correct indices can be passed forward
            for (int i = 0; i < rulesetFields.Count; i++) {
                LoadField(i);
            }
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (newFieldDefaultType == RulesetFieldType.Invalid) LogWarning("New field default type invalid!");
            if (!fieldsObjectList) LogWarning("No fieldsObjectList attached");
        }

        private RulesetField AddFieldOfType(RulesetFieldType _type) {
            RulesetField _field = ConstructFieldOfType(_type);
            rulesetFields.Add(_field);
            int _fieldIdx = rulesetFields.Count - 1;
            LoadField(_fieldIdx);

            return _field;
        }

        private RulesetField ConstructFieldOfType(RulesetFieldType _type) {
            switch (_type) {
                case RulesetFieldType.Attribute:
                    return new AttributeField();
                case RulesetFieldType.Bool:
                    return new BoolFlagField();
                case RulesetFieldType.BoolTrack:
                    return new BoolTrackField();
                case RulesetFieldType.Resource:
                    return new ResourceField();
                case RulesetFieldType.Text:
                    return new TextField();
                case RulesetFieldType.TextEnum:
                    return new TextEnumField();
                default:
                    return null;
            }
        }

        private void CopyGenericFieldProperties(RulesetField _from, RulesetField _to) {
            _to.name = _from.name;
            _to.category = _from.category;
            _to.showGenericToggle = _from.showGenericToggle;
            _to.gmOnly = _from.gmOnly;
            _to.visualizationMode = _from.visualizationMode;
        }

        private void DeleteFieldFromList(int _fieldIdx) {
            // All listeners are bound to indices, not values, thus the
            //  indices must be preserved, hence the ruleset being set
            //  to null instead of being removed from the list.
            // The exception is when it's the last item on the list.
            int _lastItemIdx = rulesetFields.Count - 1;
            if (_fieldIdx == _lastItemIdx) {
                rulesetFields.RemoveAt(_fieldIdx);
            } else {
                rulesetFields[_fieldIdx] = null;
            }
        }

        private void FeedFieldIntoUI(RulesetFieldUISwitch _refs, int _fieldIdx, GameObject _fieldObj) {
            // ATTENTION!
            // Listeners must refer to the field by index. This is the only way to allow for
            //  the field object to be potentially replaced, given how delegates cannot make
            //  use of references.
            if (_fieldIdx >= rulesetFields.Count) return;

            RulesetField _field = rulesetFields[_fieldIdx];
            if (_field == null) return;
            
            int _fieldType = _field.GetFieldTypeInt();
            if (_fieldType == -1) {
                LogWarning("Cannot feed field into the UI. Matching field class not found.");
                return;
            }

            // Field type
            _refs.fieldTypeDropdown.value = _fieldType;
            _refs.fieldTypeDropdown.onValueChanged.AddListener(delegate (int _newTypeInt) {
                RulesetField _oldField = rulesetFields[_fieldIdx];
                RulesetFieldType _newType = GetFieldTypeFromInt(_newTypeInt);
                RulesetField _newField = ConstructFieldOfType(_newType);
                if (_newField == null) {
                    LogWarning("Field type input set to an invalid value: " + _newTypeInt);
                    return;
                }
                CopyGenericFieldProperties(_oldField, _newField);
                rulesetFields[_fieldIdx] = _newField;
                ReloadFieldObjects();
            });

            // Delete field button
            _refs.deleteFieldButton.onClick.AddListener(delegate {
                DeleteFieldFromList(_fieldIdx);
                fieldsObjectList.RemoveItem(_fieldObj);
            });

            // Generic
            _refs.fieldNameInput.text = _field.name;
            _refs.fieldNameInput.onValueChanged.AddListener((_newVal) => rulesetFields[_fieldIdx].name = _newVal);

            _refs.categoryInput.text = _field.category;
            _refs.categoryInput.onValueChanged.AddListener((_newVal) => rulesetFields[_fieldIdx].category = _newVal);

            _refs.showGenericToggleToggle.isOn = _field.showGenericToggle;
            _refs.showGenericToggleToggle.onValueChanged.AddListener((_newVal) => rulesetFields[_fieldIdx].showGenericToggle = _newVal);

            _refs.gmOnlyToggle.isOn = _field.gmOnly;
            _refs.gmOnlyToggle.onValueChanged.AddListener((_newVal) => rulesetFields[_fieldIdx].gmOnly = _newVal);

            // There exist many visualization mode dropdowns, all with different options
            //  (thus they must be separate), but only one at a time will be visible,
            //  so their values may be processed with no regard which one it is.
            int _visualizationMode = rulesetFields[_fieldIdx].visualizationMode;
            foreach (Dropdown _dropdown in _refs.visualizationModeDropdowns) {
                _dropdown.value = _visualizationMode;
                _dropdown.onValueChanged.AddListener((_newVal) => rulesetFields[_fieldIdx].visualizationMode = _newVal);
            }

            // AttributeField
            var _attrField = _field as AttributeField;
            if (_attrField != null) {
                _refs.attributeFieldSection.SetActive(true);

                _refs.attrTextColorHexInput.text = _attrField.textColorHex;
                _refs.attrTextColorHexInput.onValueChanged.AddListener((_newVal) => ((AttributeField) rulesetFields[_fieldIdx]).textColorHex = _newVal);

                _refs.attrRollInput.text = _attrField.rollCommandArgument;
                _refs.attrRollInput.onValueChanged.AddListener((_newVal) => ((AttributeField) rulesetFields[_fieldIdx]).rollCommandArgument = _newVal);
            } else {
                _refs.attributeFieldSection.SetActive(false);
                _refs.attrTextColorHexInput.text = "";
                _refs.attrRollInput.text = "";
            }

            // Bool field
            var _boolField = _field as BoolFlagField;
            if (_boolField != null) {
                _refs.boolFieldSection.SetActive(true);

                _refs.boolSymbolColorHexInput.text = _boolField.symbolColorHex;
                _refs.boolSymbolColorHexInput.onValueChanged.AddListener((_newVal) => ((BoolFlagField) rulesetFields[_fieldIdx]).symbolColorHex = _newVal);
            } else {
                _refs.boolFieldSection.SetActive(false);
                _refs.boolSymbolColorHexInput.text = "";
            }

            // Bool track field
            var _trackField = _field as BoolTrackField;
            if (_trackField != null) {
                _refs.boolTrackFieldSection.SetActive(true);

                if (_trackField.toggleLabels.Count > 0) {
                    _refs.trackToggleLabelsInput.text = string.Join(", ", _trackField.toggleLabels.Items.Values);
                } else {
                    _refs.trackToggleLabelsInput.text = "";
                }
                _refs.trackToggleLabelsInput.onValueChanged.AddListener(delegate (string _newVal) {
                    BoolTrackField _f = ((BoolTrackField) rulesetFields[_fieldIdx]);
                    _f.toggleLabels = ParseListString(_newVal);
                });
            } else {
                _refs.boolTrackFieldSection.SetActive(false);
                _refs.trackToggleLabelsInput.text = "";
            }

            // Resource field
            var _resField = _field as ResourceField;
            if (_resField != null) {
                _refs.resourceFieldSection.SetActive(true);

                _refs.resBarColorHexInput.text = _resField.barColorHex;
                _refs.resBarColorHexInput.onValueChanged.AddListener((_newVal) => ((ResourceField) rulesetFields[_fieldIdx]).barColorHex = _newVal);
            } else {
                _refs.resourceFieldSection.SetActive(false);
                _refs.resBarColorHexInput.text = "";
            }

            // Enum field
            var _enumField = _field as TextEnumField;
            if (_enumField != null) {
                _refs.textEnumSection.SetActive(true);

                _refs.enumTextColorHexInput.text = _enumField.textColorHex;
                _refs.enumTextColorHexInput.onValueChanged.AddListener((_newVal) => ((TextEnumField) rulesetFields[_fieldIdx]).textColorHex = _newVal);

                if (_enumField.options.Count > 0) {
                    _refs.enumOptionsInput.text = string.Join(", ", _enumField.options.Items.Values);
                } else {
                    _refs.enumOptionsInput.text = "";
                }
                _refs.enumOptionsInput.onValueChanged.AddListener(delegate (string _newVal) {
                    TextEnumField _f = ((TextEnumField) rulesetFields[_fieldIdx]);
                    _f.options = ParseListString(_newVal);
                });
            } else {
                _refs.textEnumSection.SetActive(false);
                _refs.enumTextColorHexInput.text = "";
                _refs.enumOptionsInput.text = "";
            }

            // Text field
            var _textField = _field as TextField;
            if (_textField != null) {
                _refs.textSection.SetActive(true);

                _refs.textTextColorHexInput.text = _textField.textColorHex;
                _refs.textTextColorHexInput.onValueChanged.AddListener((_newVal) => ((TextField) rulesetFields[_fieldIdx]).textColorHex = _newVal);
            } else {
                _refs.textSection.SetActive(false);
                _refs.textTextColorHexInput.text = "";
            }
        }

        private RulesetFieldType GetFieldTypeFromInt(int _typeInt) {
            if (System.Enum.IsDefined(typeof(RulesetFieldType), _typeInt)) {
                return (RulesetFieldType) _typeInt;
            } else {
                return RulesetFieldType.Invalid;
            }
        }

        private void LoadField(int _fieldIdx) {
            RulesetField _field = rulesetFields[_fieldIdx];
            if (_field == null) return; // Null field means that it has been deleted

            GameObject _obj = fieldsObjectList.AddItem(new string[] {});
            ObjectReferenceSwitch _objRefs = _obj.GetComponent<ObjectReferenceSwitch>();
            if (_objRefs == null) {
                LogWarning("Ruleset field settings template has no ObjectReferenceSwitch");
                return;
            }
            RulesetFieldUISwitch _refs = new RulesetFieldUISwitch(_objRefs);
            FeedFieldIntoUI(_refs, _fieldIdx, _obj);
        }

        private ArraySchema<string> ParseListString(string _listStr) {
            string[] _list = _listStr.Split(',');
            return _list.ToArraySchema();
        }

        private void ReloadFieldObjects() {
            fieldsObjectList.Clear();
            for (int i = 0; i < rulesetFields.Count; i++) {
                LoadField(i);
            }
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            CheckIntegrity();
        }
    #endregion
    }
}