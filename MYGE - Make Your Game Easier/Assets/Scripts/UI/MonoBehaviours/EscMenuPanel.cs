﻿using Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCore.Utils;

namespace UI {

    public class EscMenuPanel : EnhancedMonoBehaviour {

    #region Unity Editor Fields
        
    #endregion

    #region Public Functions
        public void Close() {
            gameObject.SetActive(false);
        }

        public void Show() {
            gameObject.SetActive(true);
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {}
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
        }
    #endregion

    }
}