using Network;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class ChatPanel : EnhancedMonoBehaviour {

    #region Unity Fields
        public bool autoScrollToBottom;
        public ColyseusClientController colyseusClientController;
        public ObjectList chatMessageObjList;
        public ScrollRect chatScrollView;
        public InputField chatInput;
        public Image scrollbarImage;
        public ChatCommandParser commandParser;
    #endregion

    #region Public Functions
        public async void SendMessage() {
            string _message = chatInput.text.Trim();
            if (_message == "") return;

            // The message may be a chat command. If it is, don't send it as a chat message.
            bool _isChatCommand = commandParser.IsMessageCommand(_message);
            if (_isChatCommand) {
                try { await commandParser.ParseAndRunCommand(_message); }
                catch (System.Exception e) {
                    // Print the error message on chat
                    string _formattedException = FormatException(e);
                    Print(_formattedException);
                }
            }

            chatInput.text = "";

            if (!_isChatCommand) {
                await colyseusClientController.SendChatMessage(_message);
            }
        }

        public void HandleDiceRoll(DiceRollResult _msgSchema) {
            string _formattedRollResult = FormatDiceRollResult(_msgSchema);
            Print(_formattedRollResult);
        }

        public void HandleIncomingMessage(ChatMessage _msgSchema) {
            string _formattedMsg = FormatMessage(_msgSchema);
            Print(_formattedMsg);
        }

        public void Print(string _message) {
            GameObject _msgGameObj = CreateMessageVisibleObject(_message);

            // Force layout groups to recalculate in order to fix multiline message problems
            Canvas.ForceUpdateCanvases();
            ResetMessageObject(_msgGameObj);

            // Automatically scroll to the bottom every time there is a new message
            if (autoScrollToBottom) {
                ScrollToBottom();
            }
        }

        public void ScrollToBottom() {
            chatScrollView.verticalNormalizedPosition = 0.0f;
            //chatScrollView.velocity = new Vector2(0, Mathf.Pow(10, 6));
        }

        public void SetScrollbarVisibility(bool _visible) {
            chatScrollView.verticalScrollbar.interactable = _visible;
            scrollbarImage.enabled = _visible;
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!colyseusClientController) LogWarning("No ColyseusClientController attached");
            if (!chatMessageObjList) LogWarning("No chat message ObjectList attached");
            if (!chatScrollView) LogWarning("No chat ScrollRect attached");
            if (!chatInput) LogWarning("No chat input attached");
            if (!scrollbarImage) LogWarning("No scrollbar image attached");
            if (!commandParser) LogWarning("No ChatCommandParser attached");
        }

        private GameObject CreateMessageVisibleObject(string _richTextMessage) {
            return chatMessageObjList.AddItem(new string [] {
                _richTextMessage
            });
        }

        private string FormatDiceRollResult(DiceRollResult _rollSchema) {
            List<string> _rollStrings = new List<string>();
            foreach(DiceRollResult.IndividualRoll _roll in _rollSchema.rolls) {
                _rollStrings.Add($"{_roll.times}d{_roll.sides}");
            }
            if (_rollSchema.modifier != 0) _rollStrings.Add(_rollSchema.modifier.ToString());

            string _rollStr = string.Join("+", _rollStrings);

            // A negative number, for example -3, converts to a string of
            //  "-3" (obviously). This means that the above join operation
            //  with "+" as delimiter will create a notation of, for example,
            //  1d20+-3, hence all "+-" must be replaced with "-".
            _rollStr = _rollStr.Replace("+-", "-");

            List<string> _resultsStrings = new List<string>();
            foreach(DiceRollResult.IndividualRollResult _result in _rollSchema.individualResults) {
                _resultsStrings.Add(_result.result.ToString());
            }
            if (_rollSchema.modifier != 0) _resultsStrings.Add(_rollSchema.modifier.ToString());

            string _resultsStr = string.Join("+", _resultsStrings);
            _resultsStr = _resultsStr.Replace("+-", "-");

            string _formatted = $"<color=orange><b>{_rollSchema.rollerSessionId}</b> rolls {_rollStr}: ({_resultsStr}) = <b>{_rollSchema.finalResult}</b></color>";
            return _formatted;
        }

        private string FormatException(System.Exception _ex) {
            return "<color=red>" + _ex.Message + "</color>";
        }

        private string FormatMessage(ChatMessage _msgSchema) {
            return "<b>" + _msgSchema.authorSessionId + "</b>: " + _msgSchema.message;
        }

        /*  This method is a hack that basically resets the message object's vertical layouts.
            That helps fix multiline messages' rendering problems. */
        private void ResetMessageObject(GameObject _msgGameObj) {
            VerticalLayoutGroup _layout = _msgGameObj.GetComponent<VerticalLayoutGroup>();
            VerticalLayoutGroup _parentLayout = _msgGameObj.transform.parent.GetComponent<VerticalLayoutGroup>();
            if (_layout) {
                _layout.enabled = false;
                _layout.enabled = true;
            }
            if (_parentLayout) {
                _parentLayout.enabled = false;
                _parentLayout.enabled = true;
            }
        }

        private void RegisterIncomingMessageListeners() {
            colyseusClientController.emitter.onChatMessage.AddListener(delegate (ChatMessage _msg) {
                HandleIncomingMessage(_msg);
            });
            colyseusClientController.emitter.onDiceRoll.AddListener(delegate (DiceRollResult _msg) {
                HandleDiceRoll(_msg);
            });
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            CheckIntegrity();
            RegisterIncomingMessageListeners();
            SetScrollbarVisibility(false);
        }
    #endregion
    }
}