using Game;
using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class AreaSelectionPanel : EnhancedMonoBehaviour {
        private Dictionary<string, GameObject> entriesByAreaId = new Dictionary<string, GameObject>();
        private Dictionary<string, AreaListEntryUISwitch> refsByAreaId = new Dictionary<string, AreaListEntryUISwitch>();
        private Color defaultAreaColor;
        private Image highlightedAreaEntry = null;
        private Text playersAreaEntry = null;

        public string currentAreaId { get; private set; }

    #region Unity Fields
        public ColyseusClientController colyseusClientController;
        public PageController pageController;
        public AreaConfigPanel areaConfigPanel;
        public ObjectList areasObjectList;
        public GridManager grid;
        public FogManager fog;
        public TokenManager tokenManager;
        public Color highlightedAreaColor;
        public string playersAreaSuffix;
    #endregion

    #region Public Functions
        public async void AddNewArea() {
            await colyseusClientController.CallService("addNewArea");
        }

        public void CloseAreaConfig() {
            pageController.TurnPageOff(PageType.AreaConfig);
        }

        public void DisplayArea(AreaState _area) {
            if (_area != null) {
                currentAreaId = _area.id;
                grid.RenderGrid(_area.areaSettings.gridWidth, _area.areaSettings.gridHeight);
            } else {
                currentAreaId = "";
                grid.Clear();
            }
            // "" can be passed as the param, in that case the comparison will
            //  correctly return false for all cases and all objects will be hidden.
            tokenManager.UpdateForAreaView(currentAreaId, _clearCurrentArea: _area == null);
            fog.UpdateForAreaView(currentAreaId);
        }

        public async void RemoveArea(string _areaId) {
            await colyseusClientController.CallService("deleteArea", new {
                areaId = _areaId
            });
        }

        public async void SetPlayersArea(string _areaId) {
            await colyseusClientController.CallService("setPlayerArea" , new {
                areaId = _areaId
            });
        }

        public async void SubmitAreaConfig() {
            AreaState _areaConfig = areaConfigPanel.CollectAreaConfig();
            if (_areaConfig == null) return;

            string _areaConfigJSON = Newtonsoft.Json.JsonConvert.SerializeObject(_areaConfig);

            await colyseusClientController.CallService("modifyArea", new {
                areaId = _areaConfig.id,
                areaChangesJSON = _areaConfigJSON
            });

            CloseAreaConfig();
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!colyseusClientController) LogWarning("No colyseusClientController attached");
            if (!pageController) LogWarning("No pageController attached");
            if (!areaConfigPanel) LogWarning("No areaConfigPanel attached");
            if (!areasObjectList) LogWarning("No areasObjectList attached");
            if (!grid) LogWarning("No grid attached");
            if (!fog) LogWarning("No fogManager attached");
            if (!tokenManager) LogWarning("No tokenManager attached");
        }

        private GameObject CreateAreaListEntryObject(AreaState _area) {
            GameObject _obj =  areasObjectList.AddItem(new string[] { _area.name });
            AreaListEntryUISwitch _refs = GetAreaListEntryComponents(_obj);
            if (_refs != null) RegisterAreaListEntryUIListeners(_area.id, _refs);

            entriesByAreaId.Add(_area.id, _obj);
            refsByAreaId.Add(_area.id, _refs);

            return _obj;
        }

        private void FormatPlayersAreaText(Text _areaText) {
            _areaText.text += playersAreaSuffix;
            _areaText.fontStyle = FontStyle.Bold;
        }

        private AreaListEntryUISwitch GetAreaListEntryComponents(GameObject _obj) {
            ObjectReferenceSwitch _objRefs = _obj.GetComponent<ObjectReferenceSwitch>();
            if (_objRefs == null) {
                LogWarning("Area list entry template has no ObjectReferenceSwitch");
                return null;
            }
            AreaListEntryUISwitch _refs = new AreaListEntryUISwitch(_objRefs);
            return _refs;
        }
        
        private void HandleAreaSelection(string _areaId, AreaListEntryUISwitch _refs) {
            AreaState _area = colyseusClientController.GetLocalState().areas[_areaId];
            DisplayArea(_area);
            HighlightArea(_areaId, _refs);
        }

        private void HandleAreaSettingsButton(string _areaId) {
            OpenAreaConfig();
            AreaState _area = colyseusClientController.GetLocalState().areas[_areaId];
            areaConfigPanel.EditArea(_area);
        }

        private void HandleServerAddArea(AreaState _area, string _areaId) {
            CreateAreaListEntryObject(_area);
        }

        private void HandleServerAreaChange(string _areaId, Colyseus.Schema.DataChange _change) {
            AreaListEntryUISwitch _refs = refsByAreaId[_areaId];
            switch(_change.Field) {
                case "name":
                    string _newName = (string)_change.Value;
                    _refs.areaText.text = _newName;
                    if (playersAreaEntry == _refs.areaText) FormatPlayersAreaText(_refs.areaText);
                    break;

                default:
                    break;
            }
        }

        private void HandleServerAreaSettingsChange(string _areaId, Colyseus.Schema.DataChange _change) {
            AreaState _area = colyseusClientController.GetLocalState().areas[_areaId];
            AreaListEntryUISwitch _refs = refsByAreaId[_areaId];
            switch(_change.Field) {
                case "distanceCalculationMode":
                    break;

                case "gridWidth":
                    if (currentAreaId == _areaId) {
                        DisplayArea(_area);
                    }
                    break;

                case "gridHeight":
                    if (currentAreaId == _areaId) {
                        DisplayArea(_area);
                    }
                    break;

                case "distanceUnit":
                    break;

                case "gridSquareSideLength":
                    break;

                default:
                    break;
            }
        }

        private void HandleServerFirstState(CampaignState _state) {
            _state.areas.ForEach(delegate (string _areaId, AreaState _area) {
                HandleServerAddArea(_area, _areaId);
            });
            HandleServerPlayersAreaChanged(_state.playerAreaId);
        }

        private void HandleServerPlayersAreaChanged(string _areaId) {
            AreaState _area = colyseusClientController.GetLocalState().areas[_areaId];
            if (colyseusClientController.role == Role.Player) {
                // Player clients should have their area changed, but they don't
                //  see the list of areas, so no need to mark it in there.
                DisplayArea(_area);
            } else if (colyseusClientController.role == Role.GM) {
                // GM clients can freely switch between areas anyway, it must
                //  only be marked on their list, which they *do* see.
                if (_area == null) {
                    NullifyMarkedPlayersArea();
                } else {
                    var _refs = refsByAreaId[_area.id];
                    MarkPlayersArea(_areaId, _refs);
                }
            }
        }

        private void HandleServerRemoveArea(string _areaId) {
            RemoveAreaListEntry(_areaId);
            if (_areaId == currentAreaId) {
                DisplayArea(null);
            }
        }

        private void HandleRemoveAreaButton(string _areaId) {
            RemoveArea(_areaId);
        }

        private void HandleSetPlayersAreaButton(string _areaId) {
            SetPlayersArea(_areaId);
        }

        private void HighlightArea(string _areaId, AreaListEntryUISwitch _refs) {
            if (highlightedAreaEntry != null) {
                highlightedAreaEntry.color = defaultAreaColor;
            }
            defaultAreaColor = _refs.areaEntryImage.color;
            _refs.areaEntryImage.color = highlightedAreaColor;
            highlightedAreaEntry = _refs.areaEntryImage;
        }

        private void MarkPlayersArea(string _areaId, AreaListEntryUISwitch _refs) {
            if (playersAreaEntry != null) {
                RemovePlayersAreaFormatting(playersAreaEntry);
            }
            FormatPlayersAreaText(_refs.areaText);
            playersAreaEntry = _refs.areaText;
        }

        private void NullifyHighlightedArea() {
            if (highlightedAreaEntry != null) {
                highlightedAreaEntry.color = defaultAreaColor;
            }
            highlightedAreaEntry = null;
        }

        private void NullifyMarkedPlayersArea() {
            if (playersAreaEntry != null) {
                RemovePlayersAreaFormatting(playersAreaEntry);
            }
            playersAreaEntry = null;
        }

        private void OpenAreaConfig() {
            pageController.TurnPageOn(PageType.AreaConfig);
        }

        private void RegisterAreaCollectionStateListeners() {
            colyseusClientController.emitter.onAddArea.AddListener(delegate (AreaState _area, string _areaId) {
                HandleServerAddArea(_area, _areaId);
            });
            colyseusClientController.emitter.onAreaChanged.AddListener(delegate (string _areaId, List<Colyseus.Schema.DataChange> _changes) {
                foreach (var _change in _changes) HandleServerAreaChange(_areaId, _change);
            });
            colyseusClientController.emitter.onAreaSettingsChanged.AddListener(delegate (string _areaId, List<Colyseus.Schema.DataChange> _changes) {
                foreach (var _change in _changes) HandleServerAreaSettingsChange(_areaId, _change);
            });
            colyseusClientController.emitter.onPlayersAreaChanged.AddListener(delegate (string _areaId) {
                HandleServerPlayersAreaChanged(_areaId);
            });
            colyseusClientController.emitter.onRemoveArea.AddListener(delegate (AreaState _area, string _areaId) {
                HandleServerRemoveArea(_areaId);
            });
        }

        private void RegisterAreaListEntryUIListeners(string _areaId, AreaListEntryUISwitch _refs) {
            _refs.areaButton.onClick.AddListener(delegate {
                HandleAreaSelection(_areaId, _refs);
            });
            _refs.areaSettingsButton.onClick.AddListener(delegate {
                HandleAreaSettingsButton(_areaId);
            });
            _refs.deleteAreaButton.onClick.AddListener(delegate {
                HandleRemoveAreaButton(_areaId);
            });
            _refs.setPlayerAreaButton.onClick.AddListener(delegate {
                HandleSetPlayersAreaButton(_areaId);
            });
        }

        private void RemoveAreaListEntry(string _areaId) {
            GameObject _obj = entriesByAreaId[_areaId];
            AreaListEntryUISwitch _refs = refsByAreaId[_areaId];

            if (highlightedAreaEntry == _refs.areaEntryImage) NullifyHighlightedArea();

            areasObjectList.RemoveItem(_obj);
            entriesByAreaId.Remove(_areaId);
            refsByAreaId.Remove(_areaId);
        }

        private void RemovePlayersAreaFormatting(Text _areaText) {
            _areaText.text = TrimPlayersAreaSuffix(_areaText.text);
            _areaText.fontStyle = FontStyle.Normal;
        }

        private string TrimPlayersAreaSuffix(string _areaText) {
            int _targetLength = _areaText.Length - playersAreaSuffix.Length;
            return _areaText.Substring(0, _targetLength);
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
            RegisterAreaCollectionStateListeners();
        }

        private void Start() {
            HandleServerFirstState(colyseusClientController.GetLocalState());

            // This is in place of auto-disabler
            gameObject.SetActive(false);
        }
    #endregion
    }
}