﻿using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class CharacterFieldCategoryPanel : EnhancedMonoBehaviour {
    
    #region Unity Editor Fields
        public GameObject categoryHeader;
        public Text categoryHeaderText;
    #endregion

    #region Public Functions
        public float GetHeight() {
            var _vert = gameObject.GetComponent<VerticalLayoutGroup>();
            float _height = _vert.padding.top + (transform.childCount - 1) * _vert.spacing + _vert.padding.bottom;
            foreach (RectTransform _child in transform) {
                _height += _child.rect.height;
            }
            return _height;
        }

        public void SetCategoryHeader(string _header) {
            categoryHeaderText.text = _header;
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!categoryHeader) LogWarning("No categoryHeader attached");
            if (!categoryHeaderText) LogWarning("No categoryHeaderText attached");
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
        }
    #endregion
    }
}