﻿using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI {

    public class MessageBox : EnhancedMonoBehaviour {
    #region Unity Fields
        public Text headerText;
        public Text messageText;
        public PageController pageController;
        public PageType pageType = PageType.MessageBox;
        public EventSystem eventSystem;
    #endregion

    #region Public Functions
        public void ThrowError(string _msg) {
            DisplayMessage("Error", _msg);
        }

        public void DisplayMessage(string _header, string _msg) {
            SetMessage(_header, _msg);
            // Deselect whatever is selected at the moment. The backdrop of the Message Box page
            //  should block out clicks, but if a button is selected, it might still get clicked
            //  through hitting Enter.
            pageController.TurnPageOn(pageType);
            eventSystem.SetSelectedGameObject(null);
        }
        
        public void CloseBox() {
            pageController.TurnPageOff(pageType);
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!headerText) LogWarning("No header Text object attached to: " + gameObject.name);
            if (!messageText) LogWarning("No message Text object attached to: " + gameObject.name);
            if (!pageController) LogWarning("No PageController attached to: " + gameObject.name);
            if (!eventSystem) LogWarning("No EventSystem attached to: " + gameObject.name);
        }

        protected void SetMessage(string _header, string _msg) {
            headerText.text = _header;
            messageText.text = _msg;
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            CheckIntegrity();
        }
    #endregion // Unity Functions
    }
}