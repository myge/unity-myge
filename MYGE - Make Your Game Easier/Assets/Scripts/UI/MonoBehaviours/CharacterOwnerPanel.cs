﻿using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Menu;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class CharacterOwnerPanel : ContextObject {
        private string characterId;
    
    #region Unity Editor Fields
        public ColyseusClientController colyseusClientController;
        public Dropdown ownerSelectionDropdown;
    #endregion

    #region Public Functions
        public async void Submit() {
            string _sessionId = CollectSessionId();
            await colyseusClientController.CallService("setCharacterOwner", new {
                characterId = characterId,
                ownerSessionId = _sessionId
            });
            Close();
        }

        public void EditCharacterOwner(string _characterId) {
            characterId = _characterId;
            CampaignState _state = colyseusClientController.GetLocalState();
            CharacterState _character = _state.characters[characterId];

            LoadSessionIdOptions(_state, _character);
            OpenAtScreenPosition(Input.mousePosition);
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!colyseusClientController) LogWarning("No colyseusClientController attached");
            if (!ownerSelectionDropdown) LogWarning("No ownerSelectionDropdown attached");
        }

        private string CollectSessionId() {
            int _valueIdx = ownerSelectionDropdown.value;
            string _sessionId = ownerSelectionDropdown.options[_valueIdx].text;
            return _sessionId;
        }

        private void LoadSessionIdOptions(CampaignState _state, CharacterState _character) {
            ownerSelectionDropdown.ClearOptions();

            List<string> _optionStrings = new List<string>();
            // Always have an empty option at index 0
            _optionStrings.Add("");
            _state.allPlayersSessionIDs.ForEach((_sessionId, _value) => _optionStrings.Add(_sessionId));
            ownerSelectionDropdown.AddOptions(_optionStrings);

            int _currentIdx = _optionStrings.IndexOf(_character.ownerSessionId);
            ownerSelectionDropdown.value = _currentIdx;
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
        }
    #endregion
    }
}