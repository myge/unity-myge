﻿using Network;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class ImageSelectionPanel : EnhancedMonoBehaviour {
        private TaskCompletionSource<string> submitTCS = null;

        private string selectedImageURI = "";
        private Dictionary<string, ImageThumbnailUISwitch> refsByImageURI = new Dictionary<string, ImageThumbnailUISwitch>();
        
    #region Unity Editor Fields
        public FileCache fileCache;
        public ObjectList thumbsObjectList;
        public float thumbnailPixelsPerUnit = 100f;
    #endregion

    #region Public Functions
        public void Close() {
            if (submitTCS != null) {
                submitTCS.SetResult("");
            }
            ClosePanel();
        }

        public async Task<string> GetImageSelection() {
            // If there already is a pending selection request, wait for it to finish first
            if (submitTCS != null) await submitTCS.Task;

            gameObject.SetActive(true);
            submitTCS = new TaskCompletionSource<string>();
            string _imageURI = await submitTCS.Task;
            return _imageURI;
        }

        public async void RefreshThumbnails() {
            thumbsObjectList.Clear();
            refsByImageURI.Clear();

            string[] _imageURIs = await fileCache.GetAllImageURIs();

            foreach (string _uri in _imageURIs) {
                string _filename = GetFilenameFromURI(_uri);
                GameObject _obj = thumbsObjectList.AddItem(new string[] { _filename });
                Button _btn = _obj.GetComponent<Button>();

                ImageThumbnailUISwitch _refs = GetThumbnailRefs(_obj);
                refsByImageURI.Add(_uri, _refs);

                fileCache.GetTexture(_uri, delegate (Texture2D _texture) {
                    Sprite _sprite = GetThumbnailSprite(_texture);
                    _refs.thumbnailImage.sprite = _sprite;
                    _refs.thumbnailImage.preserveAspect = true;
                });

                _btn.onClick.AddListener(delegate {
                    SelectImage(_uri);
                });
            }
        }

        public void Submit() {
            if (submitTCS == null) {
                LogWarning("You're trying to submit an image selection dialog answer without there being any pending request");
                return;
            }

            submitTCS.SetResult(selectedImageURI);
            ClosePanel();
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!fileCache) LogWarning("No fileCache attached");
            if (!thumbsObjectList) LogWarning("No thumbsObjectList attached");
        }

        private void ClosePanel() {
            gameObject.SetActive(false);
        }


        private string GetFilenameFromURI(string _uri) {
            string[] _values = _uri.Split('/');
            string _filename = _values[_values.Length - 1];
            return _filename;
        }

        private ImageThumbnailUISwitch GetThumbnailRefs(GameObject _thumbObj) {
            ObjectReferenceSwitch _switch = _thumbObj.GetComponent<ObjectReferenceSwitch>();
            return new ImageThumbnailUISwitch(_switch);
        }

        private Sprite GetThumbnailSprite(Texture2D _texture) {
            return Sprite.Create(
                texture: _texture,
                rect: new Rect(0, 0, _texture.width, _texture.height),
                pivot: new Vector2(0, 0),
                pixelsPerUnit: thumbnailPixelsPerUnit,
                extrude: 0,
                meshType: SpriteMeshType.Tight
            );
        }

        private void HighlightImageSelection(string _imageURI) {
            var _refs = refsByImageURI[_imageURI];
            _refs.selectionBackdrop.gameObject.SetActive(true);
        }

        private void DehighlightImageSelection(string _imageURI) {
            var _refs = refsByImageURI[_imageURI];
            _refs.selectionBackdrop.gameObject.SetActive(false);
        }

        private void SelectImage(string _imageURI) {
            if (selectedImageURI != "") DehighlightImageSelection(selectedImageURI);
            if (_imageURI != "") HighlightImageSelection(_imageURI);
            selectedImageURI = _imageURI;
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            CheckIntegrity();
            RefreshThumbnails();
        }

        private void OnDisable() {
            thumbsObjectList.Clear();
            refsByImageURI.Clear();
        }
    #endregion
    }
}