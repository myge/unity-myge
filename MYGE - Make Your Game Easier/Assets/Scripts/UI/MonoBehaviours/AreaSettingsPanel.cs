﻿using Network;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    
    public class AreaSettingsPanel : EnhancedMonoBehaviour {

    #region Unity Editor Fields
        public Dropdown distanceCalcModeDropdown;
        public InputField distanceUnitInput;
        public InputField gridXInput;
        public InputField gridYInput;
        public InputField gridSquareSideInput;
    #endregion

    #region Public Functions
        public AreaSettings CollectAreaSettings() {
            AreaSettings _settings = new AreaSettings();

            _settings.definedFields = new string[] {
                "distanceCalculationMode",
                "gridWidth",
                "gridHeight",
                "gridSquareSideLength",
                "distanceUnit"
            }.ToArraySchema();

            _settings.distanceCalculationMode = distanceCalcModeDropdown.value;
            _settings.distanceUnit = distanceUnitInput.text.Trim();
            _settings.gridWidth = int.Parse(gridXInput.text);
            _settings.gridHeight = int.Parse(gridYInput.text);
            // The decimal number with InvariantCulture is read with a dot, not a comma.
            _settings.gridSquareSideLength = double.Parse(gridSquareSideInput.text.Replace(',', '.'), CultureInfo.InvariantCulture);

            return _settings;
        }

        public void LoadAreaSettings(AreaSettings _settings) {
            distanceCalcModeDropdown.value = _settings.distanceCalculationMode;
            distanceUnitInput.text = _settings.distanceUnit;
            gridXInput.text = _settings.gridWidth.ToString();
            gridYInput.text = _settings.gridHeight.ToString();
            gridSquareSideInput.text = _settings.gridSquareSideLength.ToString();
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!distanceCalcModeDropdown) LogWarning("No distanceCalcModeDropdown attached");
            if (!distanceUnitInput) LogWarning("No distanceUnitInput attached");
            if (!gridXInput) LogWarning("No gridXInput attached");
            if (!gridYInput) LogWarning("No gridYInput attached");
            if (!gridSquareSideInput) LogWarning("No gridSquareSideInput attached");
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            CheckIntegrity();
        }
    #endregion
    }
}