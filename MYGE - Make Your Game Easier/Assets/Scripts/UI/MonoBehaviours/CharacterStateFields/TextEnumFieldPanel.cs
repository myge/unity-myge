using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class TextEnumFieldPanel : RulesetFieldPanel {

    #region Unity Editor Fields
        public Dropdown dropdown;
    #endregion

    #region Public Functions
        public override void ClearValues() {
            base.ClearValues();

            dropdown.value = 0;
        }

        public void LoadFieldSpecification(TextEnumField _field) {
            base.LoadFieldSpecification(_field);

            dropdown.ClearOptions();

            List<string> _optionStrings = new List<string>();
            // Always have an empty option at index 0
            _optionStrings.Add("");
            _field.options.ForEach((_option) => _optionStrings.Add(_option));

            dropdown.AddOptions(_optionStrings);
        }

        public void LoadFieldValue(TextEnumValue _value) {
            base.LoadFieldValue(_value);

            dropdown.SetValueWithoutNotify(_value.optionIdx);
        }

        public override void SetEditable(bool _canEdit) {
            base.SetEditable(_canEdit);

            dropdown.interactable = _canEdit;
        }
    #endregion

    #region Private Functions
        
    #endregion
    }
}