﻿using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class BoolFlagFieldPanel : RulesetFieldPanel {

    #region Unity Editor Fields
        public Toggle flagToggle;
    #endregion

    #region Public Functions
        public override void ClearValues() {
            //base.ClearValues();

            flagToggle.isOn = false;
        }

        public void LoadFieldSpecification(BoolFlagField _field) {
            // BoolFlagFields are the only fields that don't have generic toggles, so
            //  the base method which refers to the generic toggle must not be called.
            //  Thus, the name must be handled manually.
            //base.LoadFieldSpecification(_field);
            nameText.text = _field.name;
        }

        public void LoadFieldValue(BoolFlagValue _value) {
            //base.LoadFieldValue(_value);

            flagToggle.isOn = _value.value;
        }

        public override void SetEditable(bool _canEdit) {
            //base.SetEditable(_canEdit);

            flagToggle.interactable = _canEdit;
        }
    #endregion

    #region Private Functions
        
    #endregion
    }
}