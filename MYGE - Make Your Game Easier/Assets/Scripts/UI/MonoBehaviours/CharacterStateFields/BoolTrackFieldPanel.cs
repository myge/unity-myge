﻿using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI {

    public class BoolTrackFieldPanel : RulesetFieldPanel {
        private List<Toggle> togglesByIdx = new List<Toggle>();
        
    #region Unity Editor Fields
        public ObjectList togglesObjList;
    #endregion

        public UnityEvent onTrackValueChanged = new UnityEvent();

    #region Public Functions
        public override void ClearValues() {
            base.ClearValues();

            foreach (Toggle _toggle in togglesByIdx) {
                _toggle.isOn = false;
            }
        }

        public bool[] GetTogglesValues() {
            bool[] _values = new bool[togglesByIdx.Count];
            for (int i = 0; i < togglesByIdx.Count; i++) {
                _values[i] = togglesByIdx[i].isOn;
            }
            return _values;
        }

        public void LoadFieldSpecification(BoolTrackField _field) {
            base.LoadFieldSpecification(_field);

            togglesObjList.Clear();
            togglesByIdx.Clear();

            _field.toggleLabels.ForEach((_label) => {
                GameObject _toggleObj = togglesObjList.AddItem(new string[]{ _label });

                // Use ObjectList to create the layout and keep track of each object's
                //  Toggle component separately.
                Toggle _toggle = _toggleObj.GetComponent<Toggle>();
                if (!_toggle) LogWarning("Bool track toggle object has no Toggle component");
                togglesByIdx.Add(_toggle);

                // Propagate the value change events upwards
                _toggle.onValueChanged.AddListener(delegate {
                    onTrackValueChanged.Invoke();
                });
            });
        }

        public void LoadFieldValue(BoolTrackValue _value) {
            if (_value.toggles.Count != togglesByIdx.Count) {
                LogWarning("Number of toggles in the interface (" + togglesByIdx.Count + ") not equal to the number of toggles in the state (" + _value.toggles.Count + ")");
                return;
            }

            for (int i = 0; i < _value.toggles.Count; i++) {
                togglesByIdx[i].isOn = _value.toggles[i];
            }
        }

        public override void SetEditable(bool _canEdit) {
            foreach (Toggle _toggle in togglesByIdx) {
                _toggle.interactable = _canEdit;
            }
        }
    #endregion

    #region Private Functions
        
    #endregion
    }
}