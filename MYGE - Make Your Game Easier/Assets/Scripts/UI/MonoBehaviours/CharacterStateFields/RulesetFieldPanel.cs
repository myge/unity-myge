using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public abstract class RulesetFieldPanel : EnhancedMonoBehaviour {

    #region Unity Editor Fields
        public Text nameText;
        public Toggle genericToggle;
    #endregion

    #region Public Functions
        public virtual void ClearValues() {
            genericToggle.isOn = false;
        }

        public virtual void SetEditable(bool _canEdit) {
            genericToggle.interactable = _canEdit;
        }
    #endregion

    #region Private Functions
        protected void LoadFieldSpecification(RulesetField _field) {
            nameText.text = _field.name;
            genericToggle.gameObject.SetActive(_field.showGenericToggle);
        }

        protected void LoadFieldValue(RulesetFieldValue _value) {
            genericToggle.isOn = _value.genericToggle;
        }
    #endregion
    }
}