using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class TextFieldPanel : RulesetFieldPanel {

    #region Unity Editor Fields
        public InputField textInput;
    #endregion

    #region Public Functions
        public override void ClearValues() {
            base.ClearValues();

            textInput.text = "";
        }

        public void LoadFieldSpecification(TextField _field) {
            base.LoadFieldSpecification(_field);
        }

        public void LoadFieldValue(TextValue _value) {
            base.LoadFieldValue(_value);

            textInput.SetTextWithoutNotify(_value.text);
        }

        public override void SetEditable(bool _canEdit) {
            base.SetEditable(_canEdit);

            textInput.interactable = _canEdit;
        }
    #endregion

    #region Private Functions
        
    #endregion
    }
}