﻿using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class AttributeFieldPanel : RulesetFieldPanel {

    #region Unity Editor Fields
        public InputField baseValueInput;
        public InputField tempModInput;
        public InputField tempOverrideInput;
        public Text finalValueText;
        public Button clickableNameButton;
    #endregion

    #region Public Functions
        public override void ClearValues() {
            base.ClearValues();

            baseValueInput.text = "";
            tempModInput.text = "";
            tempOverrideInput.text = "";
            finalValueText.text = "";
        }

        public void LoadFieldSpecification(AttributeField _field) {
            base.LoadFieldSpecification(_field);

            string _rollCommand = _field.rollCommandArgument == null ? "" :_field.rollCommandArgument.Trim();
            bool _rollCommandIsDefined = (_rollCommand != "");
            clickableNameButton.interactable = _rollCommandIsDefined;
        }

        public void LoadFieldValue(AttributeValue _value) {
            base.LoadFieldValue(_value);
            baseValueInput.SetTextWithoutNotify(_value.value.ToString());
            tempModInput.SetTextWithoutNotify(_value.tempModifier != 0 ? _value.tempModifier.ToString() : "");
            tempOverrideInput.SetTextWithoutNotify(_value.tempValueOverride != 0 ? _value.tempValueOverride.ToString() : "");
            finalValueText.text = GetFinalValue(_value).ToString();
        }

        public override void SetEditable(bool _canEdit) {
            base.SetEditable(_canEdit);

            baseValueInput.interactable = _canEdit;
            tempModInput.interactable = _canEdit;
            tempOverrideInput.interactable = _canEdit;
        }
    #endregion

    #region Private Functions
        private int GetFinalValue(AttributeValue _value) {
            // TODO: Add a separate flag that determines if the override is used or not.
            //  An override of 0 should also be legal.
            if (_value.tempValueOverride != 0) {
                return _value.tempValueOverride;
            } else {
                return _value.value + _value.tempModifier;
            }
        }
    #endregion
    }
}