using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class ResourceFieldPanel : RulesetFieldPanel {

    #region Unity Editor Fields
        public InputField currentValueInput;
        public InputField currentTempModInput;
        public InputField maxValueInput;
        public InputField maxTempModInput;
        public InputField tempBufferInput;
    #endregion

    #region Public Functions
        public override void ClearValues() {
            base.ClearValues();

            currentValueInput.text = "";
            currentTempModInput.text = "";
            maxValueInput.text = "";
            maxTempModInput.text = "";
            tempBufferInput.text = "";
        }

        public void LoadFieldSpecification(ResourceField _field) {
            base.LoadFieldSpecification(_field);
        }

        public void LoadFieldValue(ResourceValue _value) {
            base.LoadFieldValue(_value);

            currentValueInput.SetTextWithoutNotify(_value.current.ToString());
            currentTempModInput.SetTextWithoutNotify(_value.tempCurrentModifier != 0 ? _value.tempCurrentModifier.ToString() : "");
            maxValueInput.SetTextWithoutNotify(_value.max.ToString());
            maxTempModInput.SetTextWithoutNotify(_value.tempMaxModifier != 0 ? _value.tempMaxModifier.ToString() : "");
            tempBufferInput.SetTextWithoutNotify(_value.tempBuffer != 0 ? _value.tempBuffer.ToString() : "");
        }

        public override void SetEditable(bool _canEdit) {
            base.SetEditable(_canEdit);

            currentValueInput.interactable = _canEdit;
            currentTempModInput.interactable = _canEdit;
            maxValueInput.interactable = _canEdit;
            maxTempModInput.interactable = _canEdit;
            tempBufferInput.interactable = _canEdit;
        }
    #endregion

    #region Private Functions
        
    #endregion
    }
}