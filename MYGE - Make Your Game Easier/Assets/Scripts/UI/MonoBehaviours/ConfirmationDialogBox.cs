﻿using System.Threading.Tasks;
using UnityCore.Menu;

namespace UI {

    public class ConfirmationDialogBox : MessageBox {
        private TaskCompletionSource<bool> confirmationTCS = null;

    #region Unity Fields
        public string defaultHeader = "Are you sure?";
    #endregion

    #region Public Functions
        public async Task<bool> GetConfirmation(string _question) {
            // If there is a pending confirmation request, wait for it to finish first
            if (confirmationTCS != null) await confirmationTCS.Task;

            DisplayMessage(defaultHeader, _question);
            confirmationTCS = new TaskCompletionSource<bool>();
            bool _confirmed = await confirmationTCS.Task;
            return _confirmed;
        }

        public void Answer(bool _answer) {
            // If there is no pending request, just log a warning
            if (confirmationTCS == null) {
                LogWarning("You're trying to answer a confirmation dialog without there being any pending confirmation request");
                return;
            }
            
            confirmationTCS.SetResult(_answer);
            CloseBox();
        }

        public void AnswerYes() {
            Answer(true);
        }

        public void AnswerNo() {
            Answer(false);
        }
    #endregion

    #region Private Functions

    #endregion

    #region Unity Functions

    #endregion
    }
}