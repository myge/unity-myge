﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace UI {

    public class Command {
        public delegate Task CommandAction(params string[] args);

        private CommandAction action;

        public string id { get; private set; }
        public int requiredArgs { get; private set; }

    #region Public Methods
        public Command(string _id, CommandAction _action, int _requiredArgs=0) {
            if (_id.Contains(" ")) {
                throw new System.ArgumentOutOfRangeException("_id", _id, "The command ID may not contain spaces");
            }
            action = _action;
            id = _id;
            requiredArgs = _requiredArgs;
        }

        public async Task Execute(params string[] args) {
            if (args.Length < requiredArgs) {
                throw new System.ArgumentException("\"" + id + "\" command takes " + requiredArgs.ToString() + " arguments (" + args.Length + " given).");
            }
            await action(args);
        }
    #endregion
    }
}