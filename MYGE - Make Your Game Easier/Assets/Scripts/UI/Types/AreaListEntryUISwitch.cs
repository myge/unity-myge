﻿using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class AreaListEntryUISwitch {
        public Button areaButton { get; private set; }
        public Image areaEntryImage { get; private set; }
        public Text areaText { get; private set; }
        public Button deleteAreaButton { get; private set; }
        public Button areaSettingsButton { get; private set; }
        public Button setPlayerAreaButton { get; private set; }

    #region Public Functions
        public AreaListEntryUISwitch(ObjectReferenceSwitch _objs) {
            AssignReferences(_objs);
        }
    #endregion

    #region Private Functions
        private void AssignReferences(ObjectReferenceSwitch _objs) {
            areaButton = _objs.GetGameObject("Area Button").GetComponent<Button>();
            areaEntryImage = _objs.GetGameObject("Area Button").GetComponent<Image>();
            areaText = _objs.GetGameObject("Area Text").GetComponent<Text>();
            deleteAreaButton = _objs.GetGameObject("Delete Area Button").GetComponent<Button>();
            areaSettingsButton = _objs.GetGameObject("Area Settings Button").GetComponent<Button>();
            setPlayerAreaButton = _objs.GetGameObject("Set Player Area Button").GetComponent<Button>();
        }
    #endregion
    }
}