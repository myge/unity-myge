﻿using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class RulesetFieldUISwitch {
        public GameObject attributeFieldSection { get; private set; }
        public GameObject boolFieldSection { get; private set; }
        public GameObject boolTrackFieldSection { get; private set; }
        public GameObject resourceFieldSection { get; private set; }
        public GameObject textEnumSection { get; private set; }
        public GameObject textSection { get; private set; }

        public Button deleteFieldButton { get; private set; }
        public Dropdown fieldTypeDropdown { get; private set; }

        // All fields
        public InputField fieldNameInput { get; private set; }
        public InputField categoryInput { get; private set; }
        public Toggle showGenericToggleToggle { get; private set; }
        public Toggle gmOnlyToggle { get; private set; }
        public Dropdown[] visualizationModeDropdowns { get; private set; }

        // AttributeField
        public InputField attrTextColorHexInput { get; private set; }
        public InputField attrRollInput { get; private set; }

        // BoolFlagField
        public InputField boolSymbolColorHexInput { get; private set; }
        
        // BoolTrackField
        public InputField trackToggleLabelsInput { get; private set; }

        // ResourceField
        public InputField resBarColorHexInput { get; private set; }

        // TextEnumField
        public InputField enumTextColorHexInput { get; private set; }
        public InputField enumOptionsInput { get; private set; }

        // TextField
        public InputField textTextColorHexInput { get; private set; }

    #region Public Functions
        public RulesetFieldUISwitch(ObjectReferenceSwitch _objs) {
            AssignReferences(_objs);
        }
    #endregion

    #region Private Functions
        private void AssignReferences(ObjectReferenceSwitch _objs) {
            attributeFieldSection = _objs.GetGameObject("Attribute Field Options");
            boolFieldSection = _objs.GetGameObject("Bool Field Options");
            boolTrackFieldSection = _objs.GetGameObject("Bool Track Options");
            resourceFieldSection = _objs.GetGameObject("Resource Field Options");
            textEnumSection = _objs.GetGameObject("Text Enum Options");
            textSection = _objs.GetGameObject("Text Field Options");
            
            deleteFieldButton = _objs.GetGameObject("Field Delete Button").GetComponent<Button>();
            fieldTypeDropdown = _objs.GetGameObject("Field Type Dropdown").GetComponent<Dropdown>();

            fieldNameInput = _objs.GetGameObject("Field Name Input").GetComponent<InputField>();
            categoryInput = _objs.GetGameObject("Field Category Input").GetComponent<InputField>();
            showGenericToggleToggle = _objs.GetGameObject("Field Use Toggle Toggle").GetComponent<Toggle>();
            gmOnlyToggle = _objs.GetGameObject("Field GM Only Toggle").GetComponent<Toggle>();

            visualizationModeDropdowns = new Dropdown[] {
                _objs.GetGameObject("Attribute Display Mode Dropdown").GetComponent<Dropdown>(),
                _objs.GetGameObject("Bool Display Mode Dropdown").GetComponent<Dropdown>(),
                _objs.GetGameObject("Bool Track Display Mode Dropdown").GetComponent<Dropdown>(),
                _objs.GetGameObject("Resource Display Mode Dropdown").GetComponent<Dropdown>(),
                _objs.GetGameObject("Text Enum Display Mode Dropdown").GetComponent<Dropdown>(),
                _objs.GetGameObject("Text Display Mode Dropdown").GetComponent<Dropdown>(),
            };

            attrTextColorHexInput = _objs.GetGameObject("Attribute Text Color Hex Input").GetComponent<InputField>();
            attrRollInput = _objs.GetGameObject("Attribute Roll Input").GetComponent<InputField>();

            boolSymbolColorHexInput = _objs.GetGameObject("Bool Text Color Hex Input").GetComponent<InputField>();

            trackToggleLabelsInput = _objs.GetGameObject("Bool Track Options Input").GetComponent<InputField>();
            
            resBarColorHexInput = _objs.GetGameObject("Resource Bar Color Hex Input").GetComponent<InputField>();

            enumTextColorHexInput = _objs.GetGameObject("Text Enum Color Hex Input").GetComponent<InputField>();
            enumOptionsInput = _objs.GetGameObject("Text Enum Options Input").GetComponent<InputField>();

            textTextColorHexInput = _objs.GetGameObject("Text Text Color Hex Input").GetComponent<InputField>();
        }
    #endregion
    }
}