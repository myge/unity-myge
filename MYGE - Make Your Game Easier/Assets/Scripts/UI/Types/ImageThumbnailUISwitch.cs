﻿using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class ImageThumbnailUISwitch {
        public Image thumbnailImage { get; private set; }
        public Image selectionBackdrop { get; private set; }

    #region Public Functions
        public ImageThumbnailUISwitch(ObjectReferenceSwitch _objs) {
            AssignReferences(_objs);
        }
    #endregion

    #region Private Functions
        private void AssignReferences(ObjectReferenceSwitch _objs) {
            thumbnailImage = _objs.GetGameObject("Image Thumbnail").GetComponent<Image>();
            selectionBackdrop = _objs.GetGameObject("Image Thumbnail Selection Backdrop").GetComponent<Image>();
        }
    #endregion
    }
}