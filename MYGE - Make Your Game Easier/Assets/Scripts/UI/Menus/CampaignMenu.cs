﻿using Network;
using Game;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI {

    public class CampaignMenu : EnhancedMonoBehaviour {
        public ColyseusClientController colyseusClientController;
        public GridManager grid;
        public PageController pageController;
        public Button showAreaListButton;
        public ConfirmationDialogBox confirmation;

        public GameObject[] gmOnlyInterfaceElements;

    #region Public Functions
        public void ShowAreaSelection() {
            pageController.TurnPageOn(PageType.AreaList);
            showAreaListButton.gameObject.SetActive(false);
        }

        public void HideAreaSelection() {
            pageController.TurnPageOff(PageType.AreaList);
            showAreaListButton.gameObject.SetActive(true);
        }

        public async void DeleteRoom() {
            if (!await confirmation.GetConfirmation("Are you sure you want to delete the whole campaign room? All areas, tokens and characters within this room will be lost.\n\nThis operation cannot be reverted.")) return;
            // This is not awaited on purpose - the request is supposed to be sent, but we're
            //  unlikely to get a result, given how the action is to blow everything up.
            colyseusClientController.CallService("deleteRoom");
            pageController.TurnPageOff(PageType.Campaign);

            // Instead wait for the special message broadcast from the room before the explosion.
            colyseusClientController.emitter.onRoomDeleted.AddListener(delegate {
                ReloadScene();
            });
        }

        public async void LeaveRoom() {
            if (!await confirmation.GetConfirmation("Are you sure you want to leave?")) return;
            await colyseusClientController.LeaveRoom();
            pageController.TurnPageOff(PageType.Campaign);
            ReloadScene();
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!colyseusClientController) LogWarning("No colyseusClientController attached");
            if (!grid) LogWarning("No grid attached");
            if (!pageController) LogWarning("No pageController attached");
            if (!showAreaListButton) LogWarning("No showAreaListButton attached");
            if (!confirmation) LogWarning("No confirmationDialogBox attached");
        }

        private void ConfigureWithRespectToRole() {
            if (colyseusClientController.role == Role.None) {
                LogWarning("CampaignMenu was enabled, but current local role is \"None\"");
            } else if (colyseusClientController.role == Role.Player) {
                // Just disable things that only the GM should have access to
                foreach (GameObject _obj in gmOnlyInterfaceElements) {
                    _obj.SetActive(false);
                }
            }
        }

        private void ReloadScene() {
            // Reload entire scene
            Scene _scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(_scene.name);
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            CheckIntegrity();
            grid.gameObject.SetActive(true);
            ConfigureWithRespectToRole();
        }

        private void OnDisable() {
            grid.gameObject.SetActive(false);
        }
    #endregion
    }
}