﻿using Config;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;

namespace UI {

    public class MainMenu : EnhancedMonoBehaviour {
        public PageController pageController;

    #if UNITY_EDITOR
        public bool alwaysRestoreFactorySettings = true;
    #else
        public bool alwaysRestoreFactorySettings { get { return false; } }
    #endif

    #region Public Functions
        public void MainMenuToLANRoomList() {
            pageController.SwitchPage(PageType.Menu, PageType.LANRoomList);
        }

        public void MainMenuToOptions() {
            pageController.SwitchPage(PageType.Menu, PageType.Options);
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!pageController) LogWarning("No PageController attached");
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            // All initialization code should go here
            if (alwaysRestoreFactorySettings || !LocalConfig.firstRunDone) {
                Log("Program launched for the first time. Initializing.");
                LocalConfig.RestoreFactorySettings();
                LocalConfig.firstRunDone = true;
            }
        }

        private void OnEnable() {
            CheckIntegrity();
        }
    #endregion
    }
}