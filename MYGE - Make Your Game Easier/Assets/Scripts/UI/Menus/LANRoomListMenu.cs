﻿using Network;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class LANRoomListMenu : EnhancedMonoBehaviour {
        private CampaignRoomAvailable highlightedRoom;

    #region Unity Editor Fields
        public ColyseusClientController colyseusClientController;
        public ObjectList roomList;
        public Button joinRoomButton;
        public InputField ipInput;
        public MessageBox messageBox;
        public PageController pageController;
    #endregion
    
    #region Public Functions
        public async void CreateRoom() {
            SetHighlightedRoom(null);

            // TODO: Enter a "Please wait..." state, where no inputs can be given and exit it only once
            //  the async task of creating a room has been completed.
            string _ip = ipInput.text;
            colyseusClientController.SetTargetServerIP(_ip);
            bool _result = await colyseusClientController.CreateRoom();

            if (!_result) {
                messageBox.ThrowError("Cannot create a new room at " + colyseusClientController.serverIP.ToString() +  ".");
            } else {
                pageController.SwitchPage(PageType.LANRoomList, PageType.Campaign);
            }
        }

        public async void JoinRoom() {
            // TODO: Enter a "Please wait..." state, where no inputs can be given and exit it only once
            //  the async task of joining a room has been completed.
            if (highlightedRoom == null) {
                messageBox.ThrowError("No room to join selected.");
                return;
            }

            bool _result;
            if (highlightedRoom.isOnline) {
                _result = await colyseusClientController.JoinRoom(highlightedRoom);
            } else {
                _result = await colyseusClientController.RecreateRoom(highlightedRoom);
            }

            if (!_result) {
                messageBox.ThrowError("Cannot join room \"" + highlightedRoom.metadata.visibleName + "\".");
            } else {
                SetHighlightedRoom(null);
                pageController.SwitchPage(PageType.LANRoomList, PageType.Campaign);
            }
        }

        public async void Refresh() {
            // TODO: Enter a "Please wait..." state, where no inputs can be given and exit it only once
            //  the async task of looking up rooms has been completed.

            // Don't want to preserve old rooms, that might no longer exist, as selected
            SetHighlightedRoom(null);
            roomList.Clear();

            string _ip = ipInput.text;
            colyseusClientController.SetTargetServerIP(_ip);
            IEnumerable<CampaignRoomAvailable> _rooms = await colyseusClientController.GetAvailableRooms();

            if (_rooms == null) {
                messageBox.ThrowError("Cannot connect to " + _ip + ".");
            } else {
                foreach (CampaignRoomAvailable _room in _rooms) {
                    AddRoomAvailableToList(_room);
                }
            }
        }

        public void LANRoomListToMainMenu() {
            SetHighlightedRoom(null);
            pageController.SwitchPage(PageType.LANRoomList, PageType.Menu);
        }
    #endregion

    #region Private Functions
        private void AddRoomAvailableToList(CampaignRoomAvailable _room) {
            GameObject _roomObj = roomList.AddItem(new string[] {
                FormatRoomName(_room),
                FormatRoomClients(_room),
                FormatRoomMaxClients(_room)
            });

            UIEventEmitter _roomClickable = _roomObj.GetComponent<UIEventEmitter>();
            if(!_roomClickable) {
                LogWarning("The object representing the room entry has no UIEventEmitter component, cannot register onClick and onDeselect listeners");
                return;
            }

            _roomClickable.onClick.AddListener(delegate {
                HandleRoomEntryClicked(_room);
            });
        }

        private void CheckIntegrity() {
            if (!colyseusClientController) LogWarning("No ColyseusClientController attached");
            if (!roomList) LogWarning("No room ObjectList attached");
            if (!joinRoomButton) LogWarning("No Join Room button attached");
            if (!ipInput) LogWarning("No IP text input attached");
            if (!messageBox) LogWarning("No MessageBox attached");
            if (!pageController) LogWarning("No PageController attached");
        }

        private string FormatRoomClients(CampaignRoomAvailable _room) {
            if (_room.isOnline) {
                return _room.clients.ToString();
            } else {
                return "-";
            }
        }

        private string FormatRoomMaxClients(CampaignRoomAvailable _room) {
            return _room.maxClients.ToString();
        }

        private string FormatRoomName(CampaignRoomAvailable _room) {
            if (_room.isOnline) {
                return _room.metadata.visibleName;
            } else {
                return _room.metadata.visibleName + " <b>(Saved)</b>";
            }
        }

        private void HandleRoomEntryClicked(CampaignRoomAvailable _room) {
            Log("Room object clicked for room ID " + _room.roomId);
            SetHighlightedRoom(_room);
        }
        
        private void SetHighlightedRoom(CampaignRoomAvailable _room) {
            highlightedRoom = _room;
            if (_room == null) {
                SetJoinRoomAvailability(false);
            } else {
                SetJoinRoomAvailability(true);
            }
        }

        private void SetJoinRoomAvailability(bool _available) {
            joinRoomButton.interactable = _available;
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            CheckIntegrity();
            SetJoinRoomAvailability(false);
        }
    #endregion
    }
}