﻿using Config;
using Network;
using System.Collections;
using System.Collections.Generic;
using UnityCore.Menu;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UI {

    public class OptionsMenu : EnhancedMonoBehaviour {
        private PageType currentTabPage;
        private Dictionary<PageType, Image> buttonsByTabPage;

    #region Unity Editor Fields
        public RulesetOptionsPanel rulesetOptionsPanel;
        public AreaSettingsPanel areaSettingsPanel;
        public PageController pageController;
        public PageType startingTabPage;
        public Image areaTabButton;
        public Image rulesetTabButton;
        public Color tabColor;
        public Color activeTabColor;
    #endregion

    #region Public Functions
        public void OptionsToMainMenu() {
            pageController.TurnPageOff(currentTabPage);
            pageController.SwitchPage(PageType.Options, PageType.Menu);
        }

        public void ToAreaTab() {
            SwitchTab(PageType.AreaLocalDefaults);
        }

        public void ToRulesetTab() {
            SwitchTab(PageType.RulesetLocalDefaults);
        }

        public void SaveChanges() {
            LocalConfig.areaSettingDefaults = areaSettingsPanel.CollectAreaSettings();
            LocalConfig.rpgSettingDefualts = CollectRPGSettings();
        }
    #endregion

    #region Private Functions
        private void CheckIntegrity() {
            if (!rulesetOptionsPanel) LogWarning("No rulesetOptionsPanel attached");
            if (!areaSettingsPanel) LogWarning("No areaSettingsPanel attached");
            if (!pageController) LogWarning("No PageController attached");
            if (!areaTabButton) LogWarning("No area tab button attached");
            if (!rulesetTabButton) LogWarning("No ruleset tab button attached");
        }

        private RPGSettings CollectRPGSettings() {
            RPGSettings _settings = new RPGSettings();

            _settings.definedFields = new string[] {
                "ruleset"
            }.ToArraySchema();

            _settings.ruleset = rulesetOptionsPanel.CollectRuleset();

            return _settings;
        }

        private void HighlightTab(PageType _targetTab) {
            if (!buttonsByTabPage.ContainsKey(_targetTab)) {
                LogWarning("You are trying to highlight a tab page button, but there is no button assigned to page type " + _targetTab.ToString());
                return;
            }

            foreach (PageType _tab in buttonsByTabPage.Keys) {
                Image _btn = buttonsByTabPage[_tab];
                if (_tab == _targetTab) {
                    SetButtonColorHighlight(_btn);
                } else {
                    SetButtonColorNormal(_btn);
                }
            }
        }

        private void Init() {
            buttonsByTabPage = new Dictionary<PageType, Image>();
            buttonsByTabPage[PageType.AreaLocalDefaults] = areaTabButton;
            buttonsByTabPage[PageType.RulesetLocalDefaults] = rulesetTabButton;

            SwitchTab(startingTabPage);
        }

        private void LoadCurrentConfig() {
            RPGSettings _rpgSettings = LocalConfig.rpgSettingDefualts;
            rulesetOptionsPanel.LoadRuleset(_rpgSettings.ruleset);

            areaSettingsPanel.LoadAreaSettings(LocalConfig.areaSettingDefaults);
        }

        private void SetButtonColorHighlight(Image _btn) {
            _btn.color = activeTabColor;
        }

        private void SetButtonColorNormal(Image _btn) {
            _btn.color = tabColor;
        }

        private void SwitchTab(PageType _newTab) {
            if (currentTabPage == _newTab) return;

            if (currentTabPage == PageType.None) {
                pageController.TurnPageOn(_newTab);
            } else {
                pageController.SwitchPage(currentTabPage, _newTab);
            }
            currentTabPage = _newTab;

            HighlightTab(currentTabPage);
        }
    #endregion

    #region Unity Functions
        private void OnEnable() {
            CheckIntegrity();
            Init();
            LoadCurrentConfig();
        }

        private void OnDisable() {
            currentTabPage = PageType.None;
        }
    #endregion
    }
}