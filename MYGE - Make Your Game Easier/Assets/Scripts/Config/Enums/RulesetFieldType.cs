﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config {

    public enum RulesetFieldType {
        Attribute = 1,
        Bool = 3,
        BoolTrack = 5,
        Resource = 2,
        TextEnum = 4,
        Text = 0,
        Invalid = -1
    };
}