﻿using Network;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config {

    [Serializable]
    public class SessionData {
        public CampaignRoomAvailable roomData;
        public Role role;
        public string sessionId;

        public SessionData(CampaignRoomAvailable _roomData, Role _role, string _sessionId) {
            roomData = _roomData;
            role = _role;
            sessionId = _sessionId;
        }
    }
}