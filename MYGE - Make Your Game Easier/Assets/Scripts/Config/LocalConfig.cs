﻿using Network;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

namespace Config {

    public static class LocalConfig {
        public static bool firstRunDone {
            get { return LoadFirstRunDone(); }
            set { SetFirstRunDone(value); }
        }

        public static AreaSettings areaSettingDefaults {
            get { return LoadAreaSettingDefaults(); }
            set { SetAreaSettingDefaults(value); }
        }

        public static RPGSettings rpgSettingDefualts {
            get { return LoadRPGSettingDefaults(); }
            set { SetRPGSettingDefaults(value); }
        }

        public static ReadOnlyDictionary<string, SessionData> savedSessions {
            get { return LoadSavedSessionsReadOnly(); }
        }

    #region Public Functions
        public static void DeleteSavedSession(string _sessionId) {
            var _savedSessions = LoadSavedSessions();
            _savedSessions.Remove(_sessionId);
            SaveSessions(_savedSessions);
        }

        public static void DeleteSavedSession(IEnumerable<string> _sessionIDs) {
            var _savedSessions = LoadSavedSessions();
            foreach (string _sessionId in _sessionIDs) {
                _savedSessions.Remove(_sessionId);
            }
            SaveSessions(_savedSessions);
        }

        public static void DeleteSessionsForRoom(string _roomId) {
            var _sessionsToDelete = GetSessionsForRoom(_roomId);
            var _sessionIDsToDelete = _sessionsToDelete.Select(_session => _session.sessionId);
            DeleteSavedSession(_sessionIDsToDelete);
        }

        public static List<CampaignRoomAvailable> GetHibernatedRooms() {
            var _hibernatedRoomsData = new List<CampaignRoomAvailable>();
            var _roomIDsProcessed = new HashSet<string>();
            foreach (SessionData _session in savedSessions.Values) {
                if (!_roomIDsProcessed.Contains(_session.roomData.roomId)) {
                    // Make sure to mark that the room is indeed hibernated
                    _session.roomData.isOnline = false;

                    _hibernatedRoomsData.Add(_session.roomData);
                    _roomIDsProcessed.Add(_session.roomData.roomId);
                }
            }
            return _hibernatedRoomsData;
        }

        public static ReadOnlyCollection<SessionData> GetSessionsForRoom(string _roomId) {
            var _sessions = new List<SessionData>();
            foreach (SessionData _session in savedSessions.Values) {
                if (_session.roomData.roomId == _roomId) {
                    _sessions.Add(_session);
                }
            }
            return _sessions.AsReadOnly();
        }

        public static void RestoreFactorySettings() {
            Clear();
            SetAreaSettingDefaults(GetFactoryAreaSettings());
            SetRPGSettingDefaults(GetFactoryRPGSettings());
        }

        public static void StoreSessionInfo(string _sessionId, SessionData _session) {
            var _savedSessions = LoadSavedSessions();
            if (_savedSessions.ContainsKey(_sessionId)) {
                _savedSessions[_sessionId] = _session;
            } else {
                _savedSessions.Add(_sessionId, _session);
            }
            SaveSessions(_savedSessions);
        }
    #endregion

    #region Private Functions
        private static void Clear() {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }

        private static AreaSettings GetFactoryAreaSettings() {
            AreaSettings _s = new AreaSettings();

            _s.definedFields = new string[] {
                "distanceCalculationMode",
                "gridWidth",
                "gridHeight",
                "gridSquareSideLength",
                "distanceUnit"
            }.ToArraySchema();

            _s.distanceCalculationMode = 0;
            _s.gridWidth = 10;
            _s.gridHeight = 10;
            _s.gridSquareSideLength = 5.0;
            _s.distanceUnit = "ft";

            return _s;
        }

        private static RPGSettings GetFactoryRPGSettings() {
            RPGSettings _s = new RPGSettings();

            _s.definedFields = new string[] {
                "ruleset"
            }.ToArraySchema();

            _s.ruleset = GetFactoryRuleset();

            return _s;
        }

        private static Ruleset GetFactoryRuleset() {
            Ruleset _r = new Ruleset();

            _r.name = "Test";
            
            TextField _name = new TextField();
            _name.name = "Name";
            _name.textColorHex = "#ffffff";
            _r.AddField(_name);

            TextField _pronouns = new TextField();
            _pronouns.name = "Pronouns";
            _r.AddField(_pronouns);

            TextEnumField _alignment = new TextEnumField();
            _alignment.name = "Alignment";
            _alignment.options = new string[] {
                "Lawful Good",
                "Neutral Good",
                "Chaotic Good",
                "Lawful Neutral",
                "True Neutral",
                "Chaotic Neutral",
                "Lawful Evil",
                "Neutral Evil",
                "Chaotic Evil"
            }.ToArraySchema();
            _alignment.showGenericToggle = true;
            _r.AddField(_alignment);

            ResourceField _health = new ResourceField();
            _health.name = "Health";
            _health.category = "Resources";
            _health.barColorHex = "#ff0000";
            _r.AddField(_health);

            ResourceField _mana = new ResourceField();
            _mana.name = "Mana";
            _mana.category = "Resources";
            _mana.barColorHex = "#0000ff";
            _r.AddField(_mana);

            AttributeField _might = new AttributeField();
            _might.name = "Might";
            _might.category = "Abilities";
            _r.AddField(_might);
            
            AttributeField _magic = new AttributeField();
            _magic.name = "Magic";
            _magic.category = "Abilities";
            _r.AddField(_magic);

            TextField _remark = new TextField();
            _remark.name = "Remark";
            _remark.category = "Notes";
            _r.AddField(_remark);
            
            TextField _gmRemark = new TextField();
            _gmRemark.name = "GM Remark";
            _gmRemark.category = "Notes";
            _gmRemark.gmOnly = true;
            _r.AddField(_gmRemark);

            BoolFlagField _rested = new BoolFlagField();
            _rested.name = "Rested";
            _rested.category = "Conditions";
            _r.AddField(_rested);

            BoolTrackField _fear = new BoolTrackField();
            _fear.name = "Fear";
            _fear.category = "Conditions";
            _fear.toggleLabels = new string[] {
                "1",
                "2",
                "3",
                "4",
                "5",
                "6"
            }.ToArraySchema();
            _r.AddField(_fear);

            return _r;
        }

        private static bool LoadFirstRunDone() {
            return PlayerPrefs.GetInt("firstRunDone") == 1;
        }

        private static AreaSettings LoadAreaSettingDefaults() {
            string _json = PlayerPrefs.GetString("areaSettingDefaults");
            Debug.Log("[LocalConfig] Loading local area setting defaults: " + _json);
            return SchemaConverter.DeserializeAreaSettings(_json);
        }

        private static RPGSettings LoadRPGSettingDefaults() {
            string _json = PlayerPrefs.GetString("rpgSettingDefaults");
            Debug.Log("[LocalConfig] Loading local RPG setting defaults: " + _json);
            return SchemaConverter.DeserializeRPGSettings(_json);
        }

        private static Dictionary<string, SessionData> LoadSavedSessions() {
            string _json = PlayerPrefs.GetString("savedSessions");
            if (_json == null || _json == "") return new Dictionary<string, SessionData>();
            else return JsonConvert.DeserializeObject<Dictionary<string, SessionData>>(_json);
        }

        private static ReadOnlyDictionary<string, SessionData> LoadSavedSessionsReadOnly() {
            string _json = PlayerPrefs.GetString("savedSessions");
            if (_json == null || _json == "") return new ReadOnlyDictionary<string, SessionData>(new Dictionary<string, SessionData>());
            else return JsonConvert.DeserializeObject<ReadOnlyDictionary<string, SessionData>>(_json);
        }

        private static void SetFirstRunDone(bool _firstRunDone) {
            PlayerPrefs.SetInt("firstRunDone", _firstRunDone ? 1 : 0);
            PlayerPrefs.Save();
        }

        private static void SetAreaSettingDefaults(AreaSettings _s) {
            string _json = _s.ToJson();
            Debug.Log("[LocalConfig] Saving local area setting defaults: " + _json);
            PlayerPrefs.SetString("areaSettingDefaults", _json);
            PlayerPrefs.Save();
        }

        private static void SetRPGSettingDefaults(RPGSettings _s) {
            string _json = _s.ToJson();
            Debug.Log("[LocalConfig] Saving local RPG setting defaults: " + _json);
            PlayerPrefs.SetString("rpgSettingDefaults", _json);
            PlayerPrefs.Save();
        }

        private static void SaveSessions(Dictionary<string, SessionData> _savedSessions) {
            string _json = JsonConvert.SerializeObject(_savedSessions);
            Debug.Log("[LocalConfig] Saving sessions: " + _json);
            PlayerPrefs.SetString("savedSessions", _json);
            PlayerPrefs.Save();
        }
    #endregion
    }
}