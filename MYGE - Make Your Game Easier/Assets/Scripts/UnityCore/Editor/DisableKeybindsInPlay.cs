﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.ShortcutManagement;
using UnityEngine;

[InitializeOnLoad]
public static class DisableKeybindsInPlay {

    public const string PLAY_PROFILE_ID = "Play";

    private static bool playProfileExists;
    private static string editProfileId = "Default";

#region Public Methods
    // Initialize
    static DisableKeybindsInPlay() {
        // First, check if a "play" keybind profile exists
        playProfileExists = DoesPlayProfileExist();
        editProfileId = ShortcutManager.instance.activeProfileId;

        EditorApplication.playModeStateChanged += delegate (PlayModeStateChange _stateChange) {
            if (_stateChange == PlayModeStateChange.EnteredPlayMode) {
                SetKeybindsToPlay();
            } else if (_stateChange == PlayModeStateChange.EnteredEditMode) {
                SetKeybindsToEdit();
            }
        };
        EditorApplication.quitting += delegate {
            SetKeybindsToEdit();
        };
    }
#endregion

#region Private Methods
    private static bool DoesPlayProfileExist() {
        var _profileIds = ShortcutManager.instance.GetAvailableProfileIds();
        foreach (string _profileId in _profileIds) {
            if (_profileId == PLAY_PROFILE_ID) {
                return true;
            }
        }

        // Nothing was found
        return false;
    }

    private static void SetKeybindsToEdit() {
        ShortcutManager.instance.activeProfileId = editProfileId;
    }

    private static void SetKeybindsToPlay() {
        editProfileId = ShortcutManager.instance.activeProfileId;

        if (playProfileExists) {
            ShortcutManager.instance.activeProfileId = PLAY_PROFILE_ID;
        } else {
            Debug.LogWarningFormat("\"{0}\" Editor keybind profile does not exist and you have entered Play Mode. Please create such a profile with all keybinds disabled, otherwise the Editor will react to your keybind inputs even in play mode!", PLAY_PROFILE_ID);
        }
    }
#endregion

}

#endif