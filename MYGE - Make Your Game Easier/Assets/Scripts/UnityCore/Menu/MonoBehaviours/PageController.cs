﻿using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;

namespace UnityCore {

    namespace Menu {

        public class PageController : EnhancedMonoBehaviour
        {
            private Hashtable pagesByPageType;
        
        #region Unity Fields
            public PageType startingPage;
            public Page[] pages;
        #endregion // Unity Fields

        #region Public Functions
            public void TurnPageOn(PageType _type) {
                if (_type == PageType.None) return;
                if (!IsPageRegistered(_type)) {
                    LogWarning("You are trying to turn a page on of type \"" + _type + "\" that has not been registered.");
                    return;
                }

                Page _page = GetPage(_type);
                _page.Turn(true);
            }

            public void TurnPageOff(PageType _type) {
                if (_type == PageType.None) return;
                if (!IsPageRegistered(_type)) {
                    LogWarning("You are trying to turn a page off of type \"" + _type + "\" that has not been registered.");
                    return;
                }

                Page _page = GetPage(_type);
                if (_page.gameObject.activeSelf) {
                    _page.Turn(false);
                }
            }

            public void SwitchPage(PageType _typeSource, PageType _typeDestination, bool _crossFade=false) {
                if (_typeSource == PageType.None) return;
                if (_typeDestination == PageType.None) return;
                if (!IsPageRegistered(_typeSource)) {
                    LogWarning("You are trying to switch from a page of type \"" + _typeSource + "\" that has not been registered.");
                    return;
                }
                if (!IsPageRegistered(_typeDestination)) {
                    LogWarning("You are trying to switch to a page of type \"" + _typeDestination + "\" that has not been registered.");
                    return;
                }

                TurnPageOff(_typeSource);
                if (_crossFade) {
                    TurnPageOn(_typeDestination);
                } else {
                    Page _pageSource = GetPage(_typeSource);
                    Page _pageDestination = GetPage(_typeDestination);
                    StartCoroutine(WaitForPageExitAndTurnOn(_pageSource, _pageDestination));
                }
            }
        #endregion // Public Functions

        #region Private Functions
            private IEnumerator WaitForPageExitAndTurnOn(Page _pageSource, Page _pageDestination) {
                while (_pageSource.targetState != PageState.None) {
                    yield return null;
                }
                
                TurnPageOn(_pageDestination.type);
            }

            private void RegisterAllPages() {
                foreach(Page _page in pages) {
                    RegisterPage(_page);
                }
            }

            private void RegisterPage(Page _page) {
                if (IsPageRegistered(_page.type)) {
                    LogWarning("You are trying to register a page of type \"" + _page.type + "\" that has already been registered: " + _page.gameObject.name);
                    return;
                }

                pagesByPageType.Add(_page.type, _page);
                Log("New page registered of type \"" + _page.type + "\": " + _page.gameObject.name);
            }

            private Page GetPage(PageType _type) {
                if (!IsPageRegistered(_type)) {
                    LogWarning("You are trying to get a page of type \"" + _type + "\" that has not been registered.");
                    return null;
                }

                return (Page)pagesByPageType[_type];
            }

            private bool IsPageRegistered(PageType _type) {
                return pagesByPageType.ContainsKey(_type);
            }
        #endregion // Private Functions

        #region Unity Functions
            private void Awake() {
                pagesByPageType = new Hashtable();
                RegisterAllPages();

                if (startingPage != PageType.None) {
                    TurnPageOn(startingPage);
                }
            }
        #endregion // Unity Functions

        }
    }
}