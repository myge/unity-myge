﻿using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UnityCore {

    namespace Menu {

        public class ObjectList : EnhancedMonoBehaviour {
            
            private List<GameObject> entryObjects = new List<GameObject>();
        
        #region Unity Fields
            public GameObject templateObject;
            public Text[] textLabels;
        #endregion // Unity Fields

        #region Public Functions
            public GameObject AddItem(string[] _itemData) {
                if (entryObjects == null) {
                    LogWarning("The entryObjects list is null when items are about to be added to it");
                }
                
                AssignItemDataToTemplate(_itemData);
                GameObject _obj = Object.Instantiate(templateObject, gameObject.transform);
                if (!_obj.activeSelf) {
                    _obj.SetActive(true);
                }
                entryObjects.Add(_obj);
                return _obj;
            }

            public void Clear() {
                if (entryObjects == null) return;

                foreach(GameObject _obj in entryObjects) {
                    _obj.SetActive(false);
                    Destroy(_obj);
                }
                entryObjects.Clear();
            }

            public void RemoveItem(GameObject _obj) {
                if (_obj == null) return;

                _obj.SetActive(false);
                entryObjects.Remove(_obj);
                Destroy(_obj);
            }
        #endregion // Public Functions

        #region Private Functions
            private void AssignItemDataToTemplate(string[] _itemData) {
                if (_itemData.Length != textLabels.Length) {
                    LogWarning("You are assigning " + _itemData.Length + " strings to a template with " + textLabels.Length + " labels");
                }

                for (int i = 0; i < textLabels.Length; i++) {
                    if (i < _itemData.Length) {
                        textLabels[i].text = _itemData[i];
                    } else {
                        textLabels[i].text = "";
                    }
                }
            }

            private void CheckIntegrity() {
                if (!templateObject) LogWarning("No template object assigned to: " + gameObject.name);
                if (gameObject == templateObject)
                    LogWarning("You set the object \"" + gameObject.name + "\"holding the list as a template for itself");
            }
        #endregion // Private Functions

        #region Unity Functions
            private void OnEnable() {
                CheckIntegrity();
            }
        #endregion // Unity Functions

        }
    }
}