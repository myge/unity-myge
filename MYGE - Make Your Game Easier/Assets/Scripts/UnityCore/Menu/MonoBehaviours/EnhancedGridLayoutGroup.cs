﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityCore {

    namespace Menu {

        public class EnhancedGridLayoutGroup : LayoutGroup {

        #region Unity Editor Fields
            public int rows;
            public int columns;
            public Vector2 cellSize;
            public Vector2 spacing;
            public GridLayoutFitType fitType;

            public bool fitRows;
            public bool fitColumns;
        #endregion

        #region Public Methods
            public override void CalculateLayoutInputHorizontal()
            {
                base.CalculateLayoutInputHorizontal();

                if (IsFitTypeFullyAutomatic()) {
                    int _gridSide = Mathf.CeilToInt(Mathf.Sqrt(transform.childCount));
                    rows = _gridSide;
                    columns = _gridSide;

                    fitRows = true;
                    fitColumns = true;
                }

                if (fitType == GridLayoutFitType.Width || fitType == GridLayoutFitType.FixedColumns)
                    rows = Mathf.CeilToInt(transform.childCount / (float)columns);
                if (fitType == GridLayoutFitType.Height || fitType == GridLayoutFitType.FixedRows)
                    columns = Mathf.CeilToInt(transform.childCount / (float)rows);

                float _layoutWidth = rectTransform.rect.width;
                float _layoutHeight = rectTransform.rect.height;

                if (fitColumns) cellSize.x = (_layoutWidth - padding.left - padding.right) / (float)columns - (spacing.x * (columns - 1) / (float)columns);
                if (fitRows) cellSize.y = (_layoutHeight - padding.top - padding.bottom) / (float)rows - (spacing.y * (rows - 1) / (float)rows);

                int _row = 0;
                int _col = 0;

                for (int i = 0; i < rectChildren.Count; i++) {
                    _row = i / columns;
                    _col = i % columns;
                    RectTransform _child = rectChildren[i];

                    float _x = (cellSize.x + spacing.x) * _col + padding.left;
                    float _y = (cellSize.y + spacing.y) * _row + padding.top;

                    SetChildAlongAxis(_child, 0, _x, cellSize.x);
                    SetChildAlongAxis(_child, 1, _y, cellSize.y);
                }
            }

            public override void CalculateLayoutInputVertical() {}
            public override void SetLayoutHorizontal() {}
            public override void SetLayoutVertical() {}
        #endregion

        #region Private Methods
            private bool IsFitTypeFullyAutomatic() {
                return (fitType == GridLayoutFitType.Uniform || fitType == GridLayoutFitType.Width || fitType == GridLayoutFitType.Height);
            }
        #endregion
        }
    }
}