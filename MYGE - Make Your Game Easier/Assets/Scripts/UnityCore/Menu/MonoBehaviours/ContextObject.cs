﻿using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;

namespace UnityCore {

    namespace Menu {

        public class ContextObject : EnhancedMonoBehaviour {

        #region Public Methods
            public void Close() {
                gameObject.SetActive(false);
            }

            public void OpenAtScreenPosition(Vector3 _screenPos) {
                Vector3 _clampedScreenPos = ClampScreenPos(_screenPos);
                gameObject.transform.position = _clampedScreenPos;
                gameObject.SetActive(true);
            }
        #endregion

        #region Private Methods
            private Vector3 ClampScreenPos(Vector3 _screenPos) {
                RectTransform _rect = (RectTransform) transform;
                Vector3[] _corners = new Vector3[4];
                _rect.GetWorldCorners(_corners);

                float _rectWidth = _corners[2].x - _corners[1].x;
                float _rectHeight = _corners[1].y - _corners[0].y;

                float _minX = _rectWidth * _rect.pivot.x;
                float _maxX = Screen.width - _rectWidth * (1 - _rect.pivot.x);

                float _minY = _rectHeight * _rect.pivot.y;
                float _maxY = Screen.height - _rectHeight * (1 - _rect.pivot.y);

                return new Vector3(
                    x: Mathf.Clamp(_screenPos.x, _minX, _maxX),
                    y: Mathf.Clamp(_screenPos.y, _minY, _maxY),
                    z: _screenPos.z
                );
            }
        #endregion

        #region Unity Functions
            private void Update() {
                
            }
        #endregion
        }
    }
}