﻿using System.Collections;
using UnityCore.Utils;
using UnityEngine;

namespace UnityCore {

    namespace Menu {

        public class Page : EnhancedMonoBehaviour {

            private Animator animator;

        #region Unity Fields
            public PageType type;
            public bool useAnimation;
        #endregion // Unity Fields
        
        #region Public Properties
            public PageState targetState { get; private set; }
        #endregion // Public Properties

        #region Public Functions
            public void Turn(bool _on) {
                if (_on) {
                    gameObject.SetActive(true);
                }

                if (IsUsingAnimations()) {
                    animator.SetBool("on", _on);

                    StartCoroutine(AwaitAnimationAndTurn(_on));
                } else {
                    if (!_on) {
                        gameObject.SetActive(false);
                    }
                }
            }
        #endregion // Public Functions

        #region Private Functions
            private IEnumerator AwaitAnimationAndTurn(bool _on) {
                targetState = _on ? PageState.On : PageState.Off;
                
                // Wait for animator to reach the target state
                while (!animator.GetCurrentAnimatorStateInfo(0).IsName(targetState.ToString())) {
                    yield return null;
                }

                // Wait for animator to finish animating
                while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1) {
                    yield return null;
                }

                targetState = PageState.None;

                Log("Page of type \"" + type + "\" finished transitioning to " + (_on ? "on" : "off"));

                if (!_on) {
                    gameObject.SetActive(false);
                }
            }

            private void CheckAnimatorIntegrity() {
                if (IsUsingAnimations()) {
                    animator = GetComponent<Animator>();
                    if (!animator) {
                        LogWarning("You opted to animate a page of type \"" + type + "\", but no Animator component exists on the page gameObject.");
                    }
                }
            }

            private bool IsUsingAnimations() {
                return useAnimation;
            }
        #endregion // Private Functions

        #region Unity Functions
            private void OnEnable() {
                CheckAnimatorIntegrity();
            }
        #endregion // Unity Functions

        }
    }
}