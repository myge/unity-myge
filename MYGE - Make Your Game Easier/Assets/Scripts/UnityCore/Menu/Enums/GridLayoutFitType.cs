﻿namespace UnityCore {

    namespace Menu {

        public enum GridLayoutFitType {
            Uniform,
            Width,
            Height,
            FixedRows,
            FixedColumns
        }
    }
}