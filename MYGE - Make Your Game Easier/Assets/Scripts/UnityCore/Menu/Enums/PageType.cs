﻿namespace UnityCore {

    namespace Menu {

        public enum PageType {
            None,
            Loading,
            Menu,
            MessageBox,
            LANRoomList,
            Campaign,
            Options,
            AreaLocalDefaults,
            RulesetLocalDefaults,
            ConfirmationBox,
            AreaList,
            AreaConfig
        }
    }
}