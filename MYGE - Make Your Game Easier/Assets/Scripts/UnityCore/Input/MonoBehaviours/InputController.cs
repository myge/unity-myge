﻿using System.Collections;
using System.Collections.Generic;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UnityCore {

    namespace Input {

        public class InputController : EnhancedMonoBehaviour {
            private HashSet<KeyCode> handledKeyPresses = new HashSet<KeyCode>();
            private HashSet<KeyCode> handledKeyReleases = new HashSet<KeyCode>();
            private HashSet<string> handledAxes = new HashSet<string>();

            private List<InputLayer> layers = new List<InputLayer>();
            private Dictionary<string, InputLayer> layersById = new Dictionary<string, InputLayer>();

        #region Public Functions
            public void AddInputLayer(InputLayer _layer) {
                if (layersById.ContainsKey(_layer.id)) {
                    LogWarning("Cannot add InputLayer of ID \"" + _layer.id + "\", it already exists. Perhaps you're trying to add the same layer twice?");
                    return;
                }

                layers.Add(_layer);
                layersById.Add(_layer.id, _layer);

                // Key presses
                foreach (KeyCode _key in _layer.GetHandledKeyPresses()) {
                    if (!handledKeyPresses.Contains(_key)) {
                        handledKeyPresses.Add(_key);
                    }
                }

                // Key releases
                foreach (KeyCode _key in _layer.GetHandledKeyReleases()) {
                    if (!handledKeyReleases.Contains(_key)) {
                        handledKeyReleases.Add(_key);
                    }
                }

                // Axes
                foreach (string _axisName in _layer.GetHandledAxes()) {
                    if (!handledAxes.Contains(_axisName)) {
                        handledAxes.Add(_axisName);
                    }
                }
            }
        #endregion

        #region Private Functions
            // Run this function only inside Update(), otherwise GetKeyDown() will not work
            private void HandleInputs() {
                HashSet<KeyCode> _consumedKeyPresses = new HashSet<KeyCode>();
                HashSet<KeyCode> _consumedKeyReleases = new HashSet<KeyCode>();
                HashSet<string> _consumedAxes = new HashSet<string>();
                
                foreach (InputLayer _layer in layers) {
                    // Don't consider diasbled layers in any way
                    if (!_layer.enabled) continue;

                    foreach (KeyCode _key in handledKeyPresses) {
                        // Skip this key if the key has been consumed
                        if (_consumedKeyPresses.Contains(_key)) continue;

                        if (UnityEngine.Input.GetKeyDown(_key)) {
                            // HandleKeyPress returns false if it has consumed the event
                            bool _consume = !(_layer.HandleKeyPress(_key));

                            // If the layer always consumes, don't bother keeping track of 
                            //  consumed keys, as the loop will be broken after this layer.
                            if (!_layer.alwaysConsumes && _consume) {
                                _consumedKeyPresses.Add(_key);
                            }
                        }
                    }

                    // Same logic as above
                    foreach (KeyCode _key in handledKeyReleases) {
                        if (_consumedKeyReleases.Contains(_key)) continue;

                        if (UnityEngine.Input.GetKeyUp(_key)) {
                            bool _consume = !(_layer.HandleKeyRelease(_key));
                            if (!_layer.alwaysConsumes && _consume) {
                                _consumedKeyReleases.Add(_key);
                            }
                        }
                    }

                    // Same logic as in the loop above with keys
                    foreach (string _axisName in handledAxes) {
                        if (_consumedAxes.Contains(_axisName)) continue;

                        float _axisRawValue = UnityEngine.Input.GetAxisRaw(_axisName);
                        if (_axisRawValue != 0) {
                            float _axisValue = UnityEngine.Input.GetAxis(_axisName);

                            bool _consume = !(_layer.HandleAxis(_axisName, _axisValue, _axisRawValue));
                            if (!_layer.alwaysConsumes && _consume) {
                                _consumedAxes.Add(_axisName);
                            }
                        }
                    }

                    // If the layer always consumes, there is no need to go through
                    //  any other layers, as it's obvious everything was consumed
                    if (_layer.alwaysConsumes) {
                        break;
                    }
                }
            }
        #endregion

        #region Unity Functions
            private void Update() {
                HandleInputs();
            }
        #endregion
        }
    }
}