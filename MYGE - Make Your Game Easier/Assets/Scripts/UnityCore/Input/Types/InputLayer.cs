﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCore {

    namespace Input {
        public delegate bool InputHandler();
        public delegate bool AxisHandler(float _axisValue, float _axisRawValue);
        public delegate bool Predicate();
        
        public class InputLayer {
            private bool _enabled;
            private Dictionary<KeyCode, InputHandler> keyPressHandlers;
            private Dictionary<KeyCode, InputHandler> keyReleaseHandlers;
            private Dictionary<string, AxisHandler> axisHandlers;
            private Predicate predicate;

            public bool alwaysConsumes { get; private set; }
            public bool enabled {
                // If a special predicate is assigned and returns false, consider the layer disabled
                get { return predicate != null ? _enabled && predicate() : _enabled; }
                set { _enabled = value; }
            }
            public string id { get; private set; }

        #region Public Functions
            public InputLayer(string _id, 
                                Dictionary<KeyCode, InputHandler> _keyPressHandlers=null,
                                Dictionary<KeyCode, InputHandler> _keyReleaseHandlers=null,
                                Dictionary<string, AxisHandler> _axisHandlers=null,
                                bool _alwaysConsumes=true,
                                bool _startEnabled=true,
                                Predicate _predicate=null) {
                keyPressHandlers = _keyPressHandlers != null ? new Dictionary<KeyCode, InputHandler>(_keyPressHandlers) : new Dictionary<KeyCode, InputHandler>();
                keyReleaseHandlers = _keyReleaseHandlers != null ? new Dictionary<KeyCode, InputHandler>(_keyReleaseHandlers) : new Dictionary<KeyCode, InputHandler>();
                axisHandlers = _axisHandlers != null ? new Dictionary<string, AxisHandler>(_axisHandlers) : new Dictionary<string, AxisHandler>();
                predicate = _predicate;
                alwaysConsumes = _alwaysConsumes;
                enabled = _startEnabled;
                id = _id;
            }

            public void AddAxisHandler(string _axisName, AxisHandler _handler) {
                axisHandlers.Add(_axisName, _handler);
            }

            public void AddKeyPressHandler(KeyCode _key, InputHandler _handler) {
                keyPressHandlers.Add(_key, _handler);
            }

            public void AddKeyReleaseHandler(KeyCode _key, InputHandler _handler) {
                keyReleaseHandlers.Add(_key, _handler);
            }

            public Dictionary<string, AxisHandler>.KeyCollection GetHandledAxes() {
                return axisHandlers.Keys;
            }

            public Dictionary<KeyCode, InputHandler>.KeyCollection GetHandledKeyPresses() {
                return keyPressHandlers.Keys;
            }

            public Dictionary<KeyCode, InputHandler>.KeyCollection GetHandledKeyReleases() {
                return keyReleaseHandlers.Keys;
            }

            public bool HandleAxis(string _axisName, float _axisValue, float _axisRawValue) {
                if (!enabled) {
                    return true;
                }
                if (!axisHandlers.ContainsKey(_axisName)) {
                    return true;
                }
                AxisHandler _handler = axisHandlers[_axisName];
                bool _result = _handler(_axisValue, _axisRawValue);
                return _result;
            }

            public bool HandleKeyPress(KeyCode _key) {
                if (!enabled) {
                    // If the layer is not enabled, it never consumes or does anything, so
                    //  return true to pass the control to other layers.
                    return true;
                }

                if (!keyPressHandlers.ContainsKey(_key)) {
                    // If the layer has no event for a key, pass control to other layers
                    //  by returning true.
                    return true;
                }

                InputHandler _handler = keyPressHandlers[_key];
                bool _result = _handler();

                return _result;
            }

            public bool HandleKeyRelease(KeyCode _key) {
                if (!enabled) {
                    return true;
                }
                if (!keyReleaseHandlers.ContainsKey(_key)) {
                    return true;
                }

                InputHandler _handler = keyReleaseHandlers[_key];
                bool _result = _handler();

                return _result;
            }
        #endregion
        }
    }
}