﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCore {

    namespace Utils {

        public abstract class EnhancedMonoBehaviour : MonoBehaviour {
        #region Unity Fields
            public bool debug = true;
        #endregion

        #region Private Methods
            protected virtual void Log(string _msg) {
                if (!debug) return;

                Debug.Log("[" + this.GetType().Name + "] " + _msg);
            }

            protected virtual void LogWarning(string _msg) {
                if (!debug) return;
                
                Debug.LogWarning("[" + this.GetType().Name + "] " + _msg);
            }
        #endregion
        }
    }
}

