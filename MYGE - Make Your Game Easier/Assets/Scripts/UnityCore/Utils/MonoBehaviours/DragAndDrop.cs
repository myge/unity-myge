﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCore {

    namespace Utils {

        public class DragAndDrop : EnhancedMonoBehaviour {
            private bool drag = false;
            private Vector3 startingPosition;

            protected Vector3 previousMousePosition;
            protected Vector3 mouseDelta;
            protected Vector3 mouseDeltaWorld;

        #region Unity Fields
            
        #endregion

        #region Public Functions
            public void Drag() {
                startingPosition = transform.position;
                drag = true;
            }

            public Vector3 Drop(bool _return = false) {
                Vector3 _droppedPosition = transform.position;
                drag = false;
                if (_return) Return();
                return _droppedPosition;
            }

            public void Return() {
                Log("Returning token");
                drag = false;
                transform.position = startingPosition;
            }
        #endregion

        #region Private Functions
            private void TakeDeltaStep() {
                transform.position += mouseDeltaWorld;
            }

            protected virtual void UpdateMousePositions() {
                mouseDelta = UnityEngine.Input.mousePosition - previousMousePosition;
                mouseDeltaWorld = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition) - Camera.main.ScreenToWorldPoint(previousMousePosition);
                previousMousePosition = UnityEngine.Input.mousePosition;
            }
        #endregion

        #region Unity Functions
            private void Update() {
                if (drag) {
                    TakeDeltaStep();
                }
            }

            private void LateUpdate() {
                UpdateMousePositions();
            }
        #endregion
        }
    }
}