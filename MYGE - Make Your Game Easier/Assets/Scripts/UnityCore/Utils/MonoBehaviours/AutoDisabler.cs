﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCore {

    namespace Utils {

        public class AutoDisabler : EnhancedMonoBehaviour {
            public enum DisablingTrigger {
                None,
                Start,
                Awake
            }

        #region Unity Fields
            public DisablingTrigger trigger;
        #endregion

        #region Unity Functions
            private void Awake() {
                if (trigger == DisablingTrigger.Awake) {
                    Log("Disabling " + gameObject.name);
                    gameObject.SetActive(false);
                }
            }

            private void Start() {
                if (trigger == DisablingTrigger.Start) {
                    Log("Disabling " + gameObject.name);
                    gameObject.SetActive(false);
                }
            }
        #endregion
        }
    }
}