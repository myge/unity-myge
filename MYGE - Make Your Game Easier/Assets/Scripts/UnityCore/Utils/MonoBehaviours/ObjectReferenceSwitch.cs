﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCore {

    namespace Utils {

        public class ObjectReferenceSwitch : EnhancedMonoBehaviour {
            private Dictionary<string, GameObject> objectsByName = new Dictionary<string, GameObject>();
        #region Unity Fields
            public GameObject[] objects;
        #endregion

        #region Public Functions
            public GameObject GetGameObject(string _name) {
                // If the dict is empty, there is a chance CollectObjects() hasn't ran yet,
                //  for example because the object this component is on hasn't been enabled.
                // Thus, if the dict is empty, run CollectObjects() one time, just to give
                //  it a shot.
                if (objectsByName.Count == 0) {
                    CollectObjects();
                }

                if (objectsByName.ContainsKey(_name)) {
                    return objectsByName[_name];
                } else {
                    return null;
                }
            }

            public Dictionary<string, GameObject>.KeyCollection GetObjectNames() {
                return objectsByName.Keys;
            }
        #endregion

        #region Private Functions
            private void CollectObjects() {
                objectsByName.Clear();
                try {
                    foreach (GameObject _obj in objects) {
                        objectsByName.Add(_obj.name, _obj);
                    }
                } catch (System.ArgumentException) {
                    LogWarning("Objects in an ObjectReferenceSwitch must have unique names");
                    return;
                }
            }
        #endregion

        #region Unity Functions
            private void OnEnable() {
                CollectObjects();
            }
        #endregion
        }
    }
}
