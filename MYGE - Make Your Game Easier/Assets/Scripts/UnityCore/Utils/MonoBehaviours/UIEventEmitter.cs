﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UnityCore {

    namespace Utils {

        public class UIEventEmitter : MonoBehaviour, IDeselectHandler
        {
        
        #region Unity Fields
            public bool debug;
        #endregion

        #region Public Properties
            public UnityEvent onClick { get; private set; }
            public UnityEvent onDeselect { get; private set; }
        #endregion

        #region Private Functions
            private void CheckEventIntegrity() {
                Button _btn = GetComponent<Button>();
                if (!_btn) {
                    LogWarning("Object has no Button component, cannot emit Button events: " + gameObject.name);
                } else {
                    onClick = _btn.onClick;
                }

                if (onDeselect == null) onDeselect = new UnityEvent();
            }

            private void Log(string _msg) {
                if (!debug) return;

                Debug.Log("[UIEventEmitter] " + _msg);
            }

            private void LogWarning(string _msg) {
                if (!debug) return;

                Debug.LogWarning("[UIEventEmitter] " + _msg);
            }
        #endregion

        #region Unity Functions
            private void Awake() {
                CheckEventIntegrity();
            }
            public void OnDeselect(BaseEventData eventData) {
                onDeselect.Invoke();
            }
        #endregion
        }
    }
}