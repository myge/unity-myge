using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityCore {

    namespace Utils {

        public class RenderTextureDragAndDrop : DragAndDrop {

        #region Unity Fields
            public RectTransform textureTransform;
            public Camera renderingCamera;
        #endregion

        #region Public Functions

        #endregion

        #region Private Functions
            private Vector3 ScreenToWorld(Vector3 _pos) {
                Vector2 _localPoint;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(
                    textureTransform,
                    new Vector3(_pos.x, _pos.y, 0),
                    null,
                    out _localPoint
                );
                
                Vector2 _normalizedPoint = _localPoint 
                    / new Vector2(textureTransform.rect.width, textureTransform.rect.height)
                    + textureTransform.pivot;

                Ray _ray = renderingCamera.ViewportPointToRay(_normalizedPoint);

                return _ray.origin;
            }

            protected override void UpdateMousePositions() {
                mouseDelta = UnityEngine.Input.mousePosition - previousMousePosition;
                mouseDeltaWorld = ScreenToWorld(UnityEngine.Input.mousePosition) - ScreenToWorld(previousMousePosition);
                previousMousePosition = UnityEngine.Input.mousePosition;
            }
        #endregion

        #region Unity Functions

        #endregion
        }
    }
}