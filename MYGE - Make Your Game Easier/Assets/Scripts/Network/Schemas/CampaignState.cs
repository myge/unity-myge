// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class CampaignState : Schema {
		[Type(0, "string")]
		public string hostSessionId = default(string);

		[Type(1, "map", typeof(MapSchema<bool>), "boolean")]
		public MapSchema<bool> allPlayersSessionIDs = new MapSchema<bool>();

		[Type(2, "string")]
		public string visibleName = default(string);

		[Type(3, "ref", typeof(RPGSettings))]
		public RPGSettings rpgSettings = new RPGSettings();

		[Type(4, "ref", typeof(AreaSettings))]
		public AreaSettings areaSettingsDefault = new AreaSettings();

		[Type(5, "array", typeof(ArraySchema<string>), "string")]
		public ArraySchema<string> orderedAreas = new ArraySchema<string>();

		[Type(6, "map", typeof(MapSchema<AreaState>))]
		public MapSchema<AreaState> areas = new MapSchema<AreaState>();

		[Type(7, "map", typeof(MapSchema<CharacterState>))]
		public MapSchema<CharacterState> characters = new MapSchema<CharacterState>();

		[Type(8, "string")]
		public string playerAreaId = default(string);
	}
}
