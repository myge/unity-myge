// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class AreaSettings : SettingsGroup {
		[Type(1, "int32")]
		public int distanceCalculationMode = default(int);

		[Type(2, "int32")]
		public int gridWidth = default(int);

		[Type(3, "int32")]
		public int gridHeight = default(int);

		[Type(4, "string")]
		public string distanceUnit = default(string);

		[Type(5, "float64")]
		public double gridSquareSideLength = default(double);
	}
}
