// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class AttributeField : RulesetField {
		[Type(5, "string")]
		public string textColorHex = default(string);

		[Type(6, "string")]
		public string rollCommandArgument = default(string);
	}
}
