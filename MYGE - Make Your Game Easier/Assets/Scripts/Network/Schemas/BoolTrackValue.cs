// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class BoolTrackValue : RulesetFieldValue {
		[Type(2, "array", typeof(ArraySchema<bool>), "boolean")]
		public ArraySchema<bool> toggles = new ArraySchema<bool>();
	}
}
