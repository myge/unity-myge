// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class ResourceValue : RulesetFieldValue {
		[Type(2, "int32")]
		public int current = default(int);

		[Type(3, "int32")]
		public int max = default(int);

		[Type(4, "int32")]
		public int tempCurrentModifier = default(int);

		[Type(5, "int32")]
		public int tempMaxModifier = default(int);

		[Type(6, "int32")]
		public int tempBuffer = default(int);
	}
}
