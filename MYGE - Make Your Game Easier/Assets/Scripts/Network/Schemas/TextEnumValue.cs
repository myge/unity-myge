// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class TextEnumValue : RulesetFieldValue {
		[Type(2, "int32")]
		public int optionIdx = default(int);
	}
}
