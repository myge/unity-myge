// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class TokenState : Schema {
		[Type(0, "string")]
		public string id = default(string);

		[Type(1, "ref", typeof(Position))]
		public Position pos = new Position();

		[Type(2, "boolean")]
		public bool visibleToGmOnly = default(bool);

		[Type(3, "string")]
		public string characterId = default(string);
	}
}
