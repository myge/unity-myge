// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class AttributeValue : RulesetFieldValue {
		[Type(2, "int32")]
		public int value = default(int);

		[Type(3, "int32")]
		public int tempModifier = default(int);

		[Type(4, "int32")]
		public int tempValueOverride = default(int);
	}
}
