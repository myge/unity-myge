// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class CharacterState : Schema {
		[Type(0, "string")]
		public string id = default(string);

		[Type(1, "string")]
		public string ownerSessionId = default(string);

		[Type(2, "map", typeof(MapSchema<string>), "string")]
		public MapSchema<string> representedByTokens = new MapSchema<string>();

		[Type(3, "map", typeof(MapSchema<AttributeValue>))]
		public MapSchema<AttributeValue> attributeValues = new MapSchema<AttributeValue>();

		[Type(4, "map", typeof(MapSchema<BoolFlagValue>))]
		public MapSchema<BoolFlagValue> boolFlagValues = new MapSchema<BoolFlagValue>();

		[Type(5, "map", typeof(MapSchema<BoolTrackValue>))]
		public MapSchema<BoolTrackValue> boolTrackValues = new MapSchema<BoolTrackValue>();

		[Type(6, "map", typeof(MapSchema<ResourceValue>))]
		public MapSchema<ResourceValue> resourceValues = new MapSchema<ResourceValue>();

		[Type(7, "map", typeof(MapSchema<TextEnumValue>))]
		public MapSchema<TextEnumValue> textEnumValues = new MapSchema<TextEnumValue>();

		[Type(8, "map", typeof(MapSchema<TextValue>))]
		public MapSchema<TextValue> textValues = new MapSchema<TextValue>();

		[Type(9, "ref", typeof(PortraitState))]
		public PortraitState portrait = new PortraitState();
	}
}
