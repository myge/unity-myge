// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class PortraitState : Schema {
		[Type(0, "string")]
		public string imageURI = default(string);

		[Type(1, "ref", typeof(Position))]
		public Position tokenOffset = new Position();

		[Type(2, "float32")]
		public float tokenScale = default(float);
	}
}
