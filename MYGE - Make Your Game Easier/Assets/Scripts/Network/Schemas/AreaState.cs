// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class AreaState : Schema {
		[Type(0, "string")]
		public string id = default(string);

		[Type(1, "string")]
		public string name = default(string);

		[Type(2, "ref", typeof(AreaSettings))]
		public AreaSettings areaSettings = new AreaSettings();

		[Type(3, "map", typeof(MapSchema<TokenState>))]
		public MapSchema<TokenState> tokens = new MapSchema<TokenState>();

		[Type(4, "map", typeof(MapSchema<bool>), "boolean")]
		public MapSchema<bool> wallTiles = new MapSchema<bool>();
	}
}
