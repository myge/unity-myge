// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class ServiceCallResult : Schema {
		[Type(0, "string")]
		public string callId = default(string);

		[Type(1, "string")]
		public string serviceName = default(string);

		[Type(2, "string")]
		public string error = default(string);

		[Type(3, "string")]
		public string resultJSON = default(string);
	}
}
