// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class RulesetFieldValue : Schema {
		[Type(0, "string")]
		public string fieldName = default(string);

		[Type(1, "boolean")]
		public bool genericToggle = default(bool);
	}
}
