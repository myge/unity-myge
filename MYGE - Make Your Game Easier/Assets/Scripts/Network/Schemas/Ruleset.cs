// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class Ruleset : Schema {
		[Type(0, "string")]
		public string name = default(string);

		[Type(1, "array", typeof(ArraySchema<string>), "string")]
		public ArraySchema<string> orderedFields = new ArraySchema<string>();

		[Type(2, "map", typeof(MapSchema<string>), "string")]
		public MapSchema<string> fieldTypeNames = new MapSchema<string>();

		[Type(3, "map", typeof(MapSchema<AttributeField>))]
		public MapSchema<AttributeField> attributeFields = new MapSchema<AttributeField>();

		[Type(4, "map", typeof(MapSchema<BoolFlagField>))]
		public MapSchema<BoolFlagField> boolFlagFields = new MapSchema<BoolFlagField>();

		[Type(5, "map", typeof(MapSchema<BoolTrackField>))]
		public MapSchema<BoolTrackField> boolTrackFields = new MapSchema<BoolTrackField>();

		[Type(6, "map", typeof(MapSchema<ResourceField>))]
		public MapSchema<ResourceField> resourceFields = new MapSchema<ResourceField>();

		[Type(7, "map", typeof(MapSchema<TextEnumField>))]
		public MapSchema<TextEnumField> textEnumFields = new MapSchema<TextEnumField>();

		[Type(8, "map", typeof(MapSchema<TextField>))]
		public MapSchema<TextField> textFields = new MapSchema<TextField>();
	}
}
