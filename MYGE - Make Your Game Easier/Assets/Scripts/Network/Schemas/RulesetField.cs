// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.9
// 

using Colyseus.Schema;

namespace Network {
	public partial class RulesetField : Schema {
		[Type(0, "string")]
		public string name = default(string);

		[Type(1, "string")]
		public string category = default(string);

		[Type(2, "boolean")]
		public bool showGenericToggle = default(bool);

		[Type(3, "boolean")]
		public bool gmOnly = default(bool);

		[Type(4, "int32")]
		public int visualizationMode = default(int);
	}
}
