using Colyseus.Schema;
using Network;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;

namespace Network {
    public static class SchemaConverter {
        private static System.Type GetOrderedDictionaryKeyValueCollectionType() {
            var _parentType = typeof(OrderedDictionary);
            var _kvCollectionType = _parentType.GetNestedType("OrderedDictionaryKeyValueCollection", System.Reflection.BindingFlags.NonPublic);
            return _kvCollectionType;
        }

        public class OrderedDictionaryKeyValueCollectionConverter : JsonConverter {
            private bool keysToken = true;

            public override bool CanWrite { get { return true; } }

            public override bool CanConvert(System.Type objectType) {
               return (objectType == GetOrderedDictionaryKeyValueCollectionType());
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
                writer.WriteNull();
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer) {
                return null;
            }
        }

        // ArraySchema
        public static ArraySchema<T> ToArraySchema<T>(this IEnumerable<T> _arr) {
            Dictionary<int, T> _dict = _arr.Select((item, i) => new { i, item })
                .ToDictionary(pair => pair.i, pair => pair.item);
            return new ArraySchema<T>(_dict);
        }

        // MapSchema
        public static MapSchema<T> ToMapSchema<T>(this IEnumerable<(string, T)> _pairs) {
            MapSchema<T> _map = new MapSchema<T>();
            foreach (var (_key, _value) in _pairs) _map.Add(_key, _value);
            return _map;
        }

        // AreaSettings
        public static AreaSettings DeserializeAreaSettings(string _areaSettingsJson) {
            return JsonConvert.DeserializeObject<AreaSettings>(_areaSettingsJson);
        }
        public static string ToJson(this AreaSettings _areaSettings) {
            return JsonConvert.SerializeObject(_areaSettings);
        }

        // RPGSettings
        public static RPGSettings DeserializeRPGSettings(string _rpgSettingsJson) {
            return JsonConvert.DeserializeObject<RPGSettings>(_rpgSettingsJson, new OrderedDictionaryKeyValueCollectionConverter());
        }
        public static string ToJson(this RPGSettings _rpgSettings) {
            return JsonConvert.SerializeObject(_rpgSettings, new OrderedDictionaryKeyValueCollectionConverter());
        }

        // Ruleset
        public static Ruleset DeserializeRuleset(string _rulesetJson) {
            return JsonConvert.DeserializeObject<Ruleset>(_rulesetJson, new OrderedDictionaryKeyValueCollectionConverter());
        }
        public static string ToJson(this Ruleset _ruleset) {
            return JsonConvert.SerializeObject(_ruleset, new OrderedDictionaryKeyValueCollectionConverter());
        }
    }
}