using Colyseus;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using UnityCore.Utils;
using UnityEngine;

namespace Network {

    public class ServiceRegistry : EnhancedMonoBehaviour {
        private Dictionary<string, TaskCompletionSource<ServiceCallResult>> serviceTcsByCallId = new Dictionary<string, TaskCompletionSource<ServiceCallResult>>();

    #region Public Methods
        public TaskCompletionSource<ServiceCallResult> RegisterServiceCall(string _callId) {
            if (serviceTcsByCallId.ContainsKey(_callId)) {
                LogWarning("You are trying to register a service call of ID " + _callId + " twice");
                return null;
            }

            var _tcs = new TaskCompletionSource<ServiceCallResult>();
            serviceTcsByCallId.Add(_callId, _tcs);
            return _tcs;
        }

        public void MarkServiceCallCompletion(ServiceCallResult _result) {
            if (!serviceTcsByCallId.ContainsKey(_result.callId)) {
                LogWarning("You are trying to mark completion of a service call of ID " + _result.callId + " that hasn't been dispatched yet");
                return;
            }

            var _tcs = serviceTcsByCallId[_result.callId];
            _tcs.SetResult(_result);
        }

        public void RemoveServiceCall(string _callId) {
            if (!serviceTcsByCallId.ContainsKey(_callId)) {
                LogWarning("You are trying to remove a service call of ID " + _callId + " that doesn't exist");
                return;
            }
            serviceTcsByCallId.Remove(_callId);
        }
    #endregion

    #region Private Methods

    #endregion
    }
}