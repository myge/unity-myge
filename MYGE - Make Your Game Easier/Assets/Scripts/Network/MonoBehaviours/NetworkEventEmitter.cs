﻿using Config;
using Colyseus;
using Network.Events;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityCore.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace Network {

    public class NetworkEventEmitter : EnhancedMonoBehaviour {
        public readonly AreaCollectionEvent onAddArea = new AreaCollectionEvent();
        public readonly CharacterCollectionEvent onAddCharacter = new CharacterCollectionEvent();
        public readonly RulesetFieldValueCollectionEvent onAddRulesetFieldValue = new RulesetFieldValueCollectionEvent();
        public readonly TokenCollectionEvent onAddToken = new TokenCollectionEvent();
        public readonly WallTileEvent onAddWallTile = new WallTileEvent();
        public readonly MapItemChangesEvent onAreaChanged = new MapItemChangesEvent();
        public readonly MapItemChangesEvent onAreaSettingsChanged = new MapItemChangesEvent();
        public readonly MapItemChangesEvent onCharacterChanged = new MapItemChangesEvent();
        public readonly CharacterOwnershipEvent onCharacterOwnerChanged = new CharacterOwnershipEvent();
        public readonly CharacterPortraitEvent onCharacterPortraitChanged = new CharacterPortraitEvent();
        public readonly ChatMessageEvent onChatMessage = new ChatMessageEvent();
        public readonly DiceRollEvent onDiceRoll = new DiceRollEvent();
        public readonly CampaignStateEvent onFirstState = new CampaignStateEvent();
        public readonly StringEvent onPlayersAreaChanged = new StringEvent();
        public readonly AreaCollectionEvent onRemoveArea = new AreaCollectionEvent();
        public readonly CharacterCollectionEvent onRemoveCharacter = new CharacterCollectionEvent();
        public readonly RulesetFieldValueCollectionEvent onRemoveRulesetFieldValue = new RulesetFieldValueCollectionEvent();
        public readonly TokenCollectionEvent onRemoveToken = new TokenCollectionEvent();
        public readonly WallTileEvent onRemoveWallTile = new WallTileEvent();
        public readonly UnityEvent onRoomDeleted = new UnityEvent();
        public readonly RulesetFieldValueChangesEvent onRulesetFieldValueChanged = new RulesetFieldValueChangesEvent();
        public readonly MapItemChangesEventDepth2 onTokenChanged = new MapItemChangesEventDepth2();
        public readonly MapItemChangesEventDepth2 onTokenPositionChanged = new MapItemChangesEventDepth2();

    #region Public Methods
        public void HandleAddArea(AreaState _area, string _areaId) {
            onAddArea.Invoke(_area, _areaId);

            _area.OnChange += (_changes) => {
                HandleAreaChanges(_areaId, _changes);
            };

            _area.areaSettings.OnChange += (_changes) => {
                HandleAreaSettingChanges(_areaId, _changes);
            };

            _area.tokens.OnAdd += delegate (TokenState _token, string _tokenId) {
                HandleAddToken(_token, _tokenId, _areaId);
            };
            _area.tokens.OnRemove += delegate (TokenState _token, string _tokenId) {
                HandleRemoveToken(_token, _tokenId, _areaId);
            };

            _area.wallTiles.OnAdd += delegate (bool _wall, string _posString) {
                HandleAddWallTile(_areaId, _posString);
            };
            _area.wallTiles.OnChange += delegate (bool _wall, string _posString) {
                if (_wall) HandleAddWallTile(_areaId, _posString);
                else HandleRemoveWallTile(_areaId, _posString);
            };
            _area.wallTiles.OnRemove += delegate (bool _wall, string _posString) {
                HandleRemoveWallTile(_areaId, _posString);
            };
        }

        public void HandleAddCharacter(CharacterState _character, string _characterId) {
            onAddCharacter.Invoke(_character, _characterId);
            
            _character.OnChange += (_changes) => {
                HandleCharacterChanges(_characterId, _changes);
            };

            _character.attributeValues.OnAdd += delegate (AttributeValue _value, string _fieldName) {
                HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Attribute);
            };
            _character.attributeValues.OnRemove += delegate (AttributeValue _value, string _fieldName) {
                HandleRemoveRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Attribute);
            };

            _character.boolFlagValues.OnAdd += delegate (BoolFlagValue _value, string _fieldName) {
                HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Bool);
            };
            _character.boolFlagValues.OnRemove += delegate (BoolFlagValue _value, string _fieldName) {
                HandleRemoveRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Bool);
            };

            _character.boolTrackValues.OnAdd += delegate (BoolTrackValue _value, string _fieldName) {
                HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.BoolTrack);
            };
            _character.boolTrackValues.OnRemove += delegate (BoolTrackValue _value, string _fieldName) {
                HandleRemoveRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.BoolTrack);
            };

            _character.resourceValues.OnAdd += delegate (ResourceValue _value, string _fieldName) {
                HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Resource);
            };
            _character.resourceValues.OnRemove += delegate (ResourceValue _value, string _fieldName) {
                HandleRemoveRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Resource);
            };

            _character.textEnumValues.OnAdd += delegate (TextEnumValue _value, string _fieldName) {
                HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.TextEnum);
            };
            _character.textEnumValues.OnRemove += delegate (TextEnumValue _value, string _fieldName) {
                HandleRemoveRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.TextEnum);
            };

            _character.textValues.OnAdd += delegate (TextValue _value, string _fieldName) {
                HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Text);
            };
            _character.textValues.OnRemove += delegate (TextValue _value, string _fieldName) {
                HandleRemoveRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Text);
            };

            _character.portrait.OnChange += (_changes) => {
                HandleCharacterPortraitChanges(_characterId, _changes);
            };
        }

        public void HandleAddRulesetFieldValue(RulesetFieldValue _value, string _fieldName, string _characterId, RulesetFieldType _type) {
            onAddRulesetFieldValue.Invoke(_value, _fieldName, _characterId, _type);
            
            _value.OnChange += (_changes) => {
                HandleRulesetFieldValueChanges(_fieldName, _characterId, _type, _changes);
            };

            // Bool tracks are different, because there is a collection within the value schema
            if (_type == RulesetFieldType.BoolTrack) {
                BoolTrackValue _trackValue = (BoolTrackValue)_value;

                // TODO: Perhaps optimize. Currently the system dumbs down detailed changes
                //  to the toggle track and instead only sees whether the *entire* track
                //  has changed, and then updates the *entire* track.
                _trackValue.toggles.OnAdd += (bool _toggle, int _toggleIdx) => {
                    HandleRulesetFieldValueChanges(_fieldName, _characterId, _type, new List<Colyseus.Schema.DataChange>());
                };
                _trackValue.toggles.OnChange += (bool _toggle, int _toggleIdx) => {
                    HandleRulesetFieldValueChanges(_fieldName, _characterId, _type, new List<Colyseus.Schema.DataChange>());
                };
                _trackValue.toggles.OnRemove += (bool _toggle, int _toggleIdx) => {
                    HandleRulesetFieldValueChanges(_fieldName, _characterId, _type, new List<Colyseus.Schema.DataChange>());
                };
            }
        }

        public void HandleAddToken(TokenState _token, string _tokenId, string _areaId) {
            onAddToken.Invoke(_token, _tokenId, _areaId);

            _token.OnChange += (_changes) => {
                HandleTokenChanges(_areaId, _tokenId, _changes);
            };

            _token.pos.OnChange += (_changes) => {
                HandleTokenPositionChanges(_areaId, _tokenId, _changes);
            };
        }

        public void HandleAddWallTile(string _areaId, string _posStringInt) {
            // No need to track changes - this is a boolean map that for any given key stores
            //  either a true value or nothing at all - add and remove is enough.
            Vector2Int _pos = SchemaExtensions.VectorFromStringInt(_posStringInt);
            onAddWallTile.Invoke(_areaId, _pos.x, _pos.y);
        }

        public void HandleAreaChanges(string _areaId, List<Colyseus.Schema.DataChange> _changes) {
            onAreaChanged.Invoke(_areaId, _changes);
        }

        public void HandleAreaSettingChanges(string _areaId, List<Colyseus.Schema.DataChange> _changes) {
            onAreaSettingsChanged.Invoke(_areaId, _changes);
        }
        
        public void HandleCharacterChanges(string _characterId, List<Colyseus.Schema.DataChange> _changes) {
            onCharacterChanged.Invoke(_characterId, _changes);

            _changes.ForEach(_change => {
                switch(_change.Field) {
                    case "ownerSessionId":
                        string _newOwnerSessionId = (string)_change.Value;
                        onCharacterOwnerChanged.Invoke(_characterId, _newOwnerSessionId);
                        break;

                    default:
                        break;
                }
            });
        }

        public void HandleCharacterPortraitChanges(string _characterId, List<Colyseus.Schema.DataChange> _changes) {
            onCharacterPortraitChanged.Invoke(_characterId, _changes);
        }

        public void HandleChatMessage(ChatMessage _msg) {
            onChatMessage.Invoke(_msg);
        }

        public void HandleCallServiceResultMessage(ServiceCallResult _msg) {
            // Specific events resulting from service calls go here
        }

        public void HandleDiceRollResultMessage(DiceRollResult _msg) {
            onDiceRoll.Invoke(_msg);
        }

        public void HandleNewState(CampaignState _state, bool _isFirstState) {
            // This should be used when we want to re-render the *entire*
            //  state, which is not that often.
            if (_isFirstState) {
                onFirstState.Invoke(_state);

                // Run all collection handlers here. This is important, because otherwise various onChange handlers
                //  will not be registered. In other words, we need to simulate the addition of each area, token etc.,
                //  even if they are *already* in the state.
                _state.areas.ForEach(delegate (string _areaId, AreaState _area) {
                    HandleAddArea(_area, _areaId);
                    _area.tokens.ForEach(delegate (string _tokenId, TokenState _token) {
                        HandleAddToken(_token, _tokenId, _areaId);
                    });
                });
                _state.characters.ForEach(delegate (string _characterId, CharacterState _character) {
                    HandleAddCharacter(_character, _characterId);
                    _character.attributeValues.ForEach(delegate (string _fieldName, AttributeValue _value) {
                        HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Attribute);
                    });
                    _character.boolFlagValues.ForEach(delegate (string _fieldName, BoolFlagValue _value) {
                        HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Bool);
                    });
                    _character.boolTrackValues.ForEach(delegate (string _fieldName, BoolTrackValue _value) {
                        HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.BoolTrack);
                    });
                    _character.resourceValues.ForEach(delegate (string _fieldName, ResourceValue _value) {
                        HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Resource);
                    });
                    _character.textEnumValues.ForEach(delegate (string _fieldName, TextEnumValue _value) {
                        HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.TextEnum);
                    });
                    _character.textValues.ForEach(delegate (string _fieldName, TextValue _value) {
                        HandleAddRulesetFieldValue(_value, _fieldName, _characterId, RulesetFieldType.Text);
                    });
                });
            }
        }

        public void HandleRemoveArea(AreaState _area, string _areaId) {
            onRemoveArea.Invoke(_area, _areaId);
        }

        public void HandleRemoveCharacter(CharacterState _character, string _characterId) {
            onRemoveCharacter.Invoke(_character, _characterId);
        }

        public void HandleRemoveRulesetFieldValue(RulesetFieldValue _value, string _fieldName, string _characterId, RulesetFieldType _type) {
            onRemoveRulesetFieldValue.Invoke(_value, _fieldName, _characterId, _type);
        }

        public void HandleRemoveToken(TokenState _token, string _tokenId, string _areaId) {
            onRemoveToken.Invoke(_token, _tokenId, _areaId);
        }

        public void HandleRemoveWallTile(string _areaId, string _posStringInt) {
            Vector2Int _pos = SchemaExtensions.VectorFromStringInt(_posStringInt);
            onRemoveWallTile.Invoke(_areaId, _pos.x, _pos.y);
        }

        public void HandleRoomDeleted() {
            onRoomDeleted.Invoke();
        }

        public void HandleRulesetFieldValueChanges(string _fieldName, string _characterId, RulesetFieldType _type, List<Colyseus.Schema.DataChange> _changes) {
            onRulesetFieldValueChanged.Invoke(_fieldName, _characterId, _type, _changes);
        }

        public void HandleStateChanges(CampaignState _state, List<Colyseus.Schema.DataChange> _changes) {
            // Specific events resulting from changes in the state go here
            _changes.ForEach(_change => {
                switch(_change.Field) {
                    case "playerAreaId":
                        string _newPlayerAreaID = (string)_change.Value;
                        onPlayersAreaChanged.Invoke(_newPlayerAreaID);
                        break;

                    default:
                        break;
                }
            });
        }

        public void HandleTokenChanges(string _areaId, string _tokenId, List<Colyseus.Schema.DataChange> _changes) {
            onTokenChanged.Invoke(_areaId, _tokenId, _changes);
        }

        public void HandleTokenPositionChanges(string _areaId, string _tokenId, List<Colyseus.Schema.DataChange> _changes) {
            onTokenPositionChanged.Invoke(_areaId, _tokenId, _changes);
        }
    #endregion

    #region Private Methods

    #endregion
    }
}
