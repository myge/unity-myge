﻿using Config;
using Colyseus;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using UnityCore.Utils;
using UnityEngine;

namespace Network {

    public class ColyseusClientController : EnhancedMonoBehaviour {
        public Role role { get; private set; }

        private Client client;
        private Room<CampaignState> room;
    
    #region Unity Editor Fields
        public uint port = 2567;
        public string roomTypeName = "campaign";
        public NetworkEventEmitter emitter;
        public ServiceRegistry serviceRegistry;
    #endregion

    #region Public Properties
        public IPAddress serverIP { get; private set; }
    #endregion

    #region Public Methods
        public async Task<string> CallService(string _serviceName, object _params=null) {
            if (_params == null) _params = new {};

            string _callId = Guid.NewGuid().ToString();

            ServiceCall _msg = new ServiceCall();
            _msg.callId = _callId;
            _msg.serviceName = _serviceName;
            _msg.paramsJSON = JsonConvert.SerializeObject(_params);

            Log("Sending a \"" + _msg.callId + "\" service call with ID " + _msg.serviceName + " and params " + _msg.paramsJSON);

            var _serviceTCS = serviceRegistry.RegisterServiceCall(_callId);
            await room.Send("callService", _msg);
            ServiceCallResult _resultMsg = await _serviceTCS.Task;
            serviceRegistry.RemoveServiceCall(_callId);

            if(_resultMsg.error != null && _resultMsg.error != "") {
                LogWarning("Service \"" + _serviceName + "\" call with ID " + _callId + " unsuccessful. " + _resultMsg.error);
                throw new Exception(_resultMsg.error);
            } else {
                Log("Service \"" + _serviceName + "\" call with ID " + _callId + " successful. Result: " + _resultMsg.resultJSON);

                if (_serviceName == "deleteRoom") {
                    // If we have successfully deleted the room, we can forget all the sessions for this room.
                    LocalConfig.DeleteSessionsForRoom(room.Id);
                }
            }

            return _resultMsg.resultJSON;
        }

        public async Task<bool> CreateRoom() {
            if (!RequireClient("CreateRoom")) return false;
            if (IsInRoom()) {
                LogWarning("You're trying to create a room while connected to one. Leave the room first, then create a new room.");
                return false;
            }

            try {
                Room<CampaignState> _room = await client.Create<CampaignState>(roomTypeName, new Dictionary<string, object>{
                    {"areaSettingsDefaultJSON", JsonConvert.SerializeObject(LocalConfig.areaSettingDefaults)},
                    {"rpgSettingsJSON", JsonConvert.SerializeObject(LocalConfig.rpgSettingDefualts)}
                });
                Log("Room creation successful at " + serverIP.ToString() + ", room ID: " + _room.Id + ", session ID: " + _room.SessionId);
                
                // Wait for initial state to make sure that the room has been fully initialized and the state has data
                await WaitForInitialRoomState(_room);

                Log("Room initial state acquired.");
                room = _room;
                InitializeEventListeners();

                // Upon successful room creation, it's safe to assume we are the host/GM.
                role = Role.GM;

                // Remember the session and room id locally to later reconnect.
                CampaignRoomAvailable _roomData = new CampaignRoomAvailable();
                _roomData.roomId = room.Id;
                _roomData.clients = 0;
                _roomData.maxClients = (uint)room.State.allPlayersSessionIDs.Count;
                _roomData.metadata = new CampaignRoomMetadata();
                _roomData.metadata.visibleName = room.State.visibleName;
                LocalConfig.StoreSessionInfo(room.SessionId, new SessionData(_roomData, role, room.SessionId));

                return true;
            } catch (Exception ex) {
                LogWarning("Room creation failed. " + ex.ToString());
                return false;
            }
        }

        public async Task<List<CampaignRoomAvailable>> GetAvailableRooms() {
            if (!RequireClient("GetAvailableRooms")) return null;
            try {
                List<CampaignRoomAvailable> _hibernatedRooms = LocalConfig.GetHibernatedRooms();
                CampaignRoomAvailable[] _onlineRooms = await client.GetAvailableRooms<CampaignRoomAvailable>(roomTypeName);

                Log("Available room lookup successful at " + serverIP.ToString()
                    + ", number of rooms on the server: " + _onlineRooms.Length
                    + ", number of rooms hibernated: " + _hibernatedRooms.Count
                );

                List<CampaignRoomAvailable> _roomsAvailable = new List<CampaignRoomAvailable>();

                HashSet<string> _onlineRoomIDs = new HashSet<string>();
                foreach (var _room in _onlineRooms) {
                    _onlineRoomIDs.Add(_room.roomId);
                    _roomsAvailable.Add(_room);
                }
                foreach (var _room in _hibernatedRooms) {
                    if (!_onlineRoomIDs.Contains(_room.roomId)) {
                        _roomsAvailable.Add(_room);
                    }
                }
                return _roomsAvailable;
            } catch (Exception ex) {
                LogWarning("Available room lookup unsuccessful. " + ex.ToString());
                return null;
            }
        }

        public string GetLocalSessionId() {
            if (!RequireClient("GetLocalSessionId")) return "";
            return room.SessionId;
        }

        public CampaignState GetLocalState() {
            if (!RequireClient("GetLocalState")) return null;
            return room.State;
        }

        public string GetHostHTTPAddress() {
            return "http://" + serverIP + ":" + port;
        }

        public bool IsInRoom() {
            return room != null;
        }

        public async Task<bool> JoinRoom(CampaignRoomAvailable _roomAvailable) {
            if (!RequireClient("JoinRoom")) return false;
            if (IsInRoom()) {
                LogWarning("You're trying to join a room while connected to one. Leave the room first, then create a new room.");
                return false;
            }

            try {
                // Find session info for the room
                var _savedSessions = LocalConfig.GetSessionsForRoom(_roomAvailable.roomId);
                // TODO: Add manual selection for possible multi-instancing
                var _selectedSession = _savedSessions.Count > 0 ? _savedSessions[0] : null;
                if(_selectedSession != null) Log("Previous session data found: " + JsonConvert.SerializeObject(_selectedSession));

                // If possible, re-connect to the old session. If there isn't one, we'll just get a new id
                var _options = _selectedSession == null ? null : new Dictionary<string, object>{
                    {"sessionId", _selectedSession.sessionId}
                };

                Room<CampaignState> _room = await client.JoinById<CampaignState>(_roomAvailable.roomId, _options);
                Log("Joining a room successful at " + serverIP.ToString() + ", room ID: " + _room.Id + ", session ID: " + _room.SessionId);

                // Wait for initial state to make sure that the room has been fully initialized and the state has data
                await WaitForInitialRoomState(_room);

                Log("Room initial state acquired.");
                room = _room;
                InitializeEventListeners();

                // If possible, infer the role from local session data. Otherwise, upon successfully joining someone 
                //  else's room, it's safe to assume we are just a player.
                role = _selectedSession != null ? _selectedSession.role : Role.Player;

                // Remember the session and room id locally to later reconnect.
                LocalConfig.StoreSessionInfo(room.SessionId, new SessionData(_roomAvailable, role, room.SessionId));

                return true;
            } catch (Exception ex) {
                LogWarning("Joining a room failed. " + ex.ToString());
                return false;
            }
        }

        public async Task<bool> LeaveRoom() {
            if (!RequireRoom("LeaveRoom")) return false;

            await room.Leave();
            room = null;

            // When we're not in a room, we don't have a role
            role = Role.None;

            return true;
        }

        public async Task<bool> RecreateRoom(CampaignRoomAvailable _roomAvailable) {
            if (!RequireClient("RecreateRoom")) return false;
            if (IsInRoom()) {
                LogWarning("You're trying to re-create a room while connected to one. Leave the room first, then re-create the room.");
                return false;
            }
            if (_roomAvailable.isOnline) {
                LogWarning("Cannot re-create a room that's currently online");
                return false;
            }

            try {
                // Find session info for the room
                var _savedSessions = LocalConfig.GetSessionsForRoom(_roomAvailable.roomId);
                if (_savedSessions.Count < 1) {
                    LogWarning("There are no local sessions saved for room " + _roomAvailable.roomId);
                    return false;
                }

                // TODO: Add manual selection for possible multi-instancing
                var _selectedSession = _savedSessions[0];

                if (_selectedSession.role != Role.GM) {
                    LogWarning("The local session saved for room " + _roomAvailable.roomId + " is a player session. Only the original host can re-create a room.");
                    return false;
                }

                Room<CampaignState> _room = await client.Create<CampaignState>(roomTypeName, new Dictionary<string, object>{
                    {"id", _roomAvailable.roomId},
                    {"sessionId", _selectedSession.sessionId}
                });
                Log("Room re-creation successful at " + serverIP.ToString() + ", room ID: " + _room.Id + ", session ID: " + _room.SessionId);

                // Wait for initial state to make sure that the room has been fully initialized and the state has data
                await WaitForInitialRoomState(_room);

                Log("Room initial state acquired.");
                room = _room;
                InitializeEventListeners();

                // Infer the role from found local session data
                role = _selectedSession.role;

                return true;
            } catch (Exception ex) {
                LogWarning("Room re-creation failed. " + ex.ToString());
                return false;
            }
        }

        public async Task SendChatMessage(string _message) {
            if (!RequireRoom("SendChatMessage")) return;

            await room.Send("chat", _message);
        }

        public bool SetTargetServerIP(IPAddress _ip) {
            if (IsInRoom()) {
                LogWarning("You're trying to set server IP while connected to a room. Leave the room first, then set the IP and then join a room again.");
                return false;
            } else {
                serverIP = _ip;
                UpdateClient();
                return true;
            }
        }

        public bool SetTargetServerIP(string _ip) {
            IPAddress _validatedIP;
            bool _valid = IPAddress.TryParse(_ip, out _validatedIP);
            if (!_valid) {
                LogWarning(_ip + " is not a valid IP string");
                return false;
            } else {
                return SetTargetServerIP(_validatedIP);
            }
        }
    #endregion

    #region Private Methods
        private void CheckIntegrity() {
            if (port == 0) LogWarning("Connection port is 0");
            if (roomTypeName == "") LogWarning("Room type name is empty");
            if (!serviceRegistry) LogWarning("No ServiceRegistry assigned");
            if (!emitter) LogWarning("No NetworkEventEmitter assigned");
        }

        private string GetConnectionString() {
            if (serverIP == null) return "";
            return "ws://" + serverIP.ToString() + ":" + port;
        }

        private void InitializeEventListeners() {
            if (room == null) {
                LogWarning("You are trying to initialize event listeners for a room while not being in one");
                return;
            }

            // Hook the emitter methods

            // Chat retrieval message
            room.OnMessage("chat", delegate (ChatMessage _msg) {
                Log("[Chat] " + _msg.authorSessionId + ": " + _msg.message);
                emitter.HandleChatMessage(_msg);
            });

            // Service call completion message
            room.OnMessage("callServiceResult", delegate (ServiceCallResult _msg) {
                serviceRegistry.MarkServiceCallCompletion(_msg);
                emitter.HandleCallServiceResultMessage(_msg);
            });

            // Dice roll result message
            room.OnMessage("diceRollResult", delegate (string _msgJson) {
                DiceRollResult _msg = JsonConvert.DeserializeObject<DiceRollResult>(_msgJson);
                emitter.HandleDiceRollResultMessage(_msg);
            });

            // Room deletion message
            room.OnMessage("roomDeleted", delegate (object _msg) {
                emitter.HandleRoomDeleted();
            });

            // State change
            room.State.OnChange += delegate (List<Colyseus.Schema.DataChange> _changes) {
                emitter.HandleStateChanges(room.State, _changes);
            };

            // AreaState collection changes
            room.State.areas.OnAdd += emitter.HandleAddArea;
            room.State.areas.OnRemove += emitter.HandleRemoveArea;

            // CharacterState collection changes
            room.State.characters.OnAdd += emitter.HandleAddCharacter;
            room.State.characters.OnRemove += emitter.HandleRemoveCharacter;
        }

        private bool RequireClient(string _operationDescription = "") {
            if (client == null) {
                if (_operationDescription == "") {
                    LogWarning("Client instance is null. Perhaps no IP address has been set?");
                } else {
                    LogWarning("Client instance is null. Perhaps no IP address has been set? Operation: " + _operationDescription);
                }
                return false;
            }
            return true;
        }

        private bool RequireRoom(string _operationDescription = "") {
            if (!IsInRoom()) {
                if (_operationDescription == "") {
                    LogWarning("You're not in a room, yet you're trying to perform an operation that requires you being in a room");
                } else {
                    LogWarning("You're not in a room, yet you're trying to perform an operation (" + _operationDescription + ") that requires you being in a room");
                }
                return false;
            }
            return true;
        }

        private void UpdateClient() {
            string _connectionString = GetConnectionString();
            if (_connectionString == "") {
                LogWarning("You're trying to create a Colyseus client before setting an IP address. Set the IP first and then start establishing connections.");
            } else {
                client = new Client(_connectionString);
            }
        }

        private async Task WaitForInitialRoomState(Room<CampaignState> _room) {
            var _initialStateTCS = new TaskCompletionSource<bool>();
            _room.OnStateChange += delegate (CampaignState _state, bool _isFirstState) {
                // Hook the emitter here. Apparently hooking it separately doesn't work.
                emitter.HandleNewState(_state, _isFirstState);

                if (_isFirstState) _initialStateTCS.SetResult(true);
            };
            await _initialStateTCS.Task;
        }
    #endregion

    #region Unity Functions
        private void Awake() {
            CheckIntegrity();
        }
    #endregion
    }
}