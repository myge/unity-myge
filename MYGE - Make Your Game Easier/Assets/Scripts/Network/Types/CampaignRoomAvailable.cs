using Colyseus;
using System;

namespace Network {

    [Serializable]
    public class CampaignRoomAvailable : RoomAvailable {
        public CampaignRoomMetadata metadata;

        // If false, that means the room has been disposed of server-side and must be re-created.
        public bool isOnline = true;
    }
}