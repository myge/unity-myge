﻿using Config;
using Colyseus.Schema;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Network {

    namespace Events {

        public class AreaCollectionEvent : UnityEvent<AreaState, string> {}
        public class AreaEvent : UnityEvent<AreaState> {}
        public class AreaSettingsChangesEvent : UnityEvent<AreaState, AreaSettings, List<DataChange>> {}
        public class CampaignStateEvent : UnityEvent<CampaignState> {}
        public class CharacterCollectionEvent : UnityEvent<CharacterState, string> {}
        public class CharacterOwnershipEvent : UnityEvent<string, string> {}
        public class CharacterPortraitEvent : UnityEvent<string, List<DataChange>> {}
        public class ChatMessageEvent : UnityEvent<ChatMessage> {}
        public class DiceRollEvent : UnityEvent<DiceRollResult> {}
        public class MapItemChangesEvent : UnityEvent<string, List<DataChange>> {}
        public class MapItemChangesEventDepth2 : UnityEvent<string, string, List<DataChange>> {}
        public class RulesetFieldValueChangesEvent : UnityEvent<string, string, RulesetFieldType, List<DataChange>> {}
        public class RulesetFieldValueCollectionEvent : UnityEvent<RulesetFieldValue, string, string, RulesetFieldType> {}
        public class TokenCollectionEvent : UnityEvent<TokenState, string, string> {}
        public class StringEvent : UnityEvent<string> {}
        public class WallTileEvent : UnityEvent<string, int, int> {}
    }
}