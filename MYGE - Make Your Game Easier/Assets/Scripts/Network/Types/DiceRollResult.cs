﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Network {

    [System.Serializable]
    public struct DiceRollResult {
        [System.Serializable]
        public struct IndividualRoll {
            public int times;
            public int sides;

            public IndividualRoll(int _times, int _sides) {
                times = _times;
                sides = _sides;
            }
        }

        [System.Serializable]
        public struct IndividualRollResult {
            public int sides;
            public int result;

            public IndividualRollResult(int _sides, int _result) {
                sides = _sides;
                result = _result;
            }
        }

        public string rollerSessionId;
        public List<IndividualRoll> rolls;
        public List<IndividualRollResult> individualResults;
        public int modifier;
        public int finalResult;
    }
}