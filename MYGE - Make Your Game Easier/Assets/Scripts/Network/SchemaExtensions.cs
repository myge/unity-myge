﻿using Config;
using Colyseus.Schema;
using Network;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;

namespace Network {
    public static class SchemaExtensions {
        // Ruleset
        public static void AddField(this Ruleset _ruleset, RulesetField _field) {
            switch (_field.GetFieldType()) {
                case RulesetFieldType.Text:
                    _ruleset.AddField((TextField)_field);
                    break;
                case RulesetFieldType.Attribute:
                    _ruleset.AddField((AttributeField)_field);
                    break;
                case RulesetFieldType.Resource:
                    _ruleset.AddField((ResourceField)_field);
                    break;
                case RulesetFieldType.Bool:
                    _ruleset.AddField((BoolFlagField)_field);
                    break;
                case RulesetFieldType.TextEnum:
                    _ruleset.AddField((TextEnumField)_field);
                    break;
                case RulesetFieldType.BoolTrack:
                    _ruleset.AddField((BoolTrackField)_field);
                    break;
                default:
                    break;
            }
        }
        public static void AddField(this Ruleset _ruleset, TextField _field) {
            if (_ruleset.fieldTypeNames.ContainsKey(_field.name)) {
                throw new ArgumentException("Field of name \"" + _field.name + "\" already present in the ruleset");
            }
            _ruleset.orderedFields.Items.Add(_ruleset.orderedFields.Count, _field.name);
            _ruleset.textFields.Add(_field.name, (TextField)_field);
            _ruleset.fieldTypeNames.Add(_field.name, _field.GetFieldTypeString());
        }
        public static void AddField(this Ruleset _ruleset, AttributeField _field) {
            if (_ruleset.fieldTypeNames.ContainsKey(_field.name)) {
                throw new ArgumentException("Field of name \"" + _field.name + "\" already present in the ruleset");
            }
            _ruleset.orderedFields.Items.Add(_ruleset.orderedFields.Count, _field.name);
            _ruleset.attributeFields.Add(_field.name, (AttributeField)_field);
            _ruleset.fieldTypeNames.Add(_field.name, _field.GetFieldTypeString());
        }
        public static void AddField(this Ruleset _ruleset, ResourceField _field) {
            if (_ruleset.fieldTypeNames.ContainsKey(_field.name)) {
                throw new ArgumentException("Field of name \"" + _field.name + "\" already present in the ruleset");
            }
            _ruleset.orderedFields.Items.Add(_ruleset.orderedFields.Count, _field.name);
            _ruleset.resourceFields.Add(_field.name, (ResourceField)_field);
            _ruleset.fieldTypeNames.Add(_field.name, _field.GetFieldTypeString());
        }
        public static void AddField(this Ruleset _ruleset, BoolFlagField _field) {
            if (_ruleset.fieldTypeNames.ContainsKey(_field.name)) {
                throw new ArgumentException("Field of name \"" + _field.name + "\" already present in the ruleset");
            }
            _ruleset.orderedFields.Items.Add(_ruleset.orderedFields.Count, _field.name);
            _ruleset.boolFlagFields.Add(_field.name, (BoolFlagField)_field);
            _ruleset.fieldTypeNames.Add(_field.name, _field.GetFieldTypeString());
        }
        public static void AddField(this Ruleset _ruleset, TextEnumField _field) {
            if (_ruleset.fieldTypeNames.ContainsKey(_field.name)) {
                throw new ArgumentException("Field of name \"" + _field.name + "\" already present in the ruleset");
            }
            _ruleset.orderedFields.Items.Add(_ruleset.orderedFields.Count, _field.name);
            _ruleset.textEnumFields.Add(_field.name, (TextEnumField)_field);
            _ruleset.fieldTypeNames.Add(_field.name, _field.GetFieldTypeString());
        }
        public static void AddField(this Ruleset _ruleset, BoolTrackField _field) {
            if (_ruleset.fieldTypeNames.ContainsKey(_field.name)) {
                throw new ArgumentException("Field of name \"" + _field.name + "\" already present in the ruleset");
            }
            _ruleset.orderedFields.Items.Add(_ruleset.orderedFields.Count, _field.name);
            _ruleset.boolTrackFields.Add(_field.name, (BoolTrackField)_field);
            _ruleset.fieldTypeNames.Add(_field.name, _field.GetFieldTypeString());
        }

        public static RulesetField GetFieldByName(this Ruleset _ruleset, string _name) {
            string _typeName = _ruleset.fieldTypeNames[_name];
            RulesetFieldType _type = (RulesetFieldType)Enum.Parse(typeof(RulesetFieldType), _typeName);
            switch (_type) {
                case RulesetFieldType.Text:
                    return _ruleset.textFields[_name];
                case RulesetFieldType.Attribute:
                    return _ruleset.attributeFields[_name];
                case RulesetFieldType.Resource:
                    return _ruleset.resourceFields[_name];
                case RulesetFieldType.Bool:
                    return _ruleset.boolFlagFields[_name];
                case RulesetFieldType.TextEnum:
                    return _ruleset.textEnumFields[_name];
                case RulesetFieldType.BoolTrack:
                    return _ruleset.boolTrackFields[_name];
                default:
                    return null;
            }
        }

        public static List<RulesetField> GetFieldsInOrder(this Ruleset _ruleset) {
            var _fields = new List<RulesetField>();
            _ruleset.orderedFields.ForEach(delegate (string _fieldName) {
                _fields.Add(_ruleset.GetFieldByName(_fieldName));
            });
            return _fields;
        }

        public static Dictionary<string, List<RulesetField>> GetFieldsInOrderByCategory(this Ruleset _ruleset) {
            var _categories = new Dictionary<string, List<RulesetField>>();

            _ruleset.orderedFields.ForEach(delegate (string _fieldName) {
                RulesetField _field = _ruleset.GetFieldByName(_fieldName);
                string _category = _field.category == null ? "" : _field.category;
                if (!_categories.ContainsKey(_category)) _categories.Add(_category, new List<RulesetField>());
                _categories[_category].Add(_field);
            });

            return _categories;
        }

        // RulesetField
        public static RulesetFieldType GetFieldType(this RulesetField _field) {
            if (_field is TextField) return RulesetFieldType.Text;
            if (_field is AttributeField) return RulesetFieldType.Attribute;
            if (_field is ResourceField) return RulesetFieldType.Resource;
            if (_field is BoolFlagField) return RulesetFieldType.Bool;
            if (_field is TextEnumField) return RulesetFieldType.TextEnum;
            if (_field is BoolTrackField) return RulesetFieldType.BoolTrack;
            else return RulesetFieldType.Invalid;
        }

        public static int GetFieldTypeInt(this RulesetField _field) {
            return (int)_field.GetFieldType();
        }

        public static string GetFieldTypeString(this RulesetField _field) {
            return _field.GetFieldType().ToString();
        }

        // Character
        public static RulesetFieldValue GetFieldValue(this CharacterState _character, string _fieldName, RulesetFieldType _type) {
            try {
                if (_type == RulesetFieldType.Attribute) return _character.attributeValues[_fieldName];
                if (_type == RulesetFieldType.Bool) return _character.boolFlagValues[_fieldName];
                if (_type == RulesetFieldType.BoolTrack) return _character.boolTrackValues[_fieldName];
                if (_type == RulesetFieldType.Resource) return _character.resourceValues[_fieldName];
                if (_type == RulesetFieldType.TextEnum) return _character.textEnumValues[_fieldName];
                if (_type == RulesetFieldType.Text) return _character.textValues[_fieldName];
            } catch (KeyNotFoundException) {
                return null;
            }

            return null;
        }

        // Position
        public static string ToStringInt(this Position _pos) {
            return ((int)_pos.x).ToString() + "x" + ((int)_pos.y).ToString();
        }
        public static Vector2 ToVector(this Position _pos) {
            return new Vector2(_pos.x, _pos.y);
        }
        public static Position PosFromStringInt(string _posStringInt) {
            Vector2Int _vec = VectorFromStringInt(_posStringInt);

            Position _pos = new Position();
            _pos.x = _vec.x;
            _pos.y = _vec.y;

            return _pos;
        }
        public static Vector2Int VectorFromStringInt(string _posStringInt) {
            string[] _strings = _posStringInt.Split(new char[] {'x'}, StringSplitOptions.RemoveEmptyEntries);
            if (_strings.Length != 2) {
                throw new System.FormatException("Position strings must be written as exactly two integers separated with an 'x', " + _strings.Length.ToString() + " values found instead");
            }
            int _posX = int.Parse(_strings[0]);
            int _posY = int.Parse(_strings[1]);

            return new Vector2Int(_posX, _posY);
        }

        // CampaignState
        public static bool IsPlayerTokenOwner(this CampaignState _state, string _areaId, string _tokenId, string _ownerSessionId) {
            AreaState _area = _state.areas[_areaId];
            TokenState _token = _area.tokens[_tokenId];
            CharacterState _character = _state.characters[_token.characterId];

            return _character.ownerSessionId == _ownerSessionId;
        }
        public static HashSet<TokenState> GetTokensOwnedByPlayer(this CampaignState _state, string _areaId, string _ownerSessionId) {
            var _tokens = new HashSet<TokenState>();

            AreaState _area = _state.areas[_areaId];

            _area.tokens.ForEach(delegate (string _tokenId, TokenState _token) {
                CharacterState _character = _state.characters[_token.characterId];
                if (_character.ownerSessionId == _ownerSessionId) {
                    _tokens.Add(_token);
                }
            });

            return _tokens;
        }
    }
}